<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.css'/>
	<link rel="stylesheet" href="<?= base_url('assets/css/tax.css'); ?>"/>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.topbar.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.reveal.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.dropdown.js"></script>
<script type="text/javascript" src="http://localhost/tax/assets/js/tax.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.concat.min.js">
</script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.css">
</head>
<body style='background-color:#F5F5F5;font-family: Roboto sans-serif;margin-left:5px;margin-right:5px;'>
	    <div class="wrapper">	
		<nav class="top-bar sticky" data-topbar role="navigation">
	      <ul class="title-area">
	       <li class="name">
	        <h1 class='show-for-medium-up'><a href="#">Data Assurance Hub</a></h1>
          <h1 class='show-for-small-only'><a href="#">Dashboard</a></h1>
	       </li>
	     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
	       <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	      </ul>

	  <section class="top-bar-section">
	    <!-- Right Nav Section -->
	    <ul class="right">
        <li class=""><a href="#" data-reveal-id="gen_invoice" style='font-size:1.0em'>Generate Invoice</a></li>
        <li class=""><a href="#" data-reveal-id="add_fir_member" style='font-size:1.0em'>Add Firs Team</a></li>
      	  <li class="active"><a href="#" data-reveal-id="create_company" style='font-size:1.0em'>Create Company</a></li>
          <li class=""><a href="<?=base_url('index.php/dashboard/logout')?>" style='font-size:1.0em' >Log Out</a></li>
	    </ul>

	    <!-- Left Nav Section -->
	    <ul class="left">
	      <li><a href="#"></a></li>
	    </ul>
	  </section>
	 </nav>
   <div class="callout">
    <div class=' medium-3 large-3 columns sideall' style='background-color:#364150;height:auto;'>
      <div class='row collapse' style='margin-top:30px;color:white;padding-bottom:30px;'>
         <div class='small-3 medium-4 large-4 columns'><img class='profile' src='<?=base_url('/assets/imgs/images.png')?>' style=''/></div>
         <div class='small-9 medium-8 large-8 columns'>
          <ul>
            <li id='welcome'>Welcome,</li>
            <li id='owner'>Ogunrinde Omotayo</li>
          </ul>
         </div>
        </div>
   <div class='row sidelist'style='border-top:1px solid grey;' >
       <a href='#' style='color:#b4bcc8;' ><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-10 medium-10 large-10 columns collapse'> <i class="fa fa-tachometer"></i> &nbsp;Dasboard</div></a>
   </div>
    <div class='row sidelist'style='' >
       <a href='#' style='color:#b4bcc8;' ><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-10 medium-10 large-10 columns collapse'> <i class="fa fa-calendar"></i> &nbsp;Calendar</div></a>
   </div>
   <div class='row sidelist'style='' >
       <a href='#' style='color:#b4bcc8;' ><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-10 medium-10 large-10 columns collapse'> <i class="fa fa-table"></i> &nbsp;Tables</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' ><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-10 medium-10 large-10 columns'><i class="fa fa-bar-chart"></i> &nbsp;Charts</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' ><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-10 medium-10 large-10 columns'><i class="fa fa-line-chart"></i> &nbsp;Data Presentations</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' ><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-10 medium-10 large-10 columns'> <i class="fa fa-map-marker"></i> &nbsp;Maps</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' ><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-10 medium-10 large-10 columns'> <i class="fa fa-cog"></i> &nbsp;Settings</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' ><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-10 medium-10 large-10 columns'> <i class="fa fa-cog"></i> &nbsp;Extra</div></a>
   </div>
    </div>

 <div class="das_con show-for-medium-only medium-9 columns" style='' >
      
       <div class='row' style='background-color:white;font-size:1.5em;padding-top:15px;padding-bottom:15px;padding-left:10px;'>
         <div class='small-12 medium-12 large-12 columns'>Dashboard</div>
        </div>
       <!--div class="row" style=''>
         <div class="small-12 medium-12 large-12 columns">Dashboard</div>
       </div--><hr>
       <div class="row">
        <div class="medium-4 columns icon" style='text-align:center;background-color:#3598dc'>
          <ul>
            <a href="#" class='open_list_campany' data-reveal-id="list_company">
              <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-suitcase fa-2x" style='margin-top:7px;'></i></li>
            <li><div class="row text_on_ass"><div class="small-12 medium-12 large-12 column">Managing: <span id='managing_companies'><?php echo $managing;?></span></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class='icon_name'>Company</li></a>
          </ul>
        </div>
         <div class="medium-4 columns icon_plus" style="text-align:center;background-color:#e7505a">
          <ul>
            <a href="#" class='notifications' data-reveal-id="notif">
              <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-bell-o fa-2x" style='margin-top:7px;'></i></li>
            <li><div class="row text_on_notif"><div class="small-12 medium-6 large-6 column left">Total: <?php echo $total;?></div>
                <div class="small-6 medium-6 large-6 column right">new: <?php echo $new;?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Notifications</li></a>
          </ul>
        </div>
         <div class="small-6 medium-4 large-3 columns icon_end" style="text-align:center;background-color:#32CD32">
          <ul>
            <a href="#" class='company_assess' data-reveal-id="Assessment">
              <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-eye fa-2x" style='margin-top:7px;'></i></li>
            <li><div class="row text_on_ass"><div class="small-6 medium-6 large-6 column left">On: <?php echo $all_going; ?></div>
                <div class="small-6 medium-6 large-6 column right">Done: <?php echo $all_finished; ?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Assessment</li></a>
          </ul>
        </div>
       </div>
        <div class="row" style='margin-top:20px;'>
         <div class="small-4 medium-4 large-3 columns icon_plus" style='text-align:center;background-color:#8E44AD'>
          <ul>
            <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-file-word-o fa-2x" style='color:white;margin-top:7px'></i></li>
            <li><div class="row text_on_team"><div class="small-6 medium-12 large-12 column left">Generated invoice: <?php echo ''?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Invoice</li>
          </ul>
        </div>
        <div class="small-4 medium-4 large-3 columns icon" style='text-align:center;background-color:#3598dc'>
          <ul>
            <a href="#" id='all_files' data-reveal-id="files">
              <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-download fa-2x" style='margin-top:7px;'></i></li>
            <li><div class="row text_on_ass"><div class="small-6 medium-12 large-12 column left">Total:<?php echo $all_files; ?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Files</li></a>
          </ul>
        </div>
       <div class="small-4 medium-4 large-3 columns icon_plus" style="text-align:center;background-color:#8E44AD">
          <ul>
            <a href="#" id='fir_team_member' data-reveal-id="firs_team">
              <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-user fa-2x" style='margin-top:7px;'></i></li>
            <li><div class="row text_on_team"><div class="small-4 medium-4 large-4 column left">Aud:<?php echo $Auditor;?></div>
                <div class="small-4 medium-4 large-4 column left">App:<?php echo $Approval;?></div>
                <div class="small-4 medium-4 large-4 column right">Coord:<?php echo $coordinator;?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Firs Team</li></a>
          </ul>
        </div>
       </div>
   </div>

	 <div class="das_con show-for-large-up medium-9 large-9 columns" style='' >
      
        <div class='row' style='background-color:white;font-size:1.5em;padding-top:15px;padding-bottom:15px;padding-left:10px;'>
         <div class='small-12 medium-12 large-12 columns'>Dashboard</div>
        </div>
       <!--div class="row" style=''>
         <div class="small-12 medium-12 large-12 columns">Dashboard</div>
       </div--><hr>
       <div class="row">
        <div class="small-6 medium-6 large-3 columns icon" style='text-align:center;background-color:#3598dc'>
          <ul>
            <a href="#" class='open_list_campany' data-reveal-id="list_company"><li style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-suitcase fa-2x" style='margin-top:13px;'></i></li>
            <li></li>
            <li class='show-for-medium-only'><i class="fa fa-suitcase fa-3x"></i></li>
              <li class='show-for-small-only'><i class="fa fa-suitcase fa-2x"></i></li>
            <li class='show-for-medium-only'><i class="fa fa-bell-o fa-3x"></i></li>
            <li><div class="row text_on_ass"><div class="small-12 medium-12 large-12 column">Managing: <span id='managing_companies'><?php echo $managing;?></span></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class='icon_name'>Company</li></a>
          </ul>
	        <!--ul>
	        	<a href="#" class='open_list_campany' data-reveal-id="list_company">
              <li class='show-for-large-up' style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-envelope-o fa-3x" style='color:margin-top:13px'></i></li>
              <li class='show-for-medium-only'><i class="fa fa-suitcase fa-3x"></i></li>
              <li class='show-for-small-only'><i class="fa fa-suitcase fa-2x"></i></li>
	        	<li><div class="row text_on_ass"><div class="small-12 medium-12 large-12 column">Managing: <span id='managing_companies'><?php echo $managing;?></span></div>
            </div></li>
	        	<li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
	        	<li class='icon_name'>Company</li></a>
	        </ul-->
        </div>
         <div class="small-6 medium-6 large-3 columns icon_plus" style="text-align:center;background-color:#e7505a">
	        <ul>
	        	<a href="#" class='notifications' data-reveal-id="notif">
              <li class='' style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-bell-o fa-2x" style='margin-top:13px'></i></li>
	        	  <li class='show-for-medium-only'><i class="fa fa-bell-o fa-3x"></i></li>
            <li><div class="row text_on_ass"><div class="small-12 medium-6 large-6 column left">Total: <?php echo $total;?></div>
                <div class="small-6 medium-6 large-6 column right">new: <?php echo $new;?></div>
	        	</div></li>
	        	<li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
	        	<li class="icon_name">Notifications</li></a>
	        </ul>
        </div>
         <div class="small-6 medium-3 large-3 columns icon_end" style="text-align:center;background-color:#3598dc">
	        <ul>
	        	<a href="#" class='company_assess' data-reveal-id="Assessment">
              <li class='' style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-eye fa-2x" style='margin-top:13px'></i></li>
              <li class='show-for-medium-only'><i class="fa fa-eye fa-3x"></i></li>
	        	<li><div class="row text_on_ass"><div class="small-6 medium-6 large-6 column left">On: <?php echo $all_going; ?></div>
                <div class="small-6 medium-6 large-6 column right">Done: <?php echo $all_finished; ?></div>
	        	</div></li>
	        	<li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
	        	<li class="icon_name">Assessment</li></a>
	        </ul>
        </div>
         <div class="small-4 medium-3 large-3 columns icon_plus" style='text-align:center;background-color:#e7505a'>
          <ul>
            <li class='' style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-file-word-o fa-2x" style='color:white;margin-top:13px'></i></li>
            <li class='show-for-medium-only'><i class="fa fa-eye fa-3x"></i></li>
            <li><div class="row text_on_ass"><div class="small-6 medium-12 large-12 column left">Generated invoice: <?php echo ''?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Invoice</li>
          </ul>
        </div>
       </div>
        <div class="row" style='margin-top:20px;'>
        <div class="small-4 medium-3 large-3 columns icon" style='text-align:center;background-color:#3598dc'>
	        <ul>
	        	<a href="#" id='all_files' data-reveal-id="files">
              <li class='' style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-download fa-2x" style='margin-top:13px'></i></li>
              <li class='show-for-medium-only'><i class="fa fa-download fa-3x"></i></li>
	        	<li><div class="row text_on_ass"><div class="small-6 medium-12 large-12 column left">Total:<?php echo $all_files; ?></div>
            </div></li>
	        	<li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
	        	<li class="icon_name">Files</li></a>
	        </ul>
        </div>
         <div class="small-4 medium-3 large-3 columns icon_plus" style='text-align:center;background-color:#e7505a'>
          <ul>
            <li class='' style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-envelope-o fa-2x" style='color:white;margin-top:13px'></i></li>
            <li class='show-for-medium-only'><i class="fa fa-envelope-o fa-3x"></i></li>
            <li><div class="row text_on_ass"><div class="small-6 medium-12 large-12 column left">Total:500</div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Send Email</li>
          </ul>
        </div>
         <div class="small-4 medium-3 large-3 columns icon" style="text-align:center;background-color:#3598dc">
	        <ul>
	        	<li class='' style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-user fa-2x" style='margin-top:13px'></i></li>
            <li class='show-for-medium-only'><i class="fa fa-user fa-3x"></i></li>
	        	<li><div class="row text_on_ass"><div class="small-6 medium-12 large-12 column left">Assigned Coord:500</div>
            </div></li>
	        	<li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
	        	<li class="icon_name">Coordinators</li>
	        </ul>
        </div>
         <div class="small-4 medium-3 large-3 columns icon_plus" style="text-align:center;background-color:#e7505a">
	        <ul>
	        	<a href="#" id='fir_team_member' data-reveal-id="firs_team">
              <li class='' style='border:2px solid white;width:60px;height:60px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-user fa-2x" style='margin-top:13px'></i></li>
              <li class='show-for-medium-only'><i class="fa fa-user fa-3x"></i></li>
	        	<li><div class="row text_on_ass"><div class="small-4 medium-4 large-4 column left">Aud:<?php echo $Auditor;?></div>
                <div class="small-4 medium-4 large-4 column left">App:<?php echo $Approval;?></div>
                <div class="small-4 medium-4 large-4 column right">Coord:<?php echo $coordinator;?></div>
	        	</div></li>
	        	<li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
	        	<li class="icon_name">Firs Team</li></a>
	        </ul>
        </div>
       </div>
	 </div>
  </div>
   <div class='show-for-small-only'>
      <div class='clearfix'><a href="#" class='right' id='show_tool'>qwer<i class="fa fa-bell-o fa-2x"></i></a></div>
      <div class='row'>
           <div class="small-6 columns icon" style='text-align:center;background-color:#3598dc'>
           <ul>
            <a href="#" class='open_list_campany' data-reveal-id="list_company">
              <li class='show-for-small-only'><i class="fa fa-suitcase fa-2x"></i></li>
            <li><div class="row text_on_ass"><div class="small-12 medium-12 large-12 column">Managing: 
              <span id='managing_companies'><?php echo $managing;?></span></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class='icon_name'>Company</li></a>
           </ul>
           </div>
           <div class="small-6 medium-3 large-3 columns icon_plus" style="text-align:center;background-color:#e7505a">
          <ul>
            <a href="#" class='notifications' data-reveal-id="notif">
              <li class=''><i class="fa fa-bell-o fa-2x"></i></li>
            <li><div class="row text_on_notif"><div class="small-6  left">Total: <?php echo $total;?></div>
                <div class="small-6 column right">new: <?php echo $new;?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Notifications</li></a>
          </ul>
        </div>
      </div>
         <div class='row'>
           <div class="small-6 columns icon" style='text-align:center;background-color:#32CD32'>
           <ul>
            <a href="#" class='company_assess' data-reveal-id="Assessment">
              <li class=''><i class="fa fa-eye fa-2x"></i></li>
            <li><div class="row text_on_ass"><div class="small-6 medium-6 large-6 column left">On: <?php echo $all_going; ?></div>
                <div class="small-6 medium-6 large-6 column right">Done: <?php echo $all_finished; ?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class='icon_name'>Assessment</li></a>
           </ul>
           </div>
           <div class="small-6 medium-3 large-3 columns icon_plus" style="text-align:center;background-color:#8E44AD">
          <ul>
            <a href="#" id='notifications' data-reveal-id="notif">
              <li class=''><i class="fa fa-bell-o fa-2x"></i></li>
           <li><div class="row text_on_team"><div class="small-6 medium-12 large-12 column left">Invoice: <?php echo ''?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Invoice</li></a>
          </ul>
        </div>
      </div>
       <div class="row">
        <div class="small-6 columns icon" style='text-align:center;background-color:#3598dc'>
          <ul>
            <a href="#" id='all_files' data-reveal-id="files">
              <li class=''><i class="fa fa-download fa-2x"></i></li>
            <li><div class="row text_on_ass"><div class="small-6 medium-12 large-12 column left">Total:<?php echo $all_files; ?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Files</li></a>
          </ul>
        </div>
        <div class="small-6 columns icon_plus" style="text-align:center;">
          <ul>
            <a href="#" id='fir_team_member' data-reveal-id="firs_team">
              <li class=''><i class="fa fa-user fa-2x"></i></li>
            <li><div class="row text_on_team"><div class="small-4 medium-4 large-4 column left">Aud:<?php echo $Auditor;?></div>
                <div class="small-4 medium-4 large-4 column left">App:<?php echo $Approval;?></div>
                <div class="small-4 medium-4 large-4 column right">Coord:<?php echo $coordinator;?></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide2'></div></div></li>
            <li class="icon_name">Firs Team</li></a>
          </ul>
        </div>
         </div>
   </div>
	  <div id="add_fir_member" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row add_company"><div class="small-12 medium-12 large-12 columns">ADD FIRS TEAM</div></div>
          <div class="row" style='margin-top:20px;'><div class="small-12 medium-12 large-12 columns"><input id='firs_team_name' type='text' placeholder="Name"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input id='firs_team_phone' type='text' placeholder="Phone Number"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input id='firs_team_email' type='email' placeholder="email"/></div></div>
          <div class='row'><div class="small-12 medium-12 large-12 columns">
             <select id='category'>
              <option>Select firs team</option>
              <option value='coordinator'>Coordinator</option>
              <option value='auditor'>Auditor</option>
              <option value='approval'>Approval</option>
            </select>
          </div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns">
            <select id='region'>
              <option>Attach Region</option>
              <option value='lagos'>Lagos</option>
            </select>
          </div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><a href="#" class="button expand success add_firs_team">Add Firs team</a></div></div>

		  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
	    </div>
        <div id="gen_invoice" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row add_company"><div class="small-12 medium-12 large-12 columns">GENERATE INVOICE</div></div>
          <div class='row'><div class="small-12 medium-12 large-12 columns">
             <select id='forcompany'>
            </select>
          </div></div>
          <div class="row">
            <div class="small-12 medium-12 large-12 columns">
              
            </div>
          </div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><a href="#" class="button expand success add_firs_team">Generate Invoice</a></div></div>

      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
	    
	    <div id="create_company" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row add_company"><div class="small-12 medium-12 large-12 columns">ADD COMPANY</div></div>
          <div class="row" style='margin-top:20px;'><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_name' placeholder="company Name"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_add'  placeholder="company Address"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id="company_tin" placeholder="tin id"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_rc' placeholder="RC Number"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_bvn' placeholder="BVN"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id ='company_tax_zone' placeholder="tax Zone(Region)"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_admin' placeholder="company administrator Name"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='email' id='company_email' placeholder="company administrator email"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_phone' placeholder="company administrator Phone"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><a href="#" id='add_company' class="button expand success">Add Company</a></div></div>

		  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
	    </div>
            <div id="list_company" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row" style=''><div class="small-3 medium-3 large-3 columns header">COMPANIES</div></div>
          <div class='scroll-y'>
            <div id='render_company'>
          <div style='width:98%;tex-align:center;margin-left:auto;margin-right:auto;'>
            <div class="row collapse" style='margin-top:20px;'>
            <div class="small-2 medium-2 large-2 columns" style='text-align:center;' id='company_icon'>C</div>
            <div class="small-10 medium-10 large-10 columns">
            <ul>
                <li style='font-size:1.5em;margin-top:-9px;'>Company Name</li>
                <li style='font-size:0.8em;margin-top:-9px;'>Company Address</li>
                <li style='font-size:0.8em;margin-top:-4px;'>Tax Zone</li>
            </ul>
            </div></div>
           </div> 
            <div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div>
          </div></div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
      <div id="files" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row" style=''><div class="small-3 medium-3 large-3 columns  header">Uploaded Files</div></div>
          <div class='scroll-y'>
          <div id='render_files' style='width:98%;tex-align:center;margin-left:auto;margin-right:auto;margin-top:25px;'>
            <div class="row collapse" style='margin-top:20px;'>
            <div class="small-2 medium-2 large-2 columns" style='text-align:center;' id='company_icon'>C</div>
            <div class="small-10 medium-10 large-10 columns">
            <ul>
                <li style='font-size:1.5em;margin-top:-9px;'>Company Name</li>
                <li style='font-size:0.8em;margin-top:-9px;'>Company Address</li>
                <li style='font-size:0.8em;margin-top:-4px;'>Tax Zone</li>
            </ul>
            </div></div>
           </div> 
            <div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div>
          </div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
      <div id='firs_team' class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row" style=''><div class="small-3 medium-3 large-3 columns  header">FIRS TEAM</div></div>
          <div class='container'>
          <div id='render_team' style='width:98%;tex-align:center;margin-left:auto;margin-right:auto;margin-top:5px;'>  
        </div></div>
        <div id='taskpage' class='toremove hide' style='margin-top:15px;'>
          <div class='row'><div class='small-12 medium-12 large-12 columns'><select id='task_type'><option>Select Assign</option>
          <option value='audit'>Audit</option>
          </select></div></div>
          <div class='row'><div class='small-12 medium-12 large-12 columns'><select id='company_to_audit'></select></div></div>
          <div class='row' style=''><div class='small-12 medium-12 large-12 columns'>Financial Statement Period</div></div><hr style='margin-top:-20px;'>
          <div class='row' style='margin-top:10px;margin-bottom:0px;'>
             <div class='small-4 medium-4 large-4 columns'>
               <select id='begin'>
                <option value="">Begin</option>
                 <option value='January'>January</option>
                  <option value='February'>February</option>
                   <option value='March'>March</option>
                    <option value ='April'>April</option>
                     <option value='May'>May</option>
                      <option value='June'>June</option>
                       <option value='July'>July</option>
                        <option value='August'>August</option>
                         <option value='October'>October</option>
                          <option value='November'>November</option>
                           <option value='December'>December</option>
               </select>
             </div>
             <div class='small-4 medium-4 large-4 columns'>
               <select id='end'>
                <option value="">End</option>
                 <option value='January'>January</option>
                  <option value='February'>February</option>
                   <option value='March'>March</option>
                    <option value ='April'>April</option>
                     <option value='May'>May</option>
                      <option value='June'>June</option>
                       <option value='July'>July</option>
                        <option value='August'>August</option>
                         <option value='October'>October</option>
                          <option value='November'>November</option>
                           <option value='December'>December</option>
               </select>
             </div>
             <div class='small-4 medium-4 large-4 columns'>
               <select id='year'>
                <option value="">Year</option>
                 <option value='2016'>2016</option>
                  <option value='2017'>2017</option>
                   <option value='2018'>2018</option>
                    <option value='2019'>2019</option>
                     <option value='2020'>2020</option>
                      <option value='2021'>2021</option>
                       <option value='2022'>2022</option>
                        <option value='2023'>2023</option>
                         <option value='2024'>2024</option>
                          <option value='2024'>2024</option>
                           <option value='2025'>2025</option>
               </select>
             </div>
          </div>
          <div class='row'><div class='small-12 medium-12 large-12 columns'>Audit Period</div></div><hr style='margin-top:-20px;'>
             <div class='row' style='margin-top:0px;margin-bottom:0px;'>
             <div class='small-3 medium-2 large-2 columns'>
               <select id='day_from'>
               </select>
             </div>
             <div class='small-3 medium-3 large-3 columns'>
               <select id='from_month'>
               </select>
             </div>
             <div class='small-2 medium-2 large-2 columns'>
               <select id='day_to'>
               </select>
             </div>
             <div class='small-2 medium-3 large-3 columns'>
               <select id='month_to'>
               </select>
             </div>
             <div class='small-3 medium-2 large-2 columns'>
               <select id='audit_yr'>
                <option value="">Year</option>
                 <option value='2016'>2016</option>
                  <option value='2017'>2017</option>
                   <option value='2018'>2018</option>
                    <option value='2019'>2019</option>
                     <option value='2020'>2020</option>
                      <option value='2021'>2021</option>
                       <option value='2022'>2022</option>
                        <option value='2023'>2023</option>
                         <option value='2024'>2024</option>
                          <option value='2024'>2024</option>
                           <option value='2025'>2025</option>
               </select>
             </div>
          </div>
          <div class='row'><div class='small-12 medium-12 large-12 columns'><a href="#" id='task_to_do' class='success button expand'>Assign task</a></div></div>
          </div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
          <div id="notif" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row" style=''><div class="small-3 medium-3 large-3 columns  header">Notifications</div></div>
          <div class='scroll-y'>
            <div id='render_notif'>
          <div style='width:98%;tex-align:center;margin-left:auto;margin-right:auto;'>
            <div class="row collapse" style='margin-top:20px;'>
            <div class="small-2 medium-2 large-2 columns" style='text-align:center;' id='company_icon'>C</div>
            <div class="small-10 medium-10 large-10 columns">
            <ul>
                <li style='font-size:1.5em;margin-top:-9px;'>Company Name</li>
                <li style='font-size:0.8em;margin-top:-9px;'>Company Address</li>
                <li style='font-size:0.8em;margin-top:-4px;'>Tax Zone</li>
            </ul>
            </div></div>
           </div> 
            <div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div>
          </div>
        </div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
         
        <div id="Assessment" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <div class="row" style=''><div class="small-3 medium-3 large-3 columns  header">Assessment</div></div> 
        <div class="row ass_tobbar" style='margin-top:30px;'>
          <div class="small-6 medium-6 large-6 columns ongoingAss" id='OngoingA'>
            <a href="#" id="Ogoing-page" style='color:#FFFFFF;'>Ongoing</a>
          </div>
          <div class="small-6 medium-6 large-6 columns finishedAss" id='FinishedA'>
            <a href="#" id="finished-page" style='color:#FFFFFF;'>Finished</a>
          </div>
          </div>
             <div class="ass_container">
              <div id='header_chat'></div>
             <div class='firongoingAssessment' style='width:98%;;margin-left:auto;margin-right:auto;'>
           </div>
            <div id='forchats'></div>
            <div class='hide'><input type='file' name=files[] id='sharefile'/></div>
        </div>
        <div id='msg_container'class="hide"></div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
	</div>
        
	<script>
    $(document).foundation();
  </script>
</body>
</html>