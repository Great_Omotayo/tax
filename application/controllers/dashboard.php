<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("dashboard_model");
    $this->load->library('send_email');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->view=false;
    $this->update_record();
		if(isset($this->session->userdata['user_details'][0])){
			if($this->session->userdata['user_details'][0]->is_fir_admin == 1){
        $this->load->view('dashboard/adminpage');
		   }
       else if($this->session->userdata['user_details'][0]->is_fir_coordinator == 1){
        $response = $this->dashboard_model->get_all_assigned_task();
        $to_change_pass = $this->session->userdata['user_details'][0]->to_change_pass;
        $data = array('orders'=>$response,'to_pass'=>$to_change_pass);
        $this->load->view('dashboard/coordinator',$data);
       }
       else if($this->session->userdata['user_details'][0]->is_fir_auditor == 1){
        $response = $this->dashboard_model->get_auditor_assigned_task();
        $to_change_pass = $this->session->userdata['user_details'][0]->to_change_pass;
        $data = array('assess_orders'=>$response,'to_pass'=>$to_change_pass);
        $this->load->view('dashboard/auditor',$data);
       }
       else if($this->session->userdata['user_details'][0]->is_company_admin == 1){
        $to_change_pass = $this->session->userdata['user_details'][0]->to_change_pass;
        $response = $this->dashboard_model->get_all_company_report();
        $resp = $this->dashboard_model->get_company_on_ass($assess="");
        $p=$q=0;
        if(count($resp)>0){
          for ($i=0;$i<count($resp);$i++){
            if($resp[$i]['status'] =='Work in Progress'||$resp[$i]['status'] =='Open'){
              $p++;
            }
            else if($resp[$i]['status'] =='Closed'){
              $q++;
            }
          }
        }
        $data = array('reports'=>count($response),'On'=>$p,'fin'=>$q,'to_pass'=>$to_change_pass,'orders'=>$resp);
        $this->load->view('dashboard/company',$data);
       }
		  
	}
   else {
        redirect('');
      }
   }   
	
		public function uploadfile(){
			$this->view =false;
      $data = $this->input->post();
			if(isset($_FILES['file'])){
				$files="";
            foreach($_FILES['file']['tmp_name'] as $f=>$tmp) {
		      $errors= array();
		      $file_name = str_replace(' ','_',$_FILES['file']['name'][$f]);
		      $file_size =$_FILES['file']['size'][$f];
		      $file_tmp =$_FILES['file']['tmp_name'][$f];
		      $file_type=$_FILES['file']['type'][$f];
		      $file_ext=strtolower(end(explode('.',$_FILES['file']['name'][$f])));
		      
		      $expensions= array("jpeg","jpg","png","pdf","docx","doc");
		      
			      if(in_array($file_ext,$expensions)=== false){
			         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
			      }

		      if(empty($errors)==true && ($file_ext == 'pdf' || $file_ext == 'docx'|| $file_ext == 'doc' || $file_ext =='txt')){
	           move_uploaded_file($file_tmp,"assets/upload/".$file_name);
	                if($files !=""){
	                	$files .= ';';
	                }
                    $files .= $file_name; 
				}
		}
		if(empty($errors)==true){
       $res = $this->processuploadfile($data,$files);
		$this->output->set_output(json_encode($res));
		}
		else{}
		//$this->output->set_output(json_encode(true));
	}
  }

    public function all_companies(){
    $this->view =false;
    $this->load->model("dashboard_model");
    $response = $this->dashboard_model->get_companies();
    $this->output->set_output(json_encode($response));
    
  }
  public function all_auditors(){
      $this->view =false;
    $this->load->model("dashboard_model");
    $response = $this->dashboard_model->get_auditors();
    $this->output->set_output(json_encode($response)); 
  }
  public function generate_digit(){
    $this->view = false;
    $number = mt_rand( 1000000000, 9999999999);
    return $number;
    }
  public function assign_task(){
    $this->view =false;
    $data = $this->input->post();
    $sender_id = $this->session->userdata['user_details'][0]->id;
    /*$data['auditor_id'] ="6";
    $data['begin'] = 'Feb';
    $data['end'] = 'Mar';
    $data['year'] ='2014';
    $data['task'] ='audit';
    $data['company_id'] ='1';
    $data['desc'] = 'financial statement';
     $data['audit_time']='12;March;17;May;2016'; */
    //if($data['desc'] =='financial statement'){
      $fin_period = serialize($data['begin'].';'.$data['end'].';'.$data['year']);
    //}
    $status = $this->getstatus($data['audit_time']);
    $gen_pass =$this->generate_digit();
    $this->db->select('email,firstname');
    $auditor = $this->db->get_where('all_users',array('id'=>$data['auditor_id']));
    $audName = '';
    $audemail ='';
         foreach ($auditor->result() as $value) {
          $audName = $value->firstname;
          $audemail = $value->email;
          break;
         }
    $this->db->select('email,Name');
    $this->db->join('company','company.company_admin_id = all_users.id');
    $company = $this->db->get_where('all_users',array('company.id'=>$data['company_id']));
    $comName = '';
    $comemail = '';
         foreach ($company->result() as $value) {
          $comName = $value->Name;
          $comemail = $value->email;
          break;
         }
    $this->db->trans_start();
    $new = array('task'=>'audit','task_to_id'=>$data['auditor_id'],'task_on_id'=>$data['company_id'],'assessment_id'=>$gen_pass,'assigned_by_id'=>$sender_id,'financial_statement_period'=>$fin_period,'audit_time'=>serialize($data['audit_time']),'created'=>time());
    $this->db->insert('task_for_firs_team',$new);
    $assessment = array('assessment_id'=>$gen_pass,'auditor_id'=>$data['auditor_id'],'status'=>$status,'created'=>time());
    $this->db->insert('assessment',$assessment);
    $res = $this->db->trans_complete();
    if($res==true)
    $to_send_to_user = array('status'=>$status,'assessment_id'=>$gen_pass,'auditor'=>$auditor->audName,'company'=>$comName,'success'=>'true');
    else
    $to_send_to_user = array('success'=>'false');
    $this->preparemessage($auditor->$audemail,$audName,$comemail,$comName,$data['audit_time']);

    //$assessment = array('assessment_id'=>$gen_pass,'auditor_id'=>$data['auditor_id'],'status'=>)
    
    $this->output->set_output(json_encode($to_send_to_user));
  }
   public function getstatus($audit_time){
    $this->view = false;
    //$audit_time='28;March;17;May;2016';
    $month  = array('January','February','March','April','May','June','July','August','September','October','November','December');
    $date = explode(';',$audit_time);
    $starting_date  = $date[0].'-'.(array_search($date[1], $month)+1).'-'.$date[4];
    $timestamp = strtotime($starting_date);
    //echo $timestamp.';'.time();
    //echo array_search($date[1], $month);
    if($timestamp>time()){
      return 'Planned';
    }
    else if($timestamp == time())
      {
        return 'Work in Progress';
      }
      else{
        return 'Open';
      }
     return '';
   }

   public function preparemessage($auditor_email,$auditor_firstname,$company_email,$company_name,$audit_time){
    $this->view =false;
    $break =explode(';',$audit_time);
    $coordName = $this->session->userdata['user_details'][0]->firstname;
    $coordEmail  = $this->session->userdata['user_details'][0]->email;
    $coordPhone = $this->session->userdata['user_details'][0]->phone;
    $to_auditor = "<div style='font-size:1.2em;margin-top:20px;margin-bottom:40px;'><p>Hello ".$auditor_firstname.",</p></div>";
    $to_auditor .= "<div style='font-size:1.0em'><p>This is to notify you that FIRS Coordinator(Name:".$coordName.", Email: ".$coordEmail.",Phone: ".$coordPhone.") has delegated a task to you to audit the financial Statement of  ".$company_name."";
    $to_auditor .= "This Assessment has been scheduled to take place between ".$break[0]." of ".$break[1]." to ".$break[2]."  ".$break[3]."  ".$break[4]."</p></div>";
    
    $to_company = "<div style='font-size:1.2em;margin-top:20px;margin-bottom:40px;'><p>Hello,</p></div>";
    $to_company .= "<div style='font-size:1.0em'><p>This is to notify you that FIRS Auditor has been assigned to Audit your Company financial Statement";
    $to_company .= "This Assessment has been scheduled to take place between ".$break[0]." of ".$break[1]." to ".$break[2]."  ".$break[3]." ".$break[4].".</p></div>";
    $to_company .= "Your maximum cooperation will be require within the stipulated period.</p></div>";
    $to_send = $this->load->view('mail_container/mail_template',array('mail'=>$to_auditor), true);
    $response = $this->send_email->send_mail($auditor_email,'Audit' ,$to_send);
    $to_send_to_company = $this->load->view('mail_container/mail_template',array('mail'=>$to_company), true);
    $response = $this->send_email->send_mail($company_email,'Request to Audit' ,$to_send_to_company);
   }
   public function assigned_task(){
    $this->view = false;
    $response = $this->dashboard_model->get_assigned_task();
    $this->output->set_output(json_encode($response));
   }
   public function get_auditor_task(){
    $this->view = false;
     $response = $this->dashboard_model->get_auditor_assigned_task();
    $this->output->set_output(json_encode($response));
   }
   public function request(){
    $this->view = false;
    $data = $this->input->post();
    /*$data['company_id'] = '1';
    $data['text'] = 'wxcfghkiofdsxkbgfh';
    $data['assessment_id'] = "1308486924";
    $data['request'] ='show_report';*/
    $this->db->select('*');
    $t = $this->db->get_where('assessment',array('Assessment_id'=>$data['assessment_id']));
    $stage = '';
       foreach ($t->result() as $value) {
          $stage = $value->stage;
          break;
       }
    if($stage != ''){
      $new = serialize(unserialize($stage).';'.$data['request']);
    }
    else {
      $new = serialize($data['request']);
    }
    $this->db->trans_start();
    $update = array('stage'=>$new);
    $this->db->where('Assessment_id',$data['assessment_id']);
    $r = $this->db->update('assessment',$update);
    if(isset($data['company_id'])){
      $this->chats($data);
    }
    $this->db->trans_complete();
    $this->output->set_output(json_encode($r));
   }
   public function get_company_ongoing(){
    $this->view = false;
    $data = $this->input->post();
    $assess = $data['assess'];
    $response = $this->dashboard_model->get_company_on_ass($assess);
    $this->output->set_output(json_encode($response));
    
   }
   public function get_ongoing_for_auditor(){
    $this->view = false;
    $data= $this->input->post();
    $response = $this->dashboard_model->get_ongoing_ass_for_auditor();
    $this->output->set_output(json_encode($response));
   }
   public function get_one_ongoing_for_auditor(){
    $this->view = false;
    $data= $this->input->post();
    $response = $this->dashboard_model->get_one_ongoing_ass_for_auditor($data['assess']);
    $this->output->set_output(json_encode($response));
   }
  public function processuploadfile($data,$files){
  	$this->view =false;
    $id = $this->session->userdata['user_details'][0]->id;
    //$data = $this->input->post();
    /*$data['request'] ='teet';
    $data['assessment_id'] ='1308486924';
    $files='qwerty;qwert';
    $data['fin_statement_period'] ='ewrtyuio';
    $data['filename'] ='rate';*/
    //return true;
    $this->db->select('stage');
    $t = $this->db->get_where('assessment',array('Assessment_id'=>$data['assessment_id']));
    if($this->session->userdata['user_details'][0]->is_company_admin == 1){
    $this->db->select('id');
    $y= $this->db->get_where('company',array('company_admin_id'=>$id));
    $id = '';
    $stage = '';
       foreach ($y->result() as $value) {
          $id = $value->id;
          $stage = $value->stage;
          break;
       }
    //$id =$y->result()[0]->id;
    }
    else if($this->session->userdata['user_details'][0]->is_fir_auditor == 1){
      $id = $this->session->userdata['user_details'][0]->id;
    }
    $this->db->trans_start();
    if($stage !="")
      $new = serialize(unserialize($stage).';'.$data['request']);
    else
      $new = serialize($data['request']);
    $update = array('stage'=>$new);
    $this->db->where('Assessment_id',$data['assessment_id']);
    $r = $this->db->update('assessment',$update);
    $insert = array('company_id'=>$id,'assessment_id'=>$data['assessment_id'],'files'=>$files,'financial_record_period'=>serialize($data['fin_statement_period']),'filename'=>$data['filename'],'created'=>time());
    $this->db->insert('uploaded_file',$insert);
    $res = $this->db->trans_complete();
    return $res;
  }
  public function gen_vat(){
    $this->view = false;
    $data = $this->input->post();
    $this->db->select('stage');
    $t = $this->db->get_where('assessment',array('Assessment_id'=>$data['assessment_id']));
    $stage = '';
    foreach ($t->result() as $value) {
      $stage = $value->stage;
    }
    if(count($t->result())>0){
      if($stage ==""){

        return false;
      }
    }
    else{
      return false;
    }
    //Calculate VAT and Update
    $wrap = serialize(unserialize($stage).';vat_generated');
    $update = array('Generated_vat'=>'10000000','stage'=>$wrap);
    $this->db->where('Assessment_id',$data['assessment_id']);
    $this->db->update('assessment',$update);
    $this->output->set_output(json_encode('10000000'));

  }
  public function getuploadedfile(){
    $this->view = false;
    $data = $this->input->post();
    $response = $this->dashboard_model->get_files($data['assessment_id'],$data['whichfile']);
    $this->output->set_output(json_encode($response));
  }
  public function downloaded(){
    $this->view = false;
    $data = $this->input->post();
  }
  public function getallfile(){
    $this->view = false;
    $data = $this->input->post();
    $this->db->select('files');
    $res = $this->db->get_where('uploaded_file',array('assessment_id'=>$data['assessment_id']));
    $this->output->set_output(json_encode($res->result()));
  }
  public function savereport(){
    $this->view =false;
    $id = $this->session->userdata['user_details'][0]->id;
    $data= $this->input->post();
    /*$data['company_id'] = '1';
    $data['content'] = 'wxcfghkiofdsxkbgfh';
    $data['assessment_id'] = "1077300495";
    $data['request'] ='show_report';*/
    $this->db->select('*');
    $this->db->join('task_for_firs_team','task_for_firs_team.assessment_id=assessment.Assessment_id');
    $t = $this->db->get_where('assessment',array('assessment.Assessment_id'=>$data['assessment_id']));
    $stage = '';
    $task_on_id = '';
    $assigned_by_id = '';
    foreach ($t->result() as $value) {
      $stage = $value->stage;
      $task_on_id = $value->task_on_id;
      $assigned_by_id = $value->assigned_by_id;
      break;
    }
    if($stage != ''){
      $new = serialize(unserialize($stage).';'.$data['request']);
    }
    else {
      $new = serialize($data['request']);
    }
    $this->db->trans_start();
    $report = array('assessment_id'=>$data['assessment_id'],'auditor_id'=>$id,'company_id'=>$task_on_id,'report'=>$data['content'],'created'=>time());
    $this->db->insert('report',$report);
    $update = array('stage'=>$new);
    $this->db->where('Assessment_id',$data['assessment_id']);
    $this->db->update('assessment',$update);
    $res = $this->db->trans_complete();
    if($data['request']=='show_report'){
      //to company
    $this->db->select('email');
    $this->db->join('all_users','all_users.id=company.company_admin_id');
    $com_email = $this->db->get_where('company',array('company.id'=>$task_on_id));
    $email = '';
    foreach ($com_email->result() as $value) {
      $email = $value->email;
      break;
    }
    $to_send = $this->load->view('mail_container/mail_template',array('mail'=>$data['content']), true);
    $response = $this->send_email->send_mail($email,'Report from Audit' ,$to_send);
    //to coord
    $this->db->select('email');
    $this->db->join('all_users','all_users.id=task_for_firs_team.assigned_by_id');
    $coord_email = $this->db->get_where('task_for_firs_team',array('task_for_firs_team.assigned_by_id'=>$assigned_by_id));
    $to_coord = $this->load->view('mail_container/mail_template',array('mail'=>$data['content']), true);
    $coordemail = '';
       foreach ($to_coord->result() as $value) {
          $coordemail = $value->email;
          break;
       }
    $this->send_email->send_mail($email,'Report from Audit' ,$to_coord);
    }

    $this->output->set_output(json_encode($res));
    }
     public function chats($data){
      $this->view =false;
      $id = $this->session->userdata['user_details'][0]->id;
      //$data=$this->input->post();
      $msg = array('sender_id'=>$id,'receiver_id'=>$data['company_id'],'message'=>$data['text'],'assessment_id'=>$data['assessment_id'],'type'=>$data['whichfile'],'created'=>time());
      $e= $this->db->insert('chats',$msg);
      $this->db->select('*');
      $t = $this->db->get_where('assessment',array('Assessment_id'=>$data['assessment_id']));
      $stage = '';
       foreach ($t->result() as $value) {
          $stage = $value->stage;
          break;
       }
      if($stage != ''){
        $break = explode(';',unserialize($stage));
        if($break[(count($break)-1)] !=$data['request']){
          $new = serialize(unserialize($stage).';'.$data['request']);
                $update = array('stage'=>$new);
                $this->db->where('Assessment_id',$data['assessment_id']);
                $r = $this->db->update('assessment',$update);
        }
      }
       else{
         $new = serialize(unserialize($stage).';',$data['request']);
                $update = array('stage'=>$new);
                $this->db->where('Assessment_id',$data['assessment_id']);
                $r = $this->db->update('assessment',$update);
      }
      $this->db->select('email');
      $this->db->join('task_for_firs_team','task_for_firs_team.task_on_id=company.id');
      $this->db->join('all_users','all_users.id=company.company_admin_id');
      $com_email = $this->db->get_where('company',array('company.id'=>$data['company_id']));
      $comEmail = '';
       foreach ($com_email->result() as $value) {
          $comEmail = $value->email;
          break;
       }
      $msg = "<div style='font-size:1.2em;margin-bottom:20px;'><p>Hello,</p></div>";
      $msg .= "<div style='font-size:1.0em'><p>You have a message from FIRS Auditor, as regard the ongoing financial statement auditing. Please login to your account to reply the FIRS Auditor.</p><div>";
      $msg .="<div style='text-align:center;font-size:1.2em;margin-top:30px;width:40%;margin-left:auto;margin-right:auto;'><p style='color:white;background-color:#3598dc;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;border-radius:5px;'>Click to Log In</p></div>";
      $to_company = $this->load->view('mail_container/mail_template',array('mail'=>$msg), true);
      $sta = $this->send_email->send_mail($comEmail,'Report from Auditor' ,$to_company);
      $this->output->set_output(json_encode($sta));
    }
    public function msg(){
      $this->view =false;
      $id = $this->session->userdata['user_details'][0]->id;
      $data=$this->input->post();
      /*$data['company_id'] ='1';
      $data['text'] = '120';
      $data['assessment_id'] = '1197646107';
      $data['whichfile'] ='Generated VAT';
      $data['request'] = 'share generated vat with company';*/
      /*if($data['whichfile']=='Generated VAT'){
        $this->db->select('type');
        $r = $this->db->get_where('chats',array('assessment_id'=>$data['assessment_id'],'type'=>'Generated VAT'));
        if(count($r->result())>0){
                $this->db->delete('chats', array('assessment_id' => $data['assessment_id'],'type'=>'Generated VAT'));
        }
      }*/
      $msg = array('sender_id'=>$id,'receiver_id'=>$data['company_id'],'message'=>$data['text'],'assessment_id'=>$data['assessment_id'],'type'=>$data['whichfile'],'created'=>time());
      $e= $this->db->insert('chats',$msg);
      $this->db->select('*');
      $t = $this->db->get_where('assessment',array('Assessment_id'=>$data['assessment_id']));
      $stage = '';
       foreach ($t->result() as $value) {
          $stage = $value->stage;
          break;
       }
      if(count($t->result())>0){
      if($stage != ''){
        $break = explode(';',unserialize($stage));
        if($break[(count($break)-1)] !=$data['request']){
          $new = serialize(unserialize($stage).';'.$data['request']);
                $update = array('stage'=>$new);
                $this->db->where('Assessment_id',$data['assessment_id']);
                $r = $this->db->update('assessment',$update);
        }
      }
      else{
         $new = serialize($data['request']);
                $update = array('stage'=>$new);
                $this->db->where('Assessment_id',$data['assessment_id']);
                $r = $this->db->update('assessment',$update);
      }
     }
      $this->db->select('email');
      $this->db->join('task_for_firs_team','task_for_firs_team.task_on_id=company.id');
      $this->db->join('all_users','all_users.id=company.company_admin_id');
      $com_email = $this->db->get_where('company',array('company.id'=>$data['company_id']));
      $email = '';
       foreach ($com_email->result() as $value) {
          $email = $value->email;
          break;
       }
      $msg = "<div style='font-size:1.2em;margin-bottom:20px;'><p>Hello,</p></div>";
      $msg .= "<div style='font-size:1.0em'><p>You have a message from FIRS Auditor, as regard the ongoing financial statement auditing. Please login to your account to reply the FIRS Auditor.</p><div>";
      $msg .="<div style='text-align:center;font-size:1.2em;margin-top:30px;width:40%;margin-left:auto;margin-right:auto;'><p style='color:white;background-color:#3598dc;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;border-radius:5px;'>Click to Log In</p></div>";
      $to_company = $this->load->view('mail_container/mail_template',array('mail'=>$msg), true);
      $sta = $this->send_email->send_mail($email,'Report from Auditor' ,$to_company);
      $this->output->set_output(json_encode($sta));
    }
    public function reply_msg(){
      $this->view =false;
      $data=$this->input->post();
      $id= $this->session->userdata['user_details'][0]->id;
      $msg = array('sender_id'=>$data['company_id'],'receiver_id'=>$data['auditor_id'],'message'=>$data['text'],'assessment_id'=>$data['assessment_id'],'type'=>$data['whichfile'],'created'=>time());
      $e= $this->db->insert('chats',$msg);
      $this->db->select('*');
      $t = $this->db->get_where('assessment',array('Assessment_id'=>$data['assessment_id']));
      $stage = '';
       foreach ($t->result() as $value) {
          $stage = $value->stage;
          break;
       }
      if($stage != ''){
        $break = explode(';',unserialize($stage));
        if($break[(count($break)-1)] !='chats_from_company'){
          $new = serialize(unserialize($stage).';'.$data['request']);
                $update = array('stage'=>$new);
                $this->db->where('Assessment_id',$data['assessment_id']);
                $r = $this->db->update('assessment',$update);
                $this->output->set_output(json_encode($e));
        }
      }
      $this->db->select('email,company.Name');
      $this->db->join('task_for_firs_team','task_for_firs_team.assessment_id');
      $this->db->join('assessment','assessment.auditor_id = task_for_firs_team.task_to_id');
      $this->db->join('company','company.id=task_for_firs_team.task_on_id');
      $aud = $this->db->get_where('all_users',array('all_users.id'=>$data['auditor_id'],'assessment.Assessment_id'=>$data['assessment_id']));
      $audName = '';
      $audemail = '';
       foreach ($aud->result() as $value) {
          $audName = $value->Name;
          $audemail = $value->email;
          break;
       }
      $msg = "<div style='font-size:1.2em;margin-bottom:20px;'><p>Hello ".$audName.",</p></div>";
      $msg .= "<div><p>You have a message from ".$audName.", as regard the ongoing financial statement auditing. Please login to your account to reply the Company.</p><div>";
      $msg .="<div style='text-align:center;font-size:1.2em;margin-top:30px;width:40%;margin-left:auto;margin-right:auto;'><p style='color:white;background-color:#3598dc;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;border-radius:5px;'>Click to Log In</p></div>";
      $to_auditor = $this->load->view('mail_container/mail_template',array('mail'=>$msg), true);
      $sta = $this->send_email->send_mail($audemail,'Message from '.$audName.'' ,$to_auditor);
      $this->output->set_output(json_encode($sta));
    }
     public function get_chats(){
      $this->view = false;
      $data= $this->input->post();
      $response = $this->dashboard_model->get_chats($data['assess_id'],$data['filename']);
        $this->output->set_output(json_encode($response));
    }
    public function get_company_chats(){
      $this->view = false;
      $data= $this->input->post();
      $response = $this->dashboard_model->get_company_chats($data['assess_id']);
        $this->output->set_output(json_encode($response));
    }
    public function get_all_assigned_task(){
      $this->view = false;
      $id = $this->session->userdata['user_details'][0]->id;
      $response = $this->dashboard_model->get_all_assigned_task();
      $this->output->set_output(json_encode($response));
    }
    public function coord_ongoing_ass(){
      $this->view = false;
      $response = $this->dashboard_model->get_all_coord_ongoing();
      $this->output->set_output(json_encode($response));
    }
    public function all_auditors_report(){
      $this->view = false;
      $response = $this->dashboard_model->get_all_auditor_report();
      $this->output->set_output(json_encode($response));
    }
    public function get_company_report(){
      $this->view =false;
      $response = $this->dashboard_model->get_all_company_report();
      $this->output->set_output(json_encode($response));
    }
    public function all_coord_report(){
      $this->view = false;
      $response = $this->dashboard_model->get_all_coord_report();
      $this->output->set_output(json_encode($response));
    }
    public function change_pass(){
      $this->view = false;
      $data = $this->input->post();
      /*$data['pass'] = 'qwerty1234';
      $data['cpass'] = 'qwerty1234';*/
      $this->load->helper('password');
      if($data['pass'] != $data['cpass']){
        $this->output->set_output(json_encode('false'));
        return false;
      }
      /*$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
      $hash_password = $hasher->HashPassword($data['pass']);*/
      $hash_password = $this->hashpassword($data['pass']);
       $update = array('password'=>$hash_password,'to_change_pass'=>'0');
                $this->db->where('email',$this->session->userdata['user_details'][0]->email);
                $r = $this->db->update('all_users',$update);
                 $this->db->select('*');
                 $status = $this->db->get_where('all_users',array('email'=>$this->session->userdata['user_details'][0]->email)); 
                 $this->session->set_userdata('user_details', $status->result()); 
                $this->output->set_output(json_encode($r));
             
    }
    public function get_company_assessment(){
      $this->view = false;
      $response = $this->dashboard_model->get_all_company_ass();
      $this->output->set_output(json_encode($response));
    }
  public function logout()
  { 
    $this->session->unset_userdata('user_details');
    redirect('');
  }
  public function ff(){
  $this->view =false;
  //print_r($this->session->userdata['user_details'][0]);//request file;file uploaded;file downloaded;purchase order/invoice;Accepted;otherfile
   $this->db->select('*');
    $t = $this->db->get_where('assessment',array('Assessment_id'=>'1308486924'));
      $new = serialize('Statement_of_account:message_from_company;Customer_invoices:message_from_company;payment_customer_invoices:message_from_company;supplier_invoices:message_from_company;payment_supplier_invoices:message_from_company');
    $update = array('stage'=>$new);
    $this->db->where('Assessment_id','1308486924');
    $r = $this->db->update('assessment',$update);
    $this->output->set_output(json_encode($r));

 }
  public function ffk(){
  $this->view =false;
  //print_r($this->session->userdata['user_details'][0]);//request file;file uploaded;file downloaded;purchase order/invoice;Accepted;otherfile
  $msg ="God alone I love";
   $to_auditor = $this->load->view('mail_container/mail_template',array('mail'=>$msg), true);
      $sta = $this->send_email->send_mail("ogunrindeomotayo@gmail.com",'Message from Tayo' ,$to_auditor);
      print_r($sta);

 }
       public function update_record(){
      //$r = strtolower($mon);
      $this->db->select('audit_time,assessment.Assessment_id,assessment.stage');
      $this->db->join('task_for_firs_team','task_for_firs_team.assessment_id=assessment.Assessment_id');
      $e = $this->db->get('assessment');
      foreach($e->result() as $n){
      $audit_time = unserialize($n->audit_time);
      $break_time = explode(";", $audit_time);
      $from_day = $break_time[0];
      $from_month = $break_time[1];
      $to_day = $break_time[2];
      $to_month = $break_time[3];
      $yr = $break_time[4];
      $month = array('January','February','March','April','May','June','July','August','Septemer','October','November','December');
      $month_in_lower_case = array_map('strtolower', $month);
      $from = array_search(strtolower($from_month), $month_in_lower_case);
      $to = array_search(strtolower($to_month), $month_in_lower_case); 
       $start = strtotime($from_day.'-'.($from+1).'-'.$yr);
       //echo "start:".$start;
       $present = strtotime(date('d').'-'.date('m').'-'.date('Y'));
       //echo "Present:".$present;
       $end = strtotime($to_day.'-'.($to+1).'-'.$yr);
       //echo "End:".$end;
       if($present>=$start && $end>$present && $n->stage =='' && $n->stage !='closed'){
        $update = array('status'=>'Open');
        $this->db->where('assessment_id',$n->Assessment_id);
        $this->db->update('assessment',$update);
         }
         else if($present>=$start && $end>$present && $n->stage !='' && $n->stage !='closed'){
        $update = array('status'=>'Work In Progress');
        $this->db->where('assessment_id',$n->Assessment_id);
        $this->db->update('assessment',$update);
         }
         else if($present > $end){
           $update = array('status'=>'Closed');
           $this->db->where('assessment_id',$n->Assessment_id);
           $this->db->update('assessment',$update);
         }
       }
   }
   public function pdf(){
    $this->view  = false;
   $this->db->select('email');
    $this->db->join('all_users','all_users.id=task_for_firs_team.assigned_by_id');
    $coord_email = $this->db->get_where('task_for_firs_team',array('task_for_firs_team.assigned_by_id'=>'7'));
    //print_r($coord_email->result()[0]->email);
   }
}
