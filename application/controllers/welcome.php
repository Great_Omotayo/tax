<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

   public function __construct() {
		parent::__construct();
		//$this->load->helper('phpass');
		$this->load->library('send_email');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('Welcome/index');
	}
	public function dd()
	{
		$hash = password_hash('school1234',PASSWORD_DEFAULT);
		echo $hash;
		$isValid = password_verify('qwertyuio',$hash);
		echo '<br>';
		echo $isValid;
		echo '<br>';
		print_r($this->session->all_userdata());
		echo $this->session->userdata['user_details'][0]->firstname;
	}
	/*
	public function vv(){
		$this->view =false;
         $data = $this->input->post();
         $email = trim($data['email'] ='temmytp22005@yahoo.com');
         $password = trim($data['password']='school1234');
         $this->db->select('*');
         $status = $this->db->get_where('all_users',array('email'=>$email));
         $pass = '';
         foreach ($status->result() as $value) {
         	$pass = $value->password;
         	break;
         }
         if(count($status->result())==0){
            $this->output->set_output(json_encode("0"));
         	return false;}
         $this->load->helper('password');
         $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
         $validpass = $hasher->CheckPassword($password, $pass);
         if(isset($validpass)){
         	$this->session->set_userdata('user_details', $status->result());
         	$this->output->set_output(json_encode("1"));
         }
         else{
           $this->output->set_output(json_encode("2"));
         }
         print_r($status->result());
		
		//redirect('');
	}
	*/
	public function hashpassword($password){
		$this->view = false;
		$hash = password_hash('qwertyuio',PASSWORD_DEFAULT);
		return $hash;

	}
	public function create_fir_administrator(){
		$this->view =false;
		$data= $this->input->post();
		if(strlen($data['password']) < 8){
            $reponse = array('error'=>'Password is Weak','Success'=>'False');
			$this->output->set_output(json_encode($reponse));
			return false;
		}
		elseif($data['password']!=$data['cpassword']){
			$reponse = array('error'=>'Password does not Match','Success'=>'False');
			$this->output->set_output(json_encode($reponse));
			return false;
		}
		//$this->load->helper('password');
	    //$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
	    //$hash_password = $hasher->HashPassword($data['password']);
	    $hash_password = $this->hashpassword($data['password']);
     $new_record = array('firstname'=>$data['firstname'],'lastname'=>$data['lastname'],'email'=>$data['email'],'phone'=>$data['phone'],'password'=>$hash_password,'is_fir_admin'=>1,'created_date'=>time());
     $r = $this->db->insert('all_users',$new_record);
     if($r=1){
     $this->db->select('*');
     $p= $this->db->get_where('all_users',array('email'=>$data['email']));	
     $this->session->set_userdata('user_details', $p->result());
     }
     $this->output->set_output(json_encode($r));
	}
	public function generate_digit(){
  	$this->view = false;
  	$number = mt_rand( 10000000, 99999999);
  	return $number;
    }
	public function add_company(){
		$this->view = false;
		$data = $this->input->post();
		/*$data['company_add']="8 nutating street new york";
        $data['company_admin'] = "Houston rate";
        $data['company_bvn']="89784546756";
        $data['company_email']="houstonrate@gmail.com";
        $data['company_name']="Liverpool";
        $data['company_phone']="837265465786";
        $data['company_rc']="08978446";
        $data['company_tax_zone']="Lagos";
        $data['company_tin']="83243546758";*/
		$this->load->library('send_email');
		$fir_admin_id = $this->session->userdata['user_details'][0]->id;
		$this->db->select('*');
		$exist  = $this->db->get_where('company',array('tin_id'=>trim($data['company_tin']),'BVN'=>trim($data['company_bvn']),'fir_admin'=>$fir_admin_id));
        if(count($exist->result())>0){
        	$this->output->set_output(json_encode('exist'));
        	return false;
        }
		$gen_pass = $this->generate_digit();
		//$this->load->helper('password');
	    //$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
	    //$hash_password = $hasher->HashPassword($gen_pass);
	    $hash_password = $this->hashpassword($gen_pass);
		$fir_admin_id = $this->session->userdata['user_details'][0]->id;
		$this->db->trans_start();
		$company_admin = array('firstname'=>trim($data['company_admin']),'email'=>trim($data['company_email']),'phone'=>trim($data['company_phone']),'password'=>$hash_password,'to_change_pass'=>'1','is_company_admin'=>'1','created_date'=>time());
		$this->db->insert('all_users',$company_admin);

		$id = $this->db->insert_id();
		$new = array('Name'=>trim($data['company_name']),'Address'=>trim($data['company_add']),'tin_id'=>trim($data['company_tin']),'BVN'=>trim($data['company_bvn']),'tax_zone'=>trim($data['company_tax_zone']),'RC'=>trim($data['company_rc']),'fir_admin'=>$fir_admin_id,'company_admin_id'=>$id);
		$this->db->insert('company',$new);
		$res = $this->db->trans_complete();
		if($res==true){
		  $msg = "<p>Hello ".$data['company_admin']."</p>";
		  $msg .= "<p style='margin-top:20px;'>This is to Notify you that an account have been created for you on taxassurance.com, Below is your log In details<p>";
          $msg .= "<p>User Name: ".$data['company_email']."";
          $msg .= "<p>Password: ".$gen_pass."</p>";
          $msg .="<p>NOTE: You will be required to Change your password on first log In.</p>";

          $to_send = $this->load->view('mail_container/mail_template',array('mail'=>$msg), true);
		  $response = $this->send_email->send_mail(trim($data['company_email']),'Invitation' ,$to_send);
		}
		$this->output->set_output(json_encode($response[0]['status']));
	}
	public function log_in_company_admin(){
		$this->view =false;
         $data = $this->input->post();
         $email = trim($data['email']);
         $password = trim($data['password']);
         $this->db->select('*');
         $status = $this->db->get_where('all_users',array('email'=>$email));
         $pass = '';
         foreach ($status->result() as $value) {
         	$pass = $value->password;
         	break;
         }
         if(count($status->result())==0){
            $this->output->set_output(json_encode("0"));
         	return false;}
         //$this->load->helper('password');
         //$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
         //$validpass = $hasher->CheckPassword($password, $pass);
         $validpass = password_verify($password,$pass);
         if(isset($validpass)){
         	$this->session->set_userdata('user_details', $status->result());
         	$this->output->set_output(json_encode("1"));
         }
         else{
           $this->output->set_output(json_encode("2"));
         }

	}
	/*public function vv(){
		$this->view =false;
		$this->db->select('*');
		//$this->db->select('*');
		$query = $this->db->get_where('all_users',array('email'=>'ogunrindeomotayo@gmail.com'));
		$this->session->set_userdata('user_details', $query->result());
		
		print_r($query->result());
		//redirect('');
	}*/	
	public function add_firs_team(){
		$this->view =false;
		$data = $this->input->post();
		$fir_admin_id = $this->session->userdata['user_details'][0]->id;
		$this->output->set_output(json_encode('error'));
         $check=$this->db->get_where('all_users',array('email'=>trim($data['email'])));
         if(count($check->result())>0){$this->output->set_output(json_encode('exist')); return false;}
        $gen_pass = $this->generate_digit();
		/*$this->load->helper('password');
	    $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
	    $hash_password = $hasher->HashPassword($gen_pass);*/
	    $hash_password = $this->hashpassword($gen_pass);
        if($data['category'] =='coordinator'){ 
		$this->db->trans_start();
		$fir_team = array('firstname'=>$data['name'],'password'=>$hash_password,'to_change_pass'=>'1','phone'=>$data['phone'],'email'=>$data['email'],'is_fir_coordinator'=>'1','created_date'=>time());
        $this->db->insert('all_users',$fir_team);
        $id = $this->db->insert_id();
        $t = array('fir_admin_id'=>$fir_admin_id,'category'=>'coord','region'=>$data['region'],'fir_member_id'=>$id,'created'=>time());
        $this->db->insert('fir_team_members',$t);
		$res = $this->db->trans_complete();
	    }
	    else{
	    $this->db->trans_start();
		$fir_team = array('firstname'=>$data['name'],'password'=>$hash_password,'to_change_pass'=>'1','phone'=>$data['phone'],'email'=>$data['email'],'is_fir_auditor'=>'1','created_date'=>time());
        $this->db->insert('all_users',$fir_team);
        $id = $this->db->insert_id();
        $t = array('fir_admin_id'=>$fir_admin_id,'category'=>'auditor','region'=>$data['region'],'fir_member_id'=>$id,'created'=>time());
        $this->db->insert('fir_team_members',$t);
		$res = $this->db->trans_complete();	
	    }
		if($res==true){
           $to_send = $this->msg($data,$data['category'],$gen_pass);
		   $response = $this->send_email->send_mail(trim($data['email']),'Invitation' ,$to_send);
           //$this->output->set_output(json_encode($response[0]['status']));
		   $this->output->set_output(json_encode($res));
		}else{
		   $this->output->set_output(json_encode('error'));	
		}
	}
	public function msg($data,$role,$pass){
		$this->view =false;
		  $msg="<p style='font-size:18px;'>Hello ".$data['name'].",<p>";
		  $msg .="<p style='font-size:14px;margin-top:20px;'>This is to Notify you that Mr ".$this->session->userdata['user_details'][0]->firstname." ".$this->session->userdata['user_details'][0]->lastname."
		  your FIRS Administrator has Invited you to anaylse company financial Statement in your region as delegated by your Administrator. You are assigned the role of a ".$role." </p>";
		  if($role=='coordinator'){
		  $msg .= "<p style='font-size:14px;'>Below are your login Details:<p>";	
		  $msg .= "<p style='font-size:14px;'>User Name: ".$data['email']."<p>";
		  $msg .= "<p style='font-size:14px;'>Password: ".$pass."<p>";
		  }
		  else if($role=='auditor'){
		  $msg .= "<p style='font-size:14px;'>Below are your login Details:<p>";	
		  $msg .= "<p style='font-size:14px;'>User Name: ".$data['email']."<p>";
		  $msg .= "<p style='font-size:14px;'>Password: ".$pass."<p>";
		  }
		  return $this->load->view('mail_container/mail_template',array('mail'=>$msg), true);
		  //print_r($to_send);
	}
	public function log_in_firs_team(){
         $this->view =false;
         $data = $this->input->post();
         $email = trim($data['email']);
         $password = trim($data['password']);
         $this->db->select('*');
         $status = $this->db->get_where('all_users',array('email'=>$email));
         $pass = '';
         foreach ($status->result() as $value) {
         	$pass = $value->password;
         	break;
         }
         if(count($status->result())==0){
            $this->output->set_output(json_encode("0"));
         	return false;}
         //$this->load->helper('password');
         //$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
         //$validpass = $hasher->CheckPassword($password, $pass);
         $validpass = password_verify($password,$pass);
         if(isset($validpass)){
         	$this->session->set_userdata('user_details', $status->result());
         	$this->output->set_output(json_encode("1"));
         }
         else{
           $this->output->set_output(json_encode("2"));
         }

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */