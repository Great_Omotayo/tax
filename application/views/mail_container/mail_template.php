<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <title>Immediate Action Required to keep your email address</title>
    <style>
      h2 a:visited{color:#404042 !important}
      h3 a:visited{color:#404042 !important}
      a:link{text-decoration:none;color:#0a46a4}
      a:visited{text-decoration:none;color:#0a46a4}
      a:hover{text-decoration:underline}
    </style>
    <style type="text/css">
      .ExternalClass {width: 100%; background-color: #f2f2f2;}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
            @media screen and (max-width: 600px) {
              h1 { font-size: 26px !important; }
              td[class="header_repeateer"] { width:100% !important; }
              td[class="force-col"] { display:block !important; padding:0px 0px 20px 0px !important; }
              img[class="the_stretcher"] { width:100% !important; }
              table[class="container"] { width: 95% !important; }
              td[class="container-padding"] { padding-left: 15px !important; padding-right: 15px !important; }
              div[class="footer_links"] { margin: 10px 20px 10px 20px !important; display: block !important; }
              a[class="footer_link_text"] { font-size:14px !important; }
              p[class="address"] { padding:20px !important; }
              span[id=switcher] {
                display: block;
                /*background-image: url(https://www.mts.ca/file_source/mts.ca/edm/bizinternet/redesign/Header-NEW@2x.jpg) !important;*/
                background-repeat: no-repeat !important;
                background-position: center !important;
                width: 285px !important;
                height: 114px;
                margin: 0 auto;
              }
              img[id=houdini] { width: 285px; height: 114px; }
              .sidebyside td { display: block !important;  width: 100% !important; text-align: center !important; }
              table.fauxlist td { display: block; width: 100% !important; }
              td.blockimg { display: block !important; vertical-align: middle; }
            }
    </style>
  </head>
  <body style="font-family:Roboto Condensed, sans-serif;margin:0;padding:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;" bgcolor="#ebebeb" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <br>
    <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ebebeb" id="backgroundTable" style="border-spacing:0;border-collapse:separate;margin:0;padding:0;width:100% !important;line-height:100% !important">
      <tr>
        <td align="center" valign="top" bgcolor="#ebebeb" style="border-collapse:collapse;background-color: #ebebeb;">
          <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff" style="border-spacing:0;border-collapse:separate">
            <tr>
              <td colspan="2" bgcolor="#ebebeb" align="center" style="border-collapse:collapse">
                <p style="margin:0px 0px 2px 0px;padding:0;font-family: Helvetica, sans-serif;font-size:10px;">
                  &nbsp;
              </p>
              </td>
            </tr>
            <tr style="background: #0a46a4;">
              <td width="83" height="56" align="left" valign="middle" style="border-collapse:collapse;padding-left:15px;">
              </td>
              <td width="151" height="56" align="right" valign="middle" style="border-collapse:collapse;padding-right:15px;color:white;font-size:1.3em">Data Assurance Hub
              </td>
            </tr>
          </table>
          <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff" style="border-spacing:0;border-collapse:separate">
            <tr>
              <td class="container-padding" bgcolor="#ffffff" style="border-collapse:collapse;background-color: #ffffff; padding-left: 15px; padding-right: 15px; font-size: 16px; line-height: 22px; font-family: Helvetica, sans-serif; color: #404042;text-align:left;">
                <h1 style="color:#0a46a4;margin:0;padding:20px 0px 10px 0px;color:#0a46a4;font-family: Helvetica, sans-serif;font-size:24px;line-height:28px;text-align:center;"></h1>
                <table width="100%" cellpadding="5" cellspacing="0" class="container" style="border-spacing:0;border-collapse:separate">
                  <tr>
                  	<?php echo $mail; ?>
                  </tr>
                </table>
                <br>
                <br>
                <table border="0" width="100%" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff" style="border-spacing:0;border-collapse:separate">
                  <tr>
                    <td align="center" class="force-col" style="border-collapse:collapse;padding:0px 0px 20px 50px;"><a href="" class="footer_link_text" style="text-decoration:none;color:#0a46a4;font-size:14px;color:#0a46a4;font-weight:bold;text-decoration:none;font-family: Helvetica, sans-serif;white-space:nowrap;">Contact Us</a></td>
                    <td align="center" class="force-col" style="border-collapse:collapse;padding-bottom:20px;"><a href="" class="footer_link_text" style="text-decoration:none;color:#0a46a4;font-size:14px;color:#0a46a4;font-weight:bold;text-decoration:none;font-family: Helvetica, sans-serif;white-space:nowrap;">Terms &amp; Conditions</a></td>
                    <td align="center" class="force-col" style="border-collapse:collapse;padding-bottom:20px;"><a href="" class="footer_link_text" style="text-decoration:none;color:#0a46a4;font-size:14px;color:#0a46a4;font-weight:bold;text-decoration:none;font-family: Helvetica, sans-serif;white-space:nowrap;">Privacy</a></td>
                    <td align="center" class="force-col" style="border-collapse:collapse;padding:0px 50px 20px 0px;"><a href="" class="footer_link_text" style="text-decoration:none;color:#0a46a4;font-size:14px;color:#0a46a4;font-weight:bold;text-decoration:none;font-family: Helvetica, sans-serif;white-space:nowrap;">Unsubscribe</a></td>
                  </tr>
                </table>
                <table border="0" width="100%" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff" style="border-spacing:0;border-collapse:separate">
                  <tr>
                    <td width="100%" height="1" align="center" style="border-collapse:collapse">
                      <p class="address" style="margin:0;padding:0;font-family: Helvetica, sans-serif;font-size:11px;line-height:16px;">70, Olonade street Alagomeeji Yaba Lagos</p>
                      <br>
                      <br>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <br>
    <br>
  </body>
</html>