<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/foundation.css'/>
	<link rel="stylesheet" href="<?= base_url('assets/css/tax.css'); ?>"/>
	<link rel="stylesheet" href="<?= base_url('assets/css/animate.css'); ?>"/>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation/foundation.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation/foundation.topbar.js"></script>
<!--script type="text/javascript" src="http://localhost/tax/assets/js/tax.js"></script-->
<script type="text/javascript" src="<?= base_url('assets/js/tax.js'); ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</head>
<body>
	<div class='wrapper'>
		<nav class="top-bar sticky" data-topbar role="navigation">
	      <ul class="title-area">
	       <li class="name">
	        <h1><a href="#">Data Assurance Hub</a></h1>
	       </li>
	     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
	       <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	      </ul>

	  <section class="top-bar-section">
	    <!-- Right Nav Section -->
	    <ul class="right">
	       <li class="has-dropdown">
        <a href="#">LOG IN</a>
        <ul class="dropdown">
          <li><a href="#" id='show-log-in-firs'>As FIRS Team</a></li>
          <li class="active"><a href="#" id ='show-log-in'>As Company Administrator</a></li>
          </ul>
          </li>
      	  <li class="active"><a href="#" id='show-sign-up'>SIGN UP</a></li>
	    </ul>

	    <!-- Left Nav Section -->
	    <ul class="left">
	      <li><a href="#"></a></li>
	    </ul>
	  </section>
	</nav>
	<div><img class='bgd-img' src="<?= base_url('assets/imgs/calc.jpg'); ?>" style='width:100%;height:600px;'></div>
	<div class="row sign-up-div">
    <div class="small-12 medium-5 large-7 columns"></div>
    <div class="small-12 medium-6 medium-offset-3 large-5 large-offset-7 columns end sign_up animated zoomIn">
	    <ul>
	      <li style='text-align:center;color:#FFFFFF;background-color:grey'><h4 style='padding:5px 5px 5px 5px;'>SIGN UP</h4></li>
	      <li><p>NOTE: <span style='color:red;font-size:12px;'>Sign Up for only FIRS Team Leader</span></p></li>	
	      <li><input type='text' id='firstname' placeholder='firstname'></li>
	      <li><input type='text' id='lastname' placeholder='lastname'></li>
	      <li><input type='email' id='email' placeholder='email'></li>
	      <li><input type='text' id='phone' placeholder='Phone Number'></li>
	      <li><input type='password' id='password' placeholder='password'></li>
	      <li><input type='password' id='cpassword' placeholder='Confirm password'></li>
	      <li><a class=" success expand button create_acc" href="#">Create Account</a></li>
	    </ul>
    </div>
	</div>
	<div class="row lg-in-for-company hide">
        <div class="small-12 medium-3 large-3 columns"></div>
        <div class="small-12 medium-5 large-5 columns log-in animated zoomIn">
          <ul>
             <li style='text-align:center;background-color:grey;'><h4 style='color:#FFFFFF;padding:5px 5px 5px 5px;'>LOG IN</h4></li>
             <li><p>NOTE: <span>Log In as Company Administrator</span></p></li>
             <li><input type='email' id='company-email' placeholder='email'></li>
             <li><input type='password' id='company-password' placeholder='password'></li>
             <li><input type='text' id='tin_id' placeholder='tin id'></li>
             <li><a class="success expand button right log-company-admin" href="#">LOG IN</a></li>
          </ul>
        </div>
	</div>
	<div class="row lg-in-for-firs-team hide">
		<div class="small-12 medium-3 large-3 columns"></div>
        <div class="small-12 medium-5 large-5 columns lg-in-firs animated zoomIn">
          <ul>
             <li style='text-align:center;background-color:grey;'><h4 style='color:#FFFFFF;padding:5px 5px 5px 5px;'>SIGN IN</h4></li>
             <li><p>NOTE: <span>Log In as FIRS Team</span></p></li>
             <li><input type='email' id='firs-email' placeholder='email'></li>
             <li><input type='password' id='firs-password' placeholder='password'></li>
             <li><a class=" success expand button log-in-firs-team" href="#">LOG IN</a></li>
          </ul>
        </div>
	</div>
</div>
 <script>
    $(document).foundation();
  </script>
</body>
</html>