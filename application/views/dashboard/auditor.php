<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/foundation.css'/>
    <link rel="stylesheet" type='text/css' href="<?= base_url('assets/css/taxassurance.css'); ?>"/>
    <link rel="stylesheet" href="<?= base_url('assets/css/animate.css'); ?>"/>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
   <!--script type="text/javascript" src="http://localhost/tax/assets/js/jquery-2.1.4.js"></script-->
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation/foundation.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation/foundation.topbar.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation/foundation.reveal.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/taxassurance.js'); ?>"></script>
   <script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
   <script src="//cdn.ckeditor.com/4.5.5/standard/adapters/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.min.css">
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body id='body' style='background-color:#F5F5F5;font-family:Roboto Condensed, sans-serif;margin-left:1px;margin-right:5px;overflow-x:hidden'>
  <style type="text/css">


  </style>
   <script type="text/javascript">
   $(document).ready(function(){
          CKEDITOR.replace('editor1');
          CKEDITOR.config.height='400px';
        });
   </script>

   <div class='fixed'>
  <nav class="top-bar sticky" data-topbar role="navigation">
        <ul class="title-area">
         <li class="name">
          <h1><a href="#">Data Assurance Hub</a></h1>
         </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
         <li class="toggle-topbar menu-icon menubar"><a href="#"><span>Menu</span></a></li>
        </ul>

    <section class="top-bar-section">
      <!-- Right Nav Section -->
      <ul class="right">
          <li class="has-form">
            <div class="row collapse">
              <div class="large-12 small-12 medium-12 columns">
                <input type="text" placeholder="Find Stuff" id='value_to_search'>
              </div>
            </div>
          </li>
          <li class="active show-for-small-only"><a href="#" class='tools'>TOOLS</a></li>
          <li class="for-small"><a href="#" class='tools'>TOOLS</a></li>
          <li class=""><a href="<?=base_url('index.php/dashboard/logout')?>" id='show-sign-up'>LOG OUT</a></li>
      </ul>
    </section>
  </nav>
</div>
  <div class="callout">
    <div class='small-8 medium-3 large-2 columns sideall' style='background-color:#364150;height:auto;'>
      <div class='row collapse' style='margin-top:30px;color:white;padding-bottom:30px;'>
         <div class='small-5 medium-4 large-6 columns'><img class='profile' src='<?=base_url('/assets/imgs/images.png')?>' style=''/></div>
         <div class='small-7 medium-8 large-6 columns left' style='margin-top:10px;'>
          <ul>
            <li id='welcome'>Welcome,</li>
            <li id='owner'><?php echo $this->session->userdata['user_details'][0]->firstname;;?></li>
          </ul>
         </div>
        </div>
   <div class='row sidelist'style='border-top:1px solid grey;' >
       <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns collapse'> <i class="fa fa-tachometer"></i> &nbsp;Dashboard</div></a>
   </div>
    <div class='row sidelist hide'style='' >
       <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns collapse' id='ongoing_ass'> <i class="fa fa-calendar"></i> &nbsp;Assessment</div></a>
   </div>
   <!--div class='row sidelist'style='' >
       <a href='#' style='color:#b4bcc8;' id='' >
    <div class='small-12 medium-12 large-12 columns collapse' id='finished_ass'> <i class="fa fa-table"></i> &nbsp;Finished Assessment</div></a>
   </div-->
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' id='assign_task' >
    <div class='small-12 medium-12 large-12 columns' id="orders"><i class="fa fa-bar-chart"></i> &nbsp;Assessment Order</div></a>
   </div>
   <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' id='assigned_task' >
    <div class='small-12 medium-12 large-12 columns'><i class="fa fa-bar-chart"></i> &nbsp;Assigned Task</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;'>
    <div class='small-12 medium-12 large-12 columns' id='gen_report'><i class="fa fa-line-chart"></i> &nbsp; Write Report</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' id='all_reports'><div class='small-1 medium-1 large-1 columns'></div>
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-map-marker"></i> &nbsp;Reports</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-cog"></i> &nbsp;Settings</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-cog"></i> &nbsp;Extra</div></a>
   </div>
    </div>
    <div class="small-12 medium-9 large-10 columns maincon" style='' >
        <div class='dashtop' style='margin-top:50px;color:grey;'>
         <div class='row' style='margin-left:0px;'><div class='small-12 medium-12 large-12 columns' id="dash_title">Dashboard</div></div>
        </div><hr>
        <div  class='all_gen_reports'></div>
        <div class='activity'>
          <a href="#" id='show_bar' class='hide'></a>

          <div class="msg"></div>
          <div class="gen_report hide">
           
            <div class='row'>
             <div class='small-6 medium-6 large-6 columns'><input type='text' placeholder='Assessment ID' id='report_id'/></div>  
             <div class='small-6 medium-6 large-6 columns' style='margin-top:10px;'>Copy Company <input type='checkbox' id ='copy_company'/></div>
            </div>
            <div class='row'> <div class='small-12 medium-12 large-12 columns'>
            <textarea class='editor' name='editor1' id='editor1' style='height:300px;' rows='12%' cols='92%'></textarea>
          </div></div>
            <div class="row" style="margin-top:20px;"><div class='small-12 medium-8 large-8 columns left'>
             <a href="#" class='success button tiny' id='sendreport'>Send Report</a>
            </div></div>
          </div>

           <div class='ongoing_ass'>  
         </div>
            
            <div class='finished_ass hide' style='margin-top:15px;'>
                
           </div>
             
             <?php if($assess_orders > 0): $x=0;?>
               <div class='div_for_table assessment_order show-for-large-up'>
                <table class='table_for_assess'>
                  <tr class='table_head'>
                  <th class='table_content' width="auto">ID</th>
                  <th class='table_content' width:"auto">STATUS</th>
                  <th class='table_content' width="auto">COMPANY</th>
                  <th class='table_content' width="auto">PERIOD</th>
                  <th class='table_content' width="auto">START DATE</th>
                  <th class='table_content' width="auto">END DATE</th>
                  <th class='table_content' width="auto">ASSESSMENT ID</th>
                  <th class='table_content' width='auto'>CREATED</th>
                  <th class='table_content' width='auto'>STAGE</th>
                  <th class='table_content' width='auto'>ACCESS</th>
                </tr>
              </tbody>
                <?php for($a=0;$a<count($assess_orders);$a++): ?>
                   <?php if(count(explode(':',$assess_orders[$a]['stage']))>1):?>
                          <?php list($first) = explode(':',$assess_orders[$a]['stage']); ?>

                        <?php if($first =='message_from_auditor'):?>
                               <?php $r =  "Message from Auditor";?>
                        <?php elseif($first =='message_from_company'):?>
                               <?php $r =  "Message from Company";?>
                        <?php elseif($first =='Customer_invoices'):?>
                               <?php $r =  "Customer Invoices";?>
                        <?php elseif($first =='Accepted'):?>
                                   <?php if($assess_orders[$a]['stage']=='Statement_of_account:Accepted'):?>
                                      <?php $r =  "Statement of Account Accepted";?>
                                   <?php elseif($assess_orders[$a]['stage']=='Customer_invoices:Accepted'):?>
                                      <?php $r =  "Customer Invoices Accepted";?>
                                      <?php elseif($assess_orders[$a]['stage']=='payment_customer_invoices:Accepted'):?>
                                      <?php $r =  "Payment Customer Invoices Accepted";?>
                                       <?php elseif($assess_orders[$a]['stage']=='payment_supplier_invoices:Accepted'):?>
                                      <?php $r =  "Payment Supplier Invoices Accepted";?>
                                    <?php elseif($assess_orders[$a]['stage']=='supplier_invoices:Accepted'):?>
                                      <?php $r =  "Supplier Invoices Accepted";?>
                                   <?php endif;?>
                        <?php elseif($first =='Rejected'):?>
                                    <?php echo "love1";?>
                                    <?php if($assess_orders[$a]['stage']=='Statement_of_account:Rejected'):?>
                                       <?php $r =  "Statement of Account Rejected";?>
                                    <?php elseif($assess_orders[$a]['stage']=='Customer_invoices:Rejected'):?>
                                       <?php $r =  "Customer Invoices Rejected";?>
                                     <?php elseif($assess_orders[$a]['stage']=='payment_customer_invoices:Rejected'):?>
                                      <?php $r =  "Payment Customer Invoices Rejected";?>
                                    <?php elseif($assess_orders[$a]['stage']=='payment_supplier_invoices:Rejected'):?>
                                      <?php $r =  "Payment Supplier Invoices Accepted";?>
                                    <?php elseif($assess_orders[$a]['stage']=='supplier_invoices:Rejected'):?>
                                      <?php $r =  "Supplier Invoices Accepted";?>
                                    <?php endif;?>
                             <?php endif;?>
                <?php else:?>
                 <?php if($assess_orders[$a]['stage'] =='Statement_of_account_uploaded'):?>
                 <?php $r =  "Statement of Account Uploaded";?>
                 <?php elseif($assess_orders[$a]['stage'] =='Customer_invoices_uploaded'):?>
                 <?php $r =  "Customer Invoices Uploaded";?>
               <?php elseif($assess_orders[$a]['stage'] =='payment_customer_invoices_uploaded'):?>
                 <?php $r =  "Payment Customer Invoices Uploaded";?>
                 <?php elseif($assess_orders[$a]['stage'] =='payment_supplier_invoices_uploaded'):?>
                 <?php $r =  "Payment Supplier Invoices Uploaded";?>
                 <?php elseif($assess_orders[$a]['stage'] =='supplier_invoices_uploaded'):?>
                 <?php $r =  "Supplier Invoices Uploaded";?>
               <?php elseif($assess_orders[$a]['stage'] =='external_file_uploaded'):?>
                 <?php $r =  "External file Attached";?>
               <?php elseif($assess_orders[$a]['stage'] =='vat_generated'):?>
                 <?php $r =  "VAT Generated";?>
               <?php elseif($assess_orders[$a]['stage'] =='share generated vat with company'):?>
                 <?php $r =  "VAT Generated and Share with Company";?>
               <?php elseif($assess_orders[$a]['stage'] =='final_assessment_uploaded'):?>
                 <?php $r =  "Final Report uploaded";?>
                 <?php elseif($assess_orders[$a]['stage'] =='Yet to Start'):?>
                 <?php $r =  $assess_orders[$a]['stage'];?>
                  <?php endif;?>
                <?php endif;?>
                <tr><td style='text-align:center;'><?php echo ($a+1)?></td>
                    <td class='table_sub'><?php echo ($assess_orders[$a]['status']=='Work In Progress')?'WIP':strtoupper($assess_orders[$a]['status']);?></td>
                    <td class='table_sub'><?php echo $assess_orders[$a]['company_name']?></td>
                    <td class='table_sub'><?php echo $assess_orders[$a]['period']?></td>
                    <td class='table_sub'><?php echo $assess_orders[$a]['start_audit_time']?></td>
                    <td class='table_sub'><?php echo $assess_orders[$a]['end_audit_time']?></td>
                    <td class='table_sub'><?php echo $assess_orders[$a]['assessment_id']?></td>
                    <td class='table_sub'><?php echo $assess_orders[$a]['created']?></td>
                    <td class='table_sub'><?php echo $r; ?></td>
                    <?php if($assess_orders[$a]['status']=='Planned'):?>
                    <td class='table_sub'><a href="#"  id='data<?php echo $assess_orders[$a]['assessment_id']; ?>' data-id="<?php echo $assess_orders[$a]['assessment_id']; ?>" class="button tiny" style='margin-top:12px'>Locked</a></td>
                    <?php elseif($assess_orders[$a]['status'] =='Closed'):?>
                    <td class='table_sub'><a href="#" id='data<?php echo $assess_orders[$a]['assessment_id']; ?>' data-id="<?php echo $assess_orders[$a]['assessment_id']; ?>" class="open_assess button tiny alert" style='margin-top:12px;padding-left:25px;padding-right:25px;'>Open</a></td>
                    <?php else:?>
                    <td class='table_sub'><a href="#" id='data<?php echo $assess_orders[$a]['assessment_id']; ?>' data-id="<?php echo $assess_orders[$a]['assessment_id']; ?>" class="open_assess button tiny success" style='margin-top:12px;padding-left:25px;padding-right:25px;'>Open</a></td>
                    <?php endif;?>
                  </tr>
                <script type="text/javascript">$('#dash_title').text('Assessment Orders')</script>
            <?php endfor;?>
          </tbody>
            </table>
          </div>
          <?php for($a=0;$a<count($assess_orders);$a++): ?>
             <?php if(count(explode(':',$assess_orders[$a]['stage']))>1):?>
                        <?php list($first) = explode(':',$assess_orders[$a]['stage']); ?>
                        <?php if($first =='message_from_auditor'):?>
                               <?php $r =  "Message from Auditor";?>
                        <?php elseif($first =='message_from_company'):?>
                               <?php $r =  "Message from Company";?>
                        <?php elseif($first =='Accepted'):?>
                                   <?php if($assess_orders[$a]['stage']=='Statement_of_account:Accepted'):?>
                                      <?php $r =  "Statement of Account Accepted";?>
                                   <?php elseif($assess_orders[$a]['stage']=='Customer_invoices:Accepted'):?>
                                      <?php $r =  "Customer Invoices Accepted";?>
                                      <?php elseif($assess_orders[$a]['stage']=='payment_customer_invoices:Accepted'):?>
                                      <?php $r =  "Payment Customer Invoices Accepted";?>
                                       <?php elseif($assess_orders[$a]['stage']=='payment_supplier_invoices:Accepted'):?>
                                      <?php $r =  "Payment Supplier Invoices Accepted";?>
                                    <?php elseif($assess_orders[$a]['stage']=='supplier_invoices:Accepted'):?>
                                      <?php $r =  "Supplier Invoices Accepted";?>
                                   <?php endif;?>
                        <?php elseif($first =='Rejected'):?>
                                    
                                    <?php if($assess_orders[$a]['stage']=='Statement_of_account:Rejected'):?>
                                       <?php $r =  "Statement of Account Rejected";?>
                                    <?php elseif($assess_orders[$a]['stage']=='Customer_invoices:Rejected'):?>
                                       <?php $r =  "Customer Invoices Rejected";?>
                                     <?php elseif($assess_orders[$a]['stage']=='payment_customer_invoices:Rejected'):?>
                                      <?php $r =  "Payment Customer Invoices Rejected";?>
                                    <?php elseif($assess_orders[$a]['stage']=='payment_supplier_invoices:Rejected'):?>
                                      <?php $r =  "Payment Supplier Invoices Accepted";?>
                                    <?php elseif($assess_orders[$a]['stage']=='supplier_invoices:Rejected'):?>
                                      <?php $r =  "Supplier Invoices Accepted";?>
                                    <?php endif;?>
                             <?php endif;?>
                <?php else:?>
                 <?php if($assess_orders[$a]['stage'] =='Statement_of_account_uploaded'):?>
                 <?php $r =  "Statement of Account Uploaded";?>
                 <?php elseif($assess_orders[$a]['stage'] =='Customer_invoices_uploaded'):?>
                 <?php $r =  "Customer Invoices Uploaded";?>
               <?php elseif($assess_orders[$a]['stage'] =='payment_customer_invoices_uploaded'):?>
                 <?php $r =  "Payment Customer Invoices Uploaded";?>
                 <?php elseif($assess_orders[$a]['stage'] =='payment_supplier_invoices_uploaded'):?>
                 <?php $r =  "Payment Supplier Invoices Uploaded";?>
                 <?php elseif($assess_orders[$a]['stage'] =='supplier_invoices_uploaded'):?>
                 <?php $r =  "Supplier Invoices Uploaded";?>
                  <?php elseif($assess_orders[$a]['stage'] =='Yet to Start'):?>
                 <?php $r =  $assess_orders[$a]['stage'];?>
                  <?php endif;?>
                <?php endif;?>
               <div class='assess_for_small_dev'>
              <div class='callout show-for-medium-down' style=''>
                <div class='small-12 medium-6 columns' style='padding:0px;margin:0px;margin-bottom:15px;'>
                  <ul style='border:1px solid #ddd;text-align:center;margin-bottom:10px;width:100%;padding:0px;margin:0px;'>
                    <div class='callout' style='color:white;padding:0px;'><div class='small-6 medium-6 columns' style='background-color:#3598dc;'>Status</div><div class='small-6 medium-6 columns' style='background-color:#e7505a;'><?php echo ($assess_orders[$a]['status']=='Work In Progress')?'WIP':strtoupper($assess_orders[$a]['status']);?></div></div>
                    <!--div style='background-color:#3598dc;color:white;'><li>Status</li>
                      <li style='margin-left:auto;margin-right:auto;background-color:#e7505a;'><?php echo ($assess_orders[$a]['status']=='Work In Progress')?'WIP':strtoupper($assess_orders[$a]['status']);?></li></div-->
                    <table width="100%">
                      <tr><td width="30%">Auditor</td><td width="70%"><?php echo $assess_orders[$a]['auditor_name']?></td></tr>
                      <tr><td width="40%">Assessment ID</td><td width="60%"><?php echo $assess_orders[$a]['assessment_id']?></td></tr>
                      <tr><td width="30%">Start Date</td><td width="70%"><?php echo $assess_orders[$a]['start_audit_time']?></td></tr>
                      <tr><td width="30%">End Date</td><td width="70%"><?php echo $assess_orders[$a]['end_audit_time']?></td></tr>
                      <tr><td width="30%">Period</td><td width="70%"><?php echo $assess_orders[$a]['period']?></td></tr>
                      <tr><td width="30%">Stage</td><td width="70%"><?php echo $r;?></td></tr>
                      </table>
                      <?php if($assess_orders[$a]['status']=='Planned'):?>
                    <div class='row'><div class='small-4 small-offset-4 medium-offset-4 columns end'><a href="#"  id='data<?php echo $assess_orders[$a]['assessment_id']; ?>' data-id="<?php echo $assess_orders[$a]['assessment_id']; ?>" class="button tiny" style='margin-top:12px'>Locked</a></div></div>
                    <?php elseif($assess_orders[$a]['status'] =='Closed'):?>
                    <div class='row'><div class='small-4 small-offset-4 medium-offset-4 columns end'><a href="#"  id='data<?php echo $assess_orders[$a]['assessment_id']; ?>' data-id="<?php echo $assess_orders[$a]['assessment_id']; ?>" class="open_assess button tiny alert" style='margin-top:12px;padding-left:25px;padding-right:25px;'>Open</a></div></div>
                    <?php else:?>
                    <div class='row'><div class='small-4 small-offset-4 medium-offset-4 columns end'><a href="#" id='data<?php echo $assess_orders[$a]['assessment_id']; ?>' data-id="<?php echo $assess_orders[$a]['assessment_id']; ?>" class="open_assess button tiny success" style='margin-top:12px;padding-left:25px;padding-right:25px;'>Open</a></div></div>
                    <?php endif;?>
                    </table>
                  </ul>
                </div>
              </div>
            </div>
            <?php endfor; ?>              
            <?php endif;?>
            <?php if(count($assess_orders)==0):?>
             <div class='assessment_order add_to_no_ongoing'>No Assessment Order</div>
             <style type="text/css">
               #assessment_order{
                border: none;
                width:300px;height:300px;border-radius:150px;background-color:white;margin-left:auto;margin-right:auto;text-align: center;padding-top: 150px;
               }
             </style>
            <?php endif;?>

        </div>
    </div>
   </div>
         <div id="companyuploadedfile" class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
             <div class="row"><div class="small-6 medium-5 large-4 columns header change-title" style='padding:5px 5px 5px 2px;'>UPLOADED FILE</div></div>
             <div class='doc_stuff'>
             <div class='row uploadedfile' style='margin-top:40px;'><div class='small-12 large-12 large-offset-3 columns'><a href="#">Download and Review File</a></div></div>
             <div class='row' style='margin-top:20px;'>
              <div class='small-12 medium-12 large-12 columns'>DOCUMENT STATUS</div>
            </div><hr style='margin-top:-12px;'>
             <ul  class='status_of_doc' style='margin-left:20%;'>
              <li><input type='radio' name='radio' class='sta_of_doc' id='acceptfile'  value='Accepted'/> Accept</li>
              <li><input type='radio' name='radio' class='sta_of_doc' id='rejectfile' value='Rejected'/> Reject with Comment</li>
             </ul></div>
             <div class='comment hide'>
              <div class='row' style='margin-top:20px;'><div class='small-12 medium-12 large-12 columns'><a href="#" id='backtomainpage'><i class="fa fa-chevron-left"></i>Back</a></div></div>
              <div style="width:95%;margin-left:auto;margin-right:auto;">
              <div id=''><textarea rows='10' id='reject_with_comment' placeholder='Reject with Comments....'></textarea></div>
              <a href="#" class='button tiny' id='sendcomment'>Send</a>
             </div></div>
             <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
       <div id="otherfile" class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
             <div class="row"><div class="small-12 medium-3 large-3 columns header" style='padding:5px 5px 5px 15px;'>ADD FILE</div></div>
             <div class='hide'><input type='file' name='file[]' id='collectfile'/></div>
             <div class='row' style='margin-top:40px;'>
              <div class='small-12 medium-12 large-12 columns'>FILE FROM OTHER SOURCE</div>
            </div><hr style='margin-top:-12px;'>
            <div class='row'><div class='small-12 medium-12 large-12 columns'><ul style='text-align:center;' class='listfile'></ul></div></div>
            <div class='row'><div class='small-3 medium-3 large-3 columns' style='margin-top:20px;'><a href='#' id='addfile'><i class='fa fa-upload'></i> Add File</a></div>
          <div class='small-9 medium-9 large-9 columns'><a href="#" class='button success expand' id='sendfile'>Upload File</a></div></div>
             <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
      <div id="Report" class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
             <div class="row"><div class="small-12 medium-3 large-3 columns header" style='padding:5px 5px 5px 15px;'>ADD FILE</div></div>
             <div class='hide'><input type='file' name='file[]' id='collectfile'/></div>
             <div class='row' style='margin-top:40px;'>
              <div class='small-12 medium-12 large-12 columns'>FINAL ASSESSMENT REPORT</div>
            </div><hr style='margin-top:-12px;'>
            <div class='row'><div class='small-12 medium-12 large-12 columns'><ul style='text-align:center;' class='listfile'></ul></div></div>
            <div class='row' style=''><div class='small-3 medium-3 large-3 columns' style='margin-top:20px;'><a href='#' id='addfile'><i class='fa fa-upload'></i> Add File</a></div>
          <div class='small-9 medium-9 large-9 columns'><a href="#" class='button success expand' id='sendfile'>Upload File</a></div></div>
             <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
       <div id="allconversations" class='reveal-modal' style='background-color:#F8F8FF' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
             <div style=''>
              <div class="row" style='margin-bottom:40px;'><div class="small-12 medium-2 large-3 columns header" style='padding-left:10px;padding-right:5px;font-size:1.2em'>MESSAGE</div></div>
             </div>
             <div id='allchats'></div>
              <div class="row" id='input_msg'>
                <div class="large-12 columns">
                  <div class="row collapse">
                    <div class="small-10 columns">
                      <input type="text" placeholder="message to company admin" id='text_to'>
                    </div>
                    <div class="small-2 columns">
                      <a href="#" class="button postfix" id='send_msg'>Go</a>
                    </div>
                  </div>
                </div>
              </div>
              <a class="close-reveal-modal" style='color:red;' aria-label="Close">&#215;</a>
      </div>
       <div id="generate_vat" class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
              <div class="row" style='margin-bottom:40px;'>
                <div class="small-12 medium-2 large-4 columns header" style='padding-left:5px;padding-right:5px;font-size:1.2em'>GENERATE VAT</div></div>
              <div class="row"><div class='small-12 medium-12 large-12 columns'>Generate VAT</div></div><hr style='margin-top:-3px;'>
              <div class='row loading' style='margin-top:30px;text-align:center;'><div class='small-12 medium-12 large-12 columns'><i class='fa fa-spinner fa-spin fa-3x'></i></div></div>
              <div class='row the_gen_vat' style='padding-bottom:10px;'></div>
              <div class='row'><div class='small-12 medium-12 large-6 large-offset-3 columns'><a href='#' id='share_with_company' class='button small success'><i class="fa fa-share" aria-hidden="true"></i> Share with Company</a></div></div>
              <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
       <div id="requested_file" style='padding-bottom:20px;' class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
              <div class="row" style='margin-bottom:40px;'>
                <div class="small-12 medium-5 large-5 columns header" style='padding-left:10px;padding-right:5px;padding-top:5px;padding-bottom:5px;color:white;font-size:1.2em'>UPLOADED FILE</div></div>
              <div class="row"><div class='small-12 medium-12 large-12 columns'>Company File</div></div><hr style='margin-top:-3px;'>
              <div id='show_coord_uploaded_file'></div>
              <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
       <div id="auditor_otherfile" class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
             <div class="row"><div class="small-12 medium-5 large-5 columns header" style='padding-left:15px;color:white;font-size:1.2em;'>EXTERNAL FILE</div></div>
             <div class='row' style='margin-top:40px;'>
              <div class='small-12 medium-12 large-12 columns'>FILE FROM OTHER SOURCE</div>
            </div><hr style='margin-top:-12px;'>
            <div class='row'><div class='small-12 medium-12 large-12 columns'><ul style='margin-left:20px;' class='listfile'></ul></div></div>
             <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
       <div id="change_pass" class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
             <div>
              <div class="row" style='margin-bottom:40px;'><div class="small-12 medium-5 large-5 columns header" style='padding-left:15px;padding-right:5px;font-size:1.2em'>CHANGE PASSWORD</div></div>
             </div>
             <div class='row' style='margin-bottom:5px;margin-top:40px;'><div class='small-12 medium-12 large-12 columns'><input type="password" placeholder="Passsword" id='ch_pass'></div></div>
             <div class='row' style='margin-bottom:5px;'><div class='small-12 medium-12 large-12 columns'><input type="password" placeholder="Confirm Passsword" id='ch_cpass'></div></div>
             <div class='row'><div class='small-12 medium-12 large-12 columns'><a href="#" class='button success expand' id='to_change_pass'>Change Password</a></div></div>
              <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div> 
      
</body>
 <script>
    $(document).foundation();
  </script>
  <?php if($to_pass=='1'):?>
     <script type="text/javascript">
           $(document).ready(function(e){
         $('#change_pass').foundation('reveal', 'open');
       });
     </script>
     <?php $to_pass ='0';?>
    <?php endif;?>
</body>
</html> 