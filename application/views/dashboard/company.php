<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/foundation.css'/>
    <link rel="stylesheet" href="<?= base_url('assets/css/taxassurance.css'); ?>"/>
    <link rel="stylesheet" href="<?= base_url('assets/css/animate.css'); ?>"/>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
   <!--script type="text/javascript" src="http://localhost/tax/assets/js/jquery-2.1.4.js"></script-->
   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.topbar.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.reveal.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.dropdown.js"></script>
<!--script type="text/javascript" src="http://localhost/tax/assets/js/taxassurance.js"></script-->
<script type="text/javascript" src="<?= base_url('assets/js/company.js'); ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.min.css">
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body style='background-color:#F5F5F5;font-family:Roboto Condensed, sans-serif;'>

  <style type="text/css">


  </style>
  <div class='fixed'>
  <nav class="top-bar sticky" data-topbar role="navigation">
        <ul class="title-area">
         <li class="name">
          <h1><a href="#">Data Assurance Hub</a></h1>
         </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
         <li class="toggle-topbar menu-icon  menubar"><a href="#"><span>Menu</span></a></li>
        </ul>

    <section class="top-bar-section">
      <!-- Right Nav Section -->
      <ul class="right">
         <li class="has-dropdown">
        <!--a href="#">LOG IN</a>
        <ul class="dropdown">
          <li><a href="#" id='show-log-in-firs'>As FIRS Team</a></li>
          <li class="active"><a href="#" id ='show-log-in'>As Company Administrator</a></li>
          </ul>
          </li-->
          <li class="has-form">
            <div class="row collapse">
              <div class="large-10 small-10 columns">
                <input type="text" placeholder="Find Stuff" id='value_to_search'>
              </div>
              <div class="large-2 small-2 columns">
                <a href="#" class="alert button expand"><i class='fa fa-search'></i></a>
              </div>
            </div>
          </li>
          <li class="active show-for-small-only"><a href="#" id='tools'>TOOLS</a></li>
          <li class="for-small"><a href="#" id='tools'>TOOLS</a></li>
          <li class=""><a href="<?=base_url('index.php/dashboard/logout')?>" id='show-sign-up'>LOG OUT</a></li>
      </ul>
    </section>
  </nav>
</div>
  <div class="callout">
    <div class='small-8 medium-3 large-2 columns sideall  animated zoomIn' style='background-color:#364150;height:auto;'>
      <div class='row collapse' style='margin-top:30px;color:white;padding-bottom:30px;'>
         <div class='small-3 medium-4 large-6 columns'><img class='profile' src='<?=base_url('/assets/imgs/images.png')?>' style=''/></div>
         <div class='small-9 medium-8 large-6 columns'>
          <ul>
            <li id='welcome'>Welcome,</li>
            <li id='owner'><?php echo $this->session->userdata['user_details'][0]->firstname;?></li>
          </ul>
         </div>
        </div>
   <div class='row sidelist'style='border-top:1px solid grey;' >
       <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns collapse'> <i class="fa fa-tachometer"></i> &nbsp;Dashboard</div></a>
   </div>
    <!--div class='row sidelist'style='' >
       <a href='#' style='color:#b4bcc8;' id='pre_ongoing_ass'>
    <div class='small-12 medium-12 large-12 columns collapse' id='ongoing_ass'> <i class="fa fa-calendar"></i> &nbsp;Ongoing Assessment</div></a>
   </div>
   <div class='row sidelist'style='' >
       <a href='#' data-reveal-id="" style='color:#b4bcc8;' id='company_finished_ass' >
    <div class='small-12 medium-12 large-12 columns collapse'> <i class="fa fa-table"></i> &nbsp;Finished Assessment</div></a>
   </div-->
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' id='get_company_report' >
    <div class='small-12 medium-12 large-12 columns'><i class="fa fa-bar-chart"></i> &nbsp;Report &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='notif' style=''><?php echo $reports  ?></span></div></a>
   </div>
   <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' id='assess_order' >
    <div class='small-12 medium-12 large-12 columns'><i class="fa fa-bar-chart"></i> &nbsp;Assessment Orders</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' id='order' >
    <div class='small-12 medium-12 large-12 columns'><i class="fa fa-line-chart"></i> &nbsp;Invoice</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-map-marker"></i> &nbsp;Maps</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-cog"></i> &nbsp;Settings</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' ><div class='large-1 columns'></div>
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-cog"></i> &nbsp;Extra</div></a>
   </div>
    </div>
    <div class="small-12 medium-9 large-10 columns maincon  animated zoomIn" style='' >
        <div class='dashtop' style='margin-top:50px;'>
         <div class='row' style='margin-left:0px;'><div class='small-12 medium-12 large-12 columns' id='dash_title' style='color:grey'>Dashboard</div></div>
        </div><hr>
        <div class='hide'><input type='file' name='file[]' id='collectfile'/></div>
        <div  class='all_gen_reports'></div>
         <div class="com_finished_ass hide"></div>
         <div class='ongoing_ass hide'></div>
        <div class='activity'><a href="#" id='show_bar' class='hide'></a></div>
         <?php if(count($orders) > 0): $x=0;?>

               <div class='all_company_ass' style=''>
                <table class='table_for_assess for_company show-for-large-up' style='margin-left:20px'>
                  <tr class='table_head'>
                  <th class='table_content' width="auto">ID</th>
                  <th class='table_content' width:"auto">STATUS</th>
                  <th class='table_content' width="auto">AUDITOR</th>
                  <th class='table_content' width="auto">PERIOD</th>
                  <th class='table_content' width="auto">START DATE</th>
                  <th class='table_content' width="auto">END DATE</th>
                  <th class='table_content' width="auto">ASSESSMENT ID</th>
                  <th class='table_content' width='auto'>CREATED</th>
                  <th class='table_content' width='auto'>STAGE</th>
                  <th class='table_content' width='auto'>ACCESS</th>
                </tr>
              </tbody>
                <?php for($a=0;$a<count($orders);$a++): ?>
                  <?php if(count(explode(':',$orders[$a]['stage']))>1):?>
                        <?php list($first) = explode(':',$orders[$a]['stage']); ?>
                        <?php if($first =='message_from_auditor'):?>
                               <?php $r =  "Message from Auditor";?>
                        <?php elseif($first =='message_from_company'):?>
                               <?php $r =  "Message from Company";?>
                        <?php elseif($first =='Customer_invoices'):?>
                               <?php $r =  "Customer Invoices";?>       
                        <?php elseif($first =='Accepted'):?>
                                   <?php if($orders[$a]['stage']=='Statement_of_account:Accepted'):?>
                                      <?php $r =  "Statement of Account Accepted";?>
                                   <?php elseif($orders[$a]['stage']=='Customer_invoices:Accepted'):?>
                                      <?php $r =  "Customer Invoices Accepted";?>
                                   <?php elseif($orders[$a]['stage']=='payment_customer_invoices:Accepted'):?>
                                      <?php $r =  "Payment Customer Invoices Accepted";?>
                                   <?php elseif($orders[$a]['stage']=='payment_supplier_invoices:Accepted'):?>
                                      <?php $r =  "Payment Supplier Invoices Accepted";?>
                                   <?php elseif($orders[$a]['stage']=='supplier_invoices:Accepted'):?>
                                      <?php $r =  "Supplier Invoices Accepted";?>
                                   <?php endif;?>
                        <?php elseif($first =='Rejected'):?>
                                    
                                    <?php if($orders[$a]['stage']=='Statement_of_account:Rejected'):?>
                                       <?php $r =  "Statement of Account Rejected";?>
                                    <?php elseif($orders[$a]['stage']=='Customer_invoices:Rejected'):?>
                                       <?php $r =  "Customer Invoices Rejected";?>
                                    <?php elseif($orders[$a]['stage']=='payment_customer_invoices:Rejected'):?>
                                      <?php $r =  "Payment Customer Invoices Rejected";?>
                                    <?php elseif($orders[$a]['stage']=='payment_supplier_invoices:Rejected'):?>
                                      <?php $r =  "Payment Supplier Invoices Accepted";?>
                                    <?php elseif($orders[$a]['stage']=='supplier_invoices:Rejected'):?>
                                      <?php $r =  "Supplier Invoices Accepted";?>
                                    <?php endif;?>
                        <?php endif;?>
                <?php else:?>
                               <?php if($orders[$a]['stage'] =='Statement_of_account_uploaded'):?>
                               <?php $r =  "Statement of Account Uploaded";?>
                               <?php elseif($orders[$a]['stage'] =='Customer_invoices_uploaded'):?>
                               <?php $r =  "Customer Invoices Uploaded";?>
                             <?php elseif($orders[$a]['stage'] =='payment_customer_invoices_uploaded'):?>
                               <?php $r =  "Payment Customer Invoices Uploaded";?>
                               <?php elseif($orders[$a]['status']=='Open'):?>
                               <?php $r =  "Upload Statement of Account";?>
                               <?php elseif($orders[$a]['stage'] =='payment_supplier_invoices_uploaded'):?>
                               <?php $r =  "Payment Supplier Invoices Uploaded";?>
                               <?php elseif($orders[$a]['stage'] =='supplier_invoices_uploaded'):?>
                               <?php $r =  "Supplier Invoices Uploaded";?>
                             <?php elseif($orders[$a]['stage'] =='external_file_uploaded'):?>
                               <?php $r =  "External file Attached";?>
                             <?php elseif($orders[$a]['stage'] =='vat_generated'):?>
                               <?php $r =  "VAT Generated";?>
                             <?php elseif($orders[$a]['stage'] =='share generated vat with company'):?>
                               <?php $r =  "VAT Generated and Share with Company";?>
                             <?php elseif($orders[$a]['stage'] =='final_assessment_uploaded'):?>
                               <?php $r =  "Final Report uploaded";?>
                               <?php elseif($orders[$a]['stage'] =='Yet to Start'):?>
                               <?php $r =  $orders[$a]['stage'];?>
                               <?php endif;?>
                <?php endif;?>
                <tr><td><?php echo ($a+1)?></td>
                    <td class='table_sub'><?php echo ($orders[$a]['status']=='Work In Progress')?'WIP':strtoupper($orders[$a]['status']);?></td>
                    <td class='table_sub'><?php echo $orders[$a]['auditor_name']?></td>
                    <td class='table_sub'><?php echo $orders[$a]['period']?></td>
                    <td class='table_sub'><?php echo $orders[$a]['start_audit_time']?></td>
                    <td class='table_sub'><?php echo $orders[$a]['end_audit_time']?></td>
                    <td class='table_sub'><?php echo $orders[$a]['assessment_id']?></td>
                    <td class='table_sub'><?php echo $orders[$a]['created']?></td>
                    <td class='table_sub'><?php echo $r;?></td>
                    <?php if($orders[$a]['status']=='Planned'):?>
                    <td class='table_sub'><a href="#"  id='data<?php echo $orders[$a]['assessment_id']; ?>' data-id="<?php echo $orders[$a]['assessment_id']; ?>" class="button tiny" style='margin-top:12px'>Locked</a></td>
                    <?php elseif($orders[$a]['status'] =='Closed'):?>
                    <td class='table_sub'><a href="#"  id='data<?php echo $orders[$a]['assessment_id']; ?>' data-id="<?php echo $orders[$a]['assessment_id']; ?>" class="open_assess_for_company button tiny alert" style='margin-top:12px;padding-left:25px;padding-right:25px;'>Open</a></td>
                    <?php else:?>
                    <td class='table_sub'><a href="#" id='data<?php echo $orders[$a]['assessment_id']; ?>' data-id="<?php echo $orders[$a]['assessment_id']; ?>" class="open_assess_for_company button tiny success" style='margin-top:12px;padding-left:25px;padding-right:25px;'>Open</a></td>
                    <?php endif;?>
                  </tr>
             </tbody>
             <?php endfor;?>
            </table>
          </div>
          
            <?php for($a=0;$a<count($orders);$a++): ?>
                  <?php if(count(explode(':',$orders[$a]['stage']))>1):?>
                  <?php list($first) = explode(':',$orders[$a]['stage']); ?>
                   <?php if($first =='message_from_company'):?>
                     <?php $r =  "Message from Company";?>
                  <?php elseif($first =='message_from_auditor'):?>
                        <?php $r =  "Message from Auditor";?>
                  <?php elseif($first =='Customer_invoices'):?>
                               <?php $r =  "Customer Invoices";?>
                   <?php endif;?>
                <?php else:?>
                <?php if($assess_orders[$a]['stage'] ='Statement_of_account_uploaded'):?>
                 <?php $r =  "Statement of Account Uploaded";?>
                  <?php endif;?>
                <?php endif;?>
              <div class='callout show-for-medium-down assess_for_company'>
                <div class='small-12 medium-6 columns' style='padding:0px;margin:0px;margin-bottom:15px;'>
                  <ul style='border:1px solid #ddd;text-align:center;margin-bottom:10px;padding:0px;margin:0px;'>
                    <div class='callout' style='color:white;padding:5px;'><div class='small-6 medium-6 columns' style='background-color:#3598dc;'>Status</div><div class='small-6 medium-6 columns' style='background-color:#e7505a;'><?php echo ($orders[$a]['status']=='Work In Progress')?'WIP':strtoupper($orders[$a]['status']);?></div></div>
                    <!--div style='background-color:#3598dc;color:white;'><li>Status</li>
                      <li style='margin-left:auto;margin-right:auto;background-color:#e7505a;'><?php echo ($orders[$a]['status']=='Work In Progress')?'WIP':strtoupper($orders[$a]['status']);?></li></div-->
                    <table width="100%">
                      <tr><td width="30%">Auditor</td><td width="70%"><?php echo $orders[$a]['auditor_name']?></td></tr>
                      <tr><td width="40%">Assessment ID</td><td width="60%"><?php echo $orders[$a]['assessment_id']?></td></tr>
                      <tr><td width="30%">Start Date</td><td width="70%"><?php echo $orders[$a]['start_audit_time']?></td></tr>
                      <tr><td width="30%">End Date</td><td width="70%"><?php echo $orders[$a]['end_audit_time']?></td></tr>
                      <tr><td width="30%">Period</td><td width="70%"><?php echo $orders[$a]['period']?></td></tr>
                      <tr><td width="30%">Stage</td><td width="70%"><?php echo $r;?></td></tr>
                      </table>
                      <?php if($orders[$a]['status']=='Planned'):?>
                    <div class='row'><div class='small-4 small-offset-4 medium-offset-4 columns end'><a href="#"  id='data<?php echo $orders[$a]['assessment_id']; ?>' data-id="<?php echo $orders[$a]['assessment_id']; ?>" class="button tiny" style='margin-top:12px'>Locked</a></div></div>
                    <?php elseif($orders[$a]['status'] =='Closed'):?>
                    <div class='row'><div class='small-4 small-offset-4 medium-offset-4 columns end'><a href="#"  id='data<?php echo $orders[$a]['assessment_id']; ?>' data-id="<?php echo $orders[$a]['assessment_id']; ?>" class="open_assess_for_company button tiny alert" style='margin-top:12px;padding-left:25px;padding-right:25px;'>Open</a></div></div>
                    <?php else:?>
                    <div class='row'><div class='small-4 small-offset-4 medium-offset-4 columns end'><a href="#" id='data<?php echo $orders[$a]['assessment_id']; ?>' data-id="<?php echo $orders[$a]['assessment_id']; ?>" class="open_assess_for_company button tiny success" style='margin-top:12px;padding-left:25px;padding-right:25px;'>Open</a></div></div>
                    <?php endif;?>
                    </table>
                  </ul>
                </div>
              </div>
            <?php endfor; ?>              
            <?php endif;?>
          <?php if(count($orders)==0):?>
            <div class='assessment_order coord_orders show-for-large-up'>No Assessment Order</div>
            <div class='assessment_order coord_orders show-for-medium-down'>No Assessment Orderqwerty</div>
            <style type="text/css">
               .coord_orders{
                border: none;
                width:300px;height:300px;border-radius:150px;background-color:white;margin-left:auto;margin-right:auto;text-align: center;padding-top: 150px;font-size: 1.2em;color:grey;
               }
             </style>
           <?php endif;?>
    </div>
    <div id="allconversations" class='reveal-modal' style='background-color:#F5F5F5' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
              <div class="row" style='margin-bottom:40px;'><div class="small-12 medium-2 large-3 columns header" style='padding-left:5px;padding-right:5px;font-size:1.2em'>MESSAGE</div></div>
             <div id='allchats'></div>
              <div class="row" id='input_msg'>
                <div class="large-12 columns">
                  <div class="row collapse">
                    <div class="small-10 columns">
                      <input type="text" placeholder="Hex Value" id='text_to'>
                    </div>
                    <div class="small-2 columns">
                      <a href="#" class="button postfix" id='send_msg'>Go</a>
                    </div>
                  </div>
                </div>
              </div>
              <a class="close-reveal-modal" style='color:red;' aria-label="Close">&#215;</a>
      </div>
     <div id="Morefile" class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
             <div class="row"><div class="small-12 medium-5 large-5 columns header">REQUESTED FILE</div></div>
             <div class='row' style='margin-top:40px;color:grey;font-size:1.2em;'><div class='small-12 medium-12 large-12 columns'>FIRS Auditor Request:</div></div><hr style='margin-top:-5px;'>
             <div class='auditor_request' style='font-size:0.8em;margin-left:30px;'></div>
             <div class='row' style='margin-top:40px;color:grey;font-size:1.2em;'><div class='small-12 medium-12 large-12 columns'>Upload Requested File:</div></div><hr style='margin-top:-5px;'>
             <div class='row'><div class='small-12 medium-12 large-12 columns'><ul style='text-align:center;' class='listfile'></ul></div></div>
            <div class='row'><div class='small-3 medium-3 large-3 columns' style='margin-top:20px;'><a href='#' class='addfile'><i class='fa fa-upload'></i>Add File</a></div>
          <div class='small-9 medium-9 large-9 columns'><a href="#" class='button success expand' id=''>Upload File</a></div>
             <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
    <div id="Upfile" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row"><div class="small-12 medium-4 large-4 columns header" style='padding:5px 5px 5px 20px;'>UPLOAD FILE</div></div>
            <div class='row' style='margin-top:40px;'>
              <div class='small-12 medium-12 large-12 columns fin_title'>Annual Financial Statement Period</div>
            </div><hr style='margin-top:-3px;'>
            <div class='row'>
              <div class='small-4 medium-4 large-4 columns'>
                <select id='from'>
                  <option value="">Begin</option>
                  <option value="January">January</option>
                  <option value="February">February</option>
                  <option value="March">March</option>
                  <option value="April">April</option>
                  <option value="May">May</option>
                  <option value ="June">June</option>
                  <option value="July">July</option>
                  <option value="August">August</option>
                  <option value="September">September</option>
                  <option value="October">October</option>
                  <option value ="November">November</option>
                  <option value="December">December</option>
                </select>
              </div>
               <div class='small-4 medium-4 large-4 columns'>
                <select id='to'>
                  <option value="January">January</option>
                  <option value="February">February</option>
                  <option value="March">March</option>
                  <option value="April">April</option>
                  <option value="May">May</option>
                  <option value ="June">June</option>
                  <option value="July">July</option>
                  <option value="August">August</option>
                  <option value="September">September</option>
                  <option value="October">October</option>
                  <option value ="November">November</option>
                  <option value="December">December</option>
                </select>
              </div>
               <div class='small-4 medium-4 large-4 columns'>
                <select id='yr'>
                  <option>year</option>
                  <option value="2016">2016</option>
                  <option value="2017">2017</option>
                  <option value="2018">2018</option>
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                  <option value="2024">2024</option>
                  <option value="2025">2025</option>
                  <option value="2026">2026</option>
                  <option value="2027">2027</option>
                </select>
              </div>
            </div>
            <div class='row'><div class='small-12 medium-12 large-12 columns'><ul style='text-align:center;' class='listfile'></ul></div></div>
            <div class='row'><div class='small-3 medium-4 large-4 columns' style='margin-top:20px;'><a href='#' class='addfile'><i class='fa fa-upload'></i> Add File</a></div>
          <div class='small-9 medium-8 large-8 columns'><a href="#" class='button success expand' id='sendfile'>Upload File</a></div>
          </div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
   </div>
   <div id="change_pass" class='reveal-modal' data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
             <div>
              <div class="row" style='margin-bottom:40px;'><div class="small-12 medium-5 large-5 columns header" style='padding-left:15px;padding-right:5px;font-size:1.2em'>CHANGE PASSWORD</div></div>
             </div>
             <div class='row' style='margin-bottom:5px;margin-top:40px;'><div class='small-12 medium-12 large-12 columns'><input type="password" placeholder="Passsword" id='ch_pass'></div></div>
             <div class='row' style='margin-bottom:5px;'><div class='small-12 medium-12 large-12 columns'><input type="password" placeholder="Confirm Passsword" id='ch_cpass'></div></div>
             <div class='row'><div class='small-12 medium-12 large-12 columns'><a href="#" class='button success expand' id='to_change_pass'>Change Password</a></div></div>
              <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
     

</body>
 <script>
    $(document).foundation();
  </script>
  <?php if($to_pass=='1'):?>
     <script type="text/javascript">
           $(document).ready(function(e){
         $('#change_pass').foundation('reveal', 'open');
       });
     </script>
     <?php $to_pass ='0';?>
    <?php endif;?>
</body>
</html> 