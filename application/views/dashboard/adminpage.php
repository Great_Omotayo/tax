<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.css'/>
	<link rel="stylesheet" href="<?= base_url('assets/css/taxassurance.css'); ?>"/>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.topbar.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.reveal.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.dropdown.js"></script>
<script type="text/javascript" src="<?= base_url('assets/js/taxassurance.js'); ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.concat.min.js">
</script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.css">
</head>
<body style='background-color:#F5F5F5;font-family: Roboto sans-serif;margin-left:5px;margin-right:5px;'>
	    <div class="wrapper">	
          <nav class="top-bar sticky" data-topbar role="navigation">
        <ul class="title-area">
         <li class="name">
          <h1 class=''><a href="#">Data Assurance Hub</a></h1>
         </li>
       <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
         <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

    <section class="top-bar-section">
      <!-- Right Nav Section -->
      <ul class="right">
        <!--li class=""><a href="#" data-reveal-id="gen_invoice" style=''>Generate Invoice</a></li>
        <li class=""><a href="#" data-reveal-id="add_fir_member" style=''>Add Firs Team</a></li>
          <li class="active"><a href="#" data-reveal-id="create_company" style=''>Create Company</a></li-->
            <li class="active show-for-small-only"><a href="#" id='tools'>TOOLS</a></li>
          <li class="for-small"><a href="#" id='tools'>TOOLS</a></li>
          <li class=""><a href="<?=base_url('index.php/dashboard/logout')?>" style='' >Log Out</a></li>
      </ul>
    </section>
   </nav>
		<!--nav class="top-bar sticky" data-topbar role="navigation">
	      <ul class="title-area">
	       <li class="name">
	        <h1 class='show-for-medium-up'><a href="#">Data Assurance Hub</a></h1>
          <h1 class='show-for-small-only'><a href="#">Dashboard</a></h1>
	       </li>
	       <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	      </ul>

	  <section class="top-bar-section">
	    <ul class="right">
        <li class=""><a href="#" data-reveal-id="gen_invoice" style='font-size:1.0em'>Generate Invoice</a></li>
        <li class=""><a href="#" data-reveal-id="add_fir_member" style='font-size:1.0em'>Add Firs Team</a></li>
      	  <li class="active"><a href="#" data-reveal-id="create_company" style='font-size:1.0em'>Create Company</a></li>
          <li class=""><a href="<?=base_url('index.php/dashboard/logout')?>" style='font-size:1.0em' >Log Out</a></li>
	    </ul>


	    <ul class="left">
	      <li><a href="#"></a></li>
	    </ul>
	  </section>
	 </nav-->
   <div class="callout">
    <div class=' medium-3 large-2 columns sideall' style='background-color:#364150;height:auto;'>
      <div class='row collapse' style='margin-top:30px;color:white;padding-bottom:30px;'>
         <div class='small-3 medium-4 large-6 columns'><img class='profile' src='<?=base_url('/assets/imgs/images.png')?>' style=''/></div>
         <div class='small-9 medium-8 large-6 columns left'>
          <ul>
            <li id='welcome'>Welcome,</li>
            <li id='owner'><?php echo  $this->session->userdata['user_details'][0]->firstname.' '.$this->session->userdata['user_details'][0]->lastname;?></li>
          </ul>
         </div>
        </div>
   <div class='row sidelist'style='border-top:1px solid grey;' >
       <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns collapse'> <i class="fa fa-tachometer"></i> &nbsp;Dashboard</div></a>
   </div>
    <div class='row sidelist'style='' >
       <a href='#' style='color:#b4bcc8;' data-reveal-id="create_company" >
    <div class='small-12 medium-12 large-12 columns collapse'> <i class="fa fa-calendar"></i> &nbsp;Coordinators</div></a>
   </div>
   <div class='row sidelist'style='' >
       <a href='#' style='color:#b4bcc8;' >
    <div class='small-10 medium-10 large-10 columns collapse'> <i class="fa fa-table"></i> &nbsp;Auditors</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-10 medium-10 large-10 columns'><i class="fa fa-bar-chart"></i> &nbsp;Charts</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-10 medium-10 large-10 columns'><i class="fa fa-line-chart"></i> &nbsp;Data Presentations</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-map-marker"></i> &nbsp;Maps</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-cog"></i> &nbsp;Settings</div></a>
   </div>
    <div class='row sidelist' style=''>
      <a href='#' style='color:#b4bcc8;' >
    <div class='small-12 medium-12 large-12 columns'> <i class="fa fa-cog"></i> &nbsp;Extra</div></a>
   </div>
    </div>
	 <div class="medium-9 large-10 columns  maincon" style='' >
        <div class='dashtop' style='margin-top:50px;'>
         <div class='row' style='margin-left:0px;'><div class='small-12 medium-12 large-12 columns'>Dashboard</div></div>
        </div><hr>
      <div class='row'>
        <div class='small-12 medium-4 large-3 large-offset-1 columns box' style='text-align:center;margin-bottom:20px;'>
           <ul class=''>
            <a href="#" class='' data-reveal-id="create_company">
              <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-suitcase fa-2x" style='margin-top:7px;'></i></li>
            <li><div class="row text_on_ass"><div class="small-12 medium-12 large-12 column"></span></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div></li>
            <li class='name'>Add Company</li></a>
          </ul>
        </div>
        <div class='small-12 medium-4 large-3 large-offset-1  columns box left' style='text-align:center;background-color:#e7505a;margin-bottom:20px;'>
           <ul class=''>
            <a href="#" class='' data-reveal-id="coordinator">
              <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-user fa-2x" style='margin-top:7px;'></i></li>
            <li><div class="row text_on_ass"><div class="small-12 medium-12 large-12 column"></span></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div></li>
            <li class='name'>Add Coordinator</li></a>
          </ul>
        </div>
         <div class='small-12 medium-4 large-3 large-offset-1 columns box left' style='text-align:center;'>
           <ul class=''>
            <a href="#" class='' data-reveal-id="auditor">
              <li class='' style='border:2px solid white;width:50px;height:50px;padding-top:auto;padding-bottom:auto;margin-left:auto;margin-right:auto;border-radius:30px;margin-bottom:5px;'><i class="fa fa-user fa-2x" style='margin-top:7px;'></i></li>
            <li><div class="row text_on_ass"><div class="small-12 medium-12 large-12 column"></span></div>
            </div></li>
            <li ><div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div></li>
            <li class='name'>Add Auditor</li></a>
          </ul>
        </div>
      </div>
	 </div>
  </div>

	  <div id="coordinator" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style='padding:0px 0px 0px 0px;'>
          <div class="row add_company"><div class="small-5 medium-5 large-5 columns header" style='padding:5px 5px 5px 15px'>ADD COORDINATOR</div></div>
          <div class="row" style='margin-top:20px;'><div class="small-12 medium-12 large-12 columns"><input id='coord_name' type='text' placeholder="Name"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input id='coord_phone' type='text' placeholder="Phone Number"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input id='coord_email' type='email' placeholder="email"/></div></div>
          <div class='row'><div class="small-12 medium-12 large-12 columns">
          </div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns">
            <select id='coord_region'>
              <option>Attach Region</option>
              <option value='lagos'>Lagos</option>
            </select>
          </div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><a href="#" class="button expand success add_coord">Add Coordinator</a></div></div>

		  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
	    </div>
      <div id="auditor" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog" style='padding:0px 0px 0px 0px;'>
          <div class="row add_company"><div class="small-12 medium-4 large-4 columns header" style='padding:5px 5px 5px 20px'>ADD AUDITOR</div></div>
          <div class="row" style='margin-top:20px;'><div class="small-12 medium-12 large-12 columns"><input id='auditor_name' type='text' placeholder="Name"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input id='auditor_phone' type='text' placeholder="Phone Number"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input id='auditor_email' type='email' placeholder="email"/></div></div>
          <div class='row'><div class="small-12 medium-12 large-12 columns">
          </div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns">
            <select id='auditor_region'>
              <option>Attach Region</option>
              <option value='lagos'>Lagos</option>
            </select>
          </div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><a href="#" class="button expand success add_auditor">Add Auditor</a></div></div>

      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
        <div id="gen_invoice" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row add_company"><div class="small-12 medium-12 large-12 columns">GENERATE INVOICE</div></div>
          <div class='row'><div class="small-12 medium-12 large-12 columns">
             <select id='forcompany'>
            </select>
          </div></div>
          <div class="row">
            <div class="small-12 medium-12 large-12 columns">
              
            </div>
          </div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><a href="#" class="button expand success add_firs_team">Generate Invoice</a></div></div>

      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
	    
	    <div id="create_company" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row add_company"><div class="small-5 medium-4 large-4 columns header" style='padding:5px 5px 5px 10px;'>ADD COMPANY</div></div>
          <div class="row" style='margin-top:20px;'><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_name' placeholder="company Name"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_add'  placeholder="company Address"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id="company_tin" placeholder="tin id"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_rc' placeholder="RC Number"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_bvn' placeholder="BVN"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id ='company_tax_zone' placeholder="tax Zone(Region)"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_admin' placeholder="company administrator Name"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='email' id='company_email' placeholder="company administrator email"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><input type='text' id='company_phone' placeholder="company administrator Phone"/></div></div>
          <div class="row"><div class="small-12 medium-12 large-12 columns"><a href="#" id='add_company' class="button expand success">Add Company</a></div></div>

		  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
	    </div>
            <div id="list_company" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row" style=''><div class="small-3 medium-3 large-3 columns header">COMPANIES</div></div>
          <div class='scroll-y'>
            <div id='render_company'>
          <div style='width:98%;tex-align:center;margin-left:auto;margin-right:auto;'>
            <div class="row collapse" style='margin-top:20px;'>
            <div class="small-2 medium-2 large-2 columns" style='text-align:center;' id='company_icon'>C</div>
            <div class="small-10 medium-10 large-10 columns">
            <ul>
                <li style='font-size:1.5em;margin-top:-9px;'>Company Name</li>
                <li style='font-size:0.8em;margin-top:-9px;'>Company Address</li>
                <li style='font-size:0.8em;margin-top:-4px;'>Tax Zone</li>
            </ul>
            </div></div>
           </div> 
            <div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div>
          </div></div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
      <div id="files" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
          <div class="row" style=''><div class="small-3 medium-3 large-3 columns  header">Uploaded Files</div></div>
          <div class='scroll-y'>
          <div id='render_files' style='width:98%;tex-align:center;margin-left:auto;margin-right:auto;margin-top:25px;'>
            <div class="row collapse" style='margin-top:20px;'>
            <div class="small-2 medium-2 large-2 columns" style='text-align:center;' id='company_icon'>C</div>
            <div class="small-10 medium-10 large-10 columns">
            <ul>
                <li style='font-size:1.5em;margin-top:-9px;'>Company Name</li>
                <li style='font-size:0.8em;margin-top:-9px;'>Company Address</li>
                <li style='font-size:0.8em;margin-top:-4px;'>Tax Zone</li>
            </ul>
            </div></div>
           </div> 
            <div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div>
          </div>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>
	</div>
        
	<script>
    $(document).foundation();
  </script>
</body>
</html>