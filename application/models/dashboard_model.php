<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Dashboard_model extends CI_Model {
    

    public function get_companies(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('*');
      $this->db->join('company','company.fir_admin=fir_team_members.fir_admin_id');
      $res = $this->db->get_where('fir_team_members',array('fir_team_members.fir_member_id'=>$id));
      return $res->result();
    } 
     public function get_auditors(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('*');
      $this->db->join('all_users','fir_team_members.fir_member_id=all_users.id');
      $res = $this->db->get_where('fir_team_members',array('fir_team_members.category'=>'auditor'));
      return $res->result();
    } 

    public function get_assigned_task(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('*');
      $this->db->join('company','company.id=task_for_firs_team.task_on_id');
      $this->db->join('all_users','all_users.id = task_for_firs_team.task_to_id');
      $this->db->join('assessment','assessment.Assessment_id=task_for_firs_team.assessment_id');
      $res  = $this->db->get_where('task_for_firs_team',array('task_for_firs_team.assigned_by_id'=>$id));
       $r = array();
       
       foreach ($res->result() as $value) {
         $time = explode(';',unserialize($value->audit_time));
        $period = explode(';',unserialize($value->financial_statement_period));
        $r[] = array('auditor_name'=>$value->firstname,
                'company_name'=>$value->Name,
                'tax_zone'=>$value->tax_zone,
                'start_audit_time'=>$time[0] .' of '. $time[1] .' '.$time[4],
                'end_audit_time'=>$time[2] .' of '. $time[3] .' '.$time[4],
                'period'=>$period[0] .' to '. $period[1].' '.$period[2],
                'assessment_id'=>$value->assessment_id,
                'status'=>$value->status,
                'created'=>$date = date('d-m-Y', $value->created),
                );
      }
      return $r;
    }
    public function get_auditor_assigned_task(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('task_for_firs_team.id,task_for_firs_team.assessment_id,assessment.status,assessment.stage,task_for_firs_team.created,task_for_firs_team.financial_statement_period,task_for_firs_team.audit_time,all_users.firstname,company.Name');
      $this->db->join('company','company.id=task_for_firs_team.task_on_id');
      $this->db->join('all_users','all_users.id = task_for_firs_team.task_to_id');
      $this->db->join('assessment','assessment.Assessment_id=task_for_firs_team.assessment_id');
      $res  = $this->db->get_where('task_for_firs_team',array('task_for_firs_team.task_to_id'=>$id));
       $r = array();
       
       foreach ($res->result() as $value) {
         $time = explode(';',unserialize($value->audit_time));
         $stage= explode(';',unserialize($value->stage));
        $num = count($stage);
        $period = explode(';',unserialize($value->financial_statement_period));
        $r[] = array('id'=>$value->id,
                'auditor_name'=>$value->firstname,
                'company_name'=>$value->Name,
                'start_audit_time'=>$this->day($time[0]) .' of '. $this->month($time[1]) .' '.$time[4],
                'end_audit_time'=>$this->day($time[2]) .' of '. $this->month($time[3]) .' '.$time[4],
                'period'=>$period[0] .' to '. $period[1].' '.$period[2],
                'assessment_id'=>$value->assessment_id,
                'status'=>$value->status,
                'created'=>$date = date('d-m-Y', $value->created),
                'stage'=>($value->stage)?$stage[$num-1]:'Yet to Start',
                );
      }
      return $r;
    }

   public function get_company_on_ass($assess){
    if($assess !=""){
      $get_one = 'true';
    }
    else{
      $get_one = 'false';
    }
    $id = $this->session->userdata['user_details'][0]->id;
    $this->db->select('assessment.Assessment_id,company.Name,assessment.stage,assessment.status,assessment.created,task_for_firs_team.audit_time,task_for_firs_team.financial_statement_period,assessment.auditor_id,company.id as company_id,all_users.firstname');
    $this->db->join('task_for_firs_team','task_for_firs_team.task_on_id = company.id');
    $this->db->join('all_users','all_users.id = task_for_firs_team.task_to_id');
    $this->db->join('assessment','assessment.assessment_id=task_for_firs_team.assessment_id');
    if($get_one=='false'){
    $e  = $this->db->get_where('company',array('company.company_admin_id'=>$id));
    }
    else if($get_one =='true'){
      $e  = $this->db->get_where('company',array('company.company_admin_id'=>$id,'assessment.Assessment_id'=>$assess));
    }
    $r = array();
       
       foreach ($e->result() as $value) {
        $audit_time = unserialize($value->audit_time);
        $break_time = explode(';',$audit_time);
        $stage= explode(';',unserialize($value->stage));
        $period = explode(';',unserialize($value->financial_statement_period));
        $num = count($stage);
        $r[] = array(
                  'audit_time'=> $this->day($break_time[0]).' '.$this->month($break_time[1]).' to'.$this->day($break_time[2]).' '.$this->month($break_time[3]).' '.$break_time[4],
                 'stage'=>($value->stage)?$stage[$num-1]:'Yet to Start',
                'assessment_id'=>$value->Assessment_id,
                'status'=>$value->status,
                 'period'=>$period[0] .' to '. $period[1].' '.$period[2],
                'auditor_name'=>$value->firstname,
                'created'=>$date = date('d-m-Y', $value->created),
                'auditor_id'=>$value->auditor_id,
                'company_id' =>$value->company_id,
                'all_flow' =>($value->stage!='')?$stage:'',
                'start_audit_time'=>$this->day($break_time[0]) .' of '. $this->month($break_time[1]) .' '.$break_time[4],
                'end_audit_time'=>$this->day($break_time[2]) .' of '. $this->month($break_time[3]) .' '.$break_time[4],
                );
      }
      return $r;
   }
   public function get_ongoing_ass_for_auditor(){
    $id = $this->session->userdata['user_details'][0]->id;
    $this->db->select('assessment.Assessment_id,company.Name,company.id as company_id,assessment.stage,assessment.status,assessment.created,task_for_firs_team.audit_time');
    $this->db->join('task_for_firs_team','assessment.Assessment_id=task_for_firs_team.assessment_id');
        $this->db->join('company','company.id=task_for_firs_team.task_on_id');
    $e = $this->db->get_where('assessment',array('assessment.auditor_id'=>$id));
     $r = array();
       
       foreach ($e->result() as $value) {
        $stage= explode(';',unserialize($value->stage));
        $num = count($stage);
        $r[] = array(
                 'Name'=>$value->Name,
                 'company_id'=>$value->company_id,
                 'stage'=>($value->stage)?$stage[$num-1]:'',
                'assessment_id'=>$value->Assessment_id,
                'status'=>$value->status,
                'audit_time'=>$value->audit_time,
                'created'=>$date = date('d-m-Y', $value->created),
                'all_flow'=>($value->stage!='')?$stage:'',
                );
      }
      return $r;
   }
    public function get_one_ongoing_ass_for_auditor($assessment){
    //$id = $this->session->userdata['user_details'][0]->id;
    $this->db->select('assessment.Assessment_id,company.Name,company.id as company_id,assessment.stage,assessment.status,assessment.created,task_for_firs_team.audit_time');
    $this->db->join('task_for_firs_team','assessment.Assessment_id=task_for_firs_team.assessment_id');
        $this->db->join('company','company.id=task_for_firs_team.task_on_id');
    $e = $this->db->get_where('assessment',array('assessment.Assessment_id'=>$assessment));
     $r = array();
       
       foreach ($e->result() as $value) {
        $stage= explode(';',unserialize($value->stage));
        $num = count($stage);
        $r[] = array(
                 'Name'=>$value->Name,
                 'company_id'=>$value->company_id,
                 'stage'=>($value->stage)?$stage[$num-1]:'',
                'assessment_id'=>$value->Assessment_id,
                'status'=>$value->status,
                'audit_time'=>$value->audit_time,
                'created'=>$date = date('d-m-Y', $value->created),
                'all_flow'=>($value->stage!='')?$stage:'',
                );
      }
      return $r;
   }
   public function get_files($assess_id,$whichfile){
    //$id = $this->session->userdata['user_details'][0]->id;
    $this->db->select('files,financial_record_period');
    $res = $this->db->get_where('uploaded_file',array('assessment_id'=>$assess_id,'filename'=>$whichfile));
     $r = array();
       
       foreach ($res->result() as $value) {
        $r[] = array(
                'files'=>$value->files,
                'period'=>unserialize($value->financial_record_period),
                );
      }
      return $r;
   }
    
    public function get_team_member(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('fir_team_members.fir_member_id,fir_team_members.category,fir_team_members.region,all_users.firstname,all_users.lastname,all_users.email,all_users.phone');
      $this->db->join('all_users','fir_team_members.fir_member_id=all_users.id');
      $r = $this->db->get_where('fir_team_members',array('fir_admin_id'=>$id));
      return $r->result();

    }
    public function get_chats($assess_id,$filename){
      if($this->session->userdata['user_details'][0]->is_company_admin==1){
        $this->db->select('company.id');
        $p = $this->db->get_where('Company',array('Company.company_admin_id'=>$this->session->userdata['user_details'][0]->id));
        $id = '';
       foreach ($p->result() as $value) {
          $id = $value->id;
          break;
       }
        //$id= $p->result()[0]->id;
      }
      else {
        $id = $this->session->userdata['user_details'][0]->id;
      }
      $this->db->select('*');
      $this->db->join('task_for_firs_team','task_for_firs_team.assessment_id=chats.Assessment_id');
      $res = $this->db->get_where('chats',array('chats.assessment_id'=>$assess_id,'type'=>$filename));
      $r = array();
       foreach ($res->result() as $value) {
        $r[] = array(
                'task_to_id'=>$value->task_to_id,
                'task_on_id'=>$value->task_on_id,
                'message'=>$value->message,
                'sender'=>$value->sender_id,
                'me'=>$id,
                'type'=>$value->type,
                );
      }
      return $r;
    }
    public function get_company_chats($assess_id){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('id');
      $company_id = $this->db->get_where('company',array('company_admin_id'=>$id));
      $com_id = '';
       foreach ($company_id->result() as $value) {
          $com_id = $value->id;
          break;
       }
      $this->db->select('*');
      $this->db->join('task_for_firs_team','task_for_firs_team.assessment_id=chats.Assessment_id');
      $res = $this->db->get_where('chats',array('chats.assessment_id'=>$assess_id));
      $r = array();
       foreach ($res->result() as $value) {
        $r[] = array(
                'task_to_id'=>$value->task_to_id,
                'task_on_id'=>$value->task_on_id,
                'message'=>$value->message,
                'sender'=>$value->sender_id,
                'me'=>$com_id,
                'type'=>$value->type,
                );
      }
      return $r;
    }
    public function month($month){
      If($month =="January"){
        return "Jan";
      }
      else if($month =="February"){
        return "Feb";
      }
      else if($month =="March"){
        return "Mar";
      }
      else if($month='April'){
        return "Apr";
      }
      else if($month='May'){
        return "May";
      }
      else if($month='June'){
        return "June";
      }
      else if($month='July'){
        return "July";
      }
      else if($month='August'){
        return "Aug";
      }
      else if($month='September'){
        return "Sept";
      }
      else if($month='October'){
        return "Oct";
      }
      else if($month='November'){
        return "Nov";
      }
      else if($month='December'){
        return "Dec";
      }

    }
    public function day($day){
     if(substr($day, -1)=='1' && $day !='11'){
     return $day.'<sup>st</sup>';
     }
     else if(substr($day, -1)=='2' && $day!='12'){
      return $day.'<sup>nd</sup>';
     }
     else if(substr($day, -1) == '3' && $day!='13'){
      return $day.'<sup>rd</sup>';
     }
     else{
       return $day.'<sup>th</sup>';
     }
    }
    public function get_all_assigned_task(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('task_for_firs_team.id,task_for_firs_team.assessment_id,assessment.status,assessment.stage,task_for_firs_team.created,task_for_firs_team.financial_statement_period,task_for_firs_team.audit_time,all_users.firstname,company.Name');
      $this->db->join('company','company.id=task_for_firs_team.task_on_id');
      $this->db->join('all_users','all_users.id = task_for_firs_team.task_to_id');
      $this->db->join('assessment','assessment.Assessment_id=task_for_firs_team.assessment_id');
      $res  = $this->db->get_where('task_for_firs_team',array('task_for_firs_team.assigned_by_id'=>$id));
       $r = array();
       
       foreach ($res->result() as $value) {
         $time = explode(';',unserialize($value->audit_time));
        $period = explode(';',unserialize($value->financial_statement_period));
        $stage= explode(';',unserialize($value->stage));
        $num = count($stage);
        $r[] = array('id'=>$value->id,
                'auditor_name'=>$value->firstname,
                'company_name'=>$value->Name,
                'start_audit_time'=>$this->day($time[0]) .' of '. $this->month($time[1]) .' '.$time[4],
                'end_audit_time'=>$this->day($time[2]) .' of '. $this->month($time[3]) .' '.$time[4],
                'period'=>$period[0] .' to '. $period[1].' '.$period[2],
                'assessment_id'=>$value->assessment_id,
                'status'=>$value->status,
                'created'=>$date = date('d-m-Y', $value->created),
                'stage'=>($value->stage)?$stage[$num-1]:'Yet to Start',
                );
      }
      return $r;
    }
    public function get_all_coord_ongoing(){
        $id = $this->session->userdata['user_details'][0]->id;
    $this->db->select('assessment.Assessment_id,company.Name,company.id as company_id,assessment.stage,assessment.status,assessment.created,task_for_firs_team.audit_time');
    $this->db->join('task_for_firs_team','assessment.Assessment_id=task_for_firs_team.assessment_id');
        $this->db->join('company','company.id=task_for_firs_team.task_on_id');
    $e = $this->db->get_where('assessment',array('task_for_firs_team.assigned_by_id'=>$id));
     $r = array();
       
       foreach ($e->result() as $value) {
        $stage= explode(';',unserialize($value->stage));
        $num = count($stage);
        $audit_time = explode(';',unserialize($value->audit_time));
        $r[] = array(
                 'Name'=>$value->Name,
                 'company_id'=>$value->company_id,
                 'stage'=>($value->stage)?$stage[$num-1]:'',
                'assessment_id'=>$value->Assessment_id,
                'status'=>$value->status,
                'audit_time'=>$audit_time[0] .' of ' .$audit_time[1]. ' to ' .$audit_time[2]. ' of ' .$audit_time[3]. ' ' .$audit_time[4],
                'created'=>$date = date('d-m-Y', $value->created),
                'all_flow'=>($value->stage!='')?$stage:'',
                );
      }
      return $r;
    }
    public function get_all_auditor_report(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('report,company.Name,assessment_id,created');
      $this->db->join('company','company.id = report.company_id');
      $report = $this->db->get_where('report',array('report.auditor_id'=>$id));
      $r =array();
      foreach ($report->result() as $value) {
          $r[] =array(
                   'Name'=>$value->Name,
                    'report'=>$value->report,
                    'created'=> $date = date('d-m-Y', $value->created),
                    'assessment_id' =>$value->assessment_id
                  );
      }
      return $r;
    }
    public function get_all_coord_report(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('task_to_id');
      $res = $this->db->get_where('task_for_firs_team',array('task_for_firs_team.assigned_by_id'=>$id));
      $r = array();
      $unique = array();
      foreach ($res->result() as $value) {
        if(!in_array($value->task_to_id, $r))
         $r[] = $value->task_to_id; 
      }
     foreach ($r as $v) {
         $this->db->select('report.report,report.created,company.Name,report.assessment_id');
         $this->db->join('company','company.id=report.company_id');
         $response  = $this->db->get_where('report',array('report.auditor_id'=>$v));
         foreach ($response->result() as $e) {
           $unique[] = array('created'=>date('d-m-Y', $e->created),'report'=>$e->report,'Name'=>$e->Name,'assessment_id'=>$e->assessment_id);
         }
     }
     return $unique;
    }
    public function get_all_company_report(){
      $id = $this->session->userdata['user_details'][0]->id;
      $this->db->select('assessment.Assessment_id,company.Name,company.id as company_id,assessment.stage,assessment.status,assessment.created,task_for_firs_team.audit_time');
      $this->db->join('task_for_firs_team','assessment.Assessment_id=task_for_firs_team.assessment_id');
      $this->db->join('company','company.id=task_for_firs_team.task_on_id');
      $e = $this->db->get_where('assessment',array('company.company_admin_id'=>$id));
      $assess_id = array();
      foreach ($e->result() as $value) {
       $stage= explode(';',unserialize($value->stage)); 
       $is_report = in_array('show_report', $stage);
       if($is_report =='1'){
        $assess_id[] = $value->Assessment_id;
       }
      }
      //echo count($assess_id);
       $t=array();
      if(count($assess_id)>0){
       
        for($b=0;$b<count($assess_id);$b++){
        $this->db->select('*');
        $res = $this->db->get_where('report',array('assessment_id'=>$assess_id[$b]));
         foreach ($res->result() as $value) {
           $t[]=array('assessment_id'=>$value->assessment_id,'report'=>$value->report,'created'=>date('d-m-Y', $value->created));
         }
        }
        
      }
      return $t;
    }
    
  } 

