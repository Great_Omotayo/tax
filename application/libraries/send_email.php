<?php

class Send_email 
{   

    public function send_mail($receiver_mail,$subject,$msg,$filename=false){
 
    $config = array();
 
    $config['api_key'] = "key-933bf1dfdf520db25cc31030fcd43c29";
 
    $config['api_url'] = "https://api.mailgun.net/v2/reale.com.ng/messages";

    $message = array();

    $message['from'] = "My Company &lt;Tax Office taxoffice@taxoffice.com";
 
    $message['to'] = $receiver_mail;
 
    $message['h:Reply-To'] = "no-reply@noreply.com";
 
    $message['subject'] = $subject;
 
    $message['html'] = $msg;
    
   if($filename != false)
    {
          $message['attachment'] = curl_file_create(realpath(APPPATH . '../assets/documents/'.$filename.''));
    }

    $ch = curl_init();
 
    curl_setopt($ch, CURLOPT_URL, $config['api_url']);
 
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
 
    curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 
    curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
 
    $result = curl_exec($ch);
    curl_close($ch);
    //return $result;
    $response = json_decode($result);
    $resp = array();
    if(isset($response->id)){
        $resp[0] = array('status'=>'sent');
        return $resp;
    }
    else{
        $resp[0] = array('status'=>'failed');
        return $resp;
    }
}
    
}       
/*require_once 'mandrill/src/Mandrill.php';
Class Send_Email
{


   
	public function send_mail($receiver_mail,$subject,$msg)
	{
		//$receiver_mail = "omotayoogunrinde@gmail.com";
		$sender_mail = "no_reply@reale.com";
		//$subject = "Meeting with landlord";
		//console.log($receiver_mail,$subject,$msg);
		
try {
    $mandrill = new Mandrill('fIfKuPDehJlK47p5HuQiHg');
    $message = array(
        'html' => $msg,
        //'text' => $msg,
        'subject' => $subject,
        'from_email' => $sender_mail,
        'from_name' => 'Reale Estate Agent',
        'to' => array(
            array(
                'email' => $receiver_mail,
                'name' => 'Reale Estate Agent',
                'type' => 'to'
            )
        ),
        'headers' => array('Reply-To' => 'message.reply@example.com'),
        'important' => false,
        'track_opens' => null,
        'track_clicks' => null,
        'auto_text' => null,
        'auto_html' => null,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => null,
        'view_content_link' => null,
        'bcc_address' => 'message.bcc_address@example.com',
        'tracking_domain' => null,
        'signing_domain' => null,
        'return_path_domain' => null,
        'merge' => true,
        'merge_language' => 'mailchimp',
        'global_merge_vars' => array(
            array(
                'name' => 'merge1',
                'content' => 'merge1 content'
            )
        ),
        'merge_vars' => array(
            array(
                'rcpt' => 'recipient.email@example.com',
                'vars' => array(
                    array(
                        'name' => 'merge2',
                        'content' => 'merge2 content'
                    )
                )
            )
        )
        
    );
    $async = false;
    $ip_pool = 'Main Pool';
    $send_at = 'example send_at';
    $result = $mandrill->messages->send($message, $async, $ip_pool);
    return $result;
    /*
    Array
    (
        [0] => Array
            (
                [email] => recipient.email@example.com
                [status] => sent
                [reject_reason] => hard-bounce
                [_id] => abc123abc123abc123abc123abc123
            )
    
    )
    
} catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }
	}
    public function send_report($receiver_mail,$subject,$msg,$type,$filename,$attach)
    {
        //$receiver_mail = "omotayoogunrinde@gmail.com";
        $sender_mail = "no_reply@reale.com";
        //$subject = "Meeting with landlord";
        //console.log($receiver_mail,$subject,$msg);
        
try {
    $mandrill = new Mandrill('fIfKuPDehJlK47p5HuQiHg');
    $message = array(
        'html' => $msg,
        //'text' => $msg,
        'subject' => $subject,
        'from_email' => $sender_mail,
        'from_name' => 'Reale Estate Agent',
        'to' => array(
            array(
                'email' => $receiver_mail,
                'name' => 'Reale Estate Agent',
                'type' => 'to'
            )
        ),
        'headers' => array('Reply-To' => 'message.reply@example.com'),
        'important' => false,
        'track_opens' => null,
        'track_clicks' => null,
        'auto_text' => null,
        'auto_html' => null,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => null,
        'view_content_link' => null,
        'bcc_address' => 'message.bcc_address@example.com',
        'tracking_domain' => null,
        'signing_domain' => null,
        'return_path_domain' => null,
        'merge' => true,
        'merge_language' => 'mailchimp',
        'global_merge_vars' => array(
            array(
                'name' => 'merge1',
                'content' => 'merge1 content'
            )
        ),
        'merge_vars' => array(
            array(
                'rcpt' => 'recipient.email@example.com',
                'vars' => array(
                    array(
                        'name' => 'merge2',
                        'content' => 'merge2 content'
                    )
                )
            )
        ),
         'attachments' => array(
            array(
                'type' => $type,
                'name' => $filename,
                'content' => $attach,
            ),
             
        )
        
    );
    $async = false;
    $ip_pool = 'Main Pool';
    $send_at = 'example send_at';
    $result = $mandrill->messages->send($message, $async, $ip_pool);
    return $result;
} catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }
    }

}*/
?>