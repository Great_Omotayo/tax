$(document).ready(function(){
  function isEmail(email) {
     var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  function randomNumber(){
     var randStr = "";
   for(var letter=1;letter<=12;letter++) {
    randStr+= letter%2==0 ? String.fromCharCode(Math.random()*(91-65)+65) : Math.ceil(Math.random()*9);
    }
    return randStr;
  }
origin_url = "http://localhost/tax";
//origin_url = "http://taxassurance23.epizy.com";
 $('.create_acc').click(function(e){
   e.preventDefault();
   var firstname = $('#firstname').val();
   var lastname = $('#lastname').val();
   var email =$('#email').val(); 
   var phone =$('#phone').val();
   var password = $('#password').val();
   var cpassword = $('#cpassword').val();
   if($('#password').val()!=$('#cpassword').val()){
  		swal("Oops!", "Password didn't Match", "error");
  		return 'False';
  	}
    if(firstname =='' || lastname =='' || email == '' || phone== '' || password=='' || cpassword==''){
      swal("Oops!", "All Fields are Required", "error");
      return false;
    }
     swal({title:"", text:"Creating Account....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
  	data = {firstname:firstname,lastname:lastname,email:email,phone:phone,password:password,cpassword:cpassword}
  	$.ajax({url:origin_url+"/index.php/welcome/create_fir_administrator", type:"POST", data:data,
         success:function(response) {
         var status = JSON.parse(response); 
         if(status==true){
             sweetAlert({
             title: 'Confirm',
             text: 'Account Created, Do you want to proceed to Log In',
             type: 'success',
             showCancelButton: true,
             confirmButtonText: 'Yes',
             cancelButtonText: 'No'
          },function(){
            window.location.href = origin_url+'/index.php/dashboard';
          });
           $('#firstname').val('');
           $('#lastname').val('');
           $('#email').val('');
           $('#password').val('');
           $('#cpassword').val('');
           $('#phone').val('');
         }
     }
 });
 });
  $('.log-company-admin').click(function(e){
     e.preventDefault();
     var email =$('#company-email').val(); 
     var password = $('#company-password').val();
     var tin_id = $('#tin_id').val();
     var error = '';
     if(email == ""){
       error += ' Email';
      }
      if(error != ''){
        error += ' and';
      }
      if(password ==""){
        error += ' Password'
      }
      if(error !=''){
        swal("Oops!", error+ ' Field Empty', "error");
        return false;
      }
          // alert("am");
      data = {email:email,password:password,tin_id:tin_id}
      $.ajax({url:origin_url+"/index.php/welcome/log_in_company_admin", type:"POST", data:data,
           success:function(response) {
           var status = JSON.parse(response); 
           if(status=="0"){
             swal("Oops!", "Unknown Email, Please Sign In with the Registered Email Address", "error");
           }
           else if(status=="1") {
            window.location.href = origin_url+'/index.php/dashboard';
           }
           else if(status=="2") {
            swal("Oops!", "Email and Password not matched", "error");
           }
       }
   });
 });
  $('.log-in-firs-team').click(function(e){
     e.preventDefault();
     var email =$('#firs-email').val(); 
     var password = $('#firs-password').val();
     var error = '';
     if(email == ""){
       error += 'Email';
      }
      if(error != ''){
        error += 'and';
      }
      if(password ==""){
        error += 'Password'
      }
      if(error !=''){
        swal("Oops!", error+ 'Field Empty', "error");
        return false;
      }
      data = {email:email,password:password}
      alert("am");
      $.ajax({url:origin_url+"/index.php/welcome/log_in_firs_team", type:"POST", data:data,
           success:function(response) {
           var status = JSON.parse(response); 
           if(status=="0"){
             swal("Oops!", "Unknown Email, Please Sign In with the Registered Email Address", "error");
           }
           else if(status=="1") {
            window.location.href = origin_url+'/index.php/dashboard';
           }
           else if(status=="2") {
            swal("Oops!", "Email and Password not matched", "error");
           }
       }
   });
 });

 $('#password').focusout(function(){
  if($('#password').val().length<8){
  	swal("Oops!", "Password Length is Weak(Minimum of 8 Character)", "error");
  }
 });
  $('#cpassword').focusout(function(){
  	if($('#password').val()!=$('#cpassword').val()){
  		swal("Oops!", "Password didn't Match", "error");
  	}
  });
  $('#show-log-in').click(function(){
    if($('.lg-in-for-company').hasClass('hide')){
    $('.lg-in-for-company').removeClass('hide');
     }
     if(!$('.sign-up-div').hasClass('hide')){
    $('.sign-up-div').addClass('hide');
     }
     if(!$('.lg-in-for-firs-team').hasClass('hide')){
      $('.lg-in-for-firs-team').addClass('hide');
     }
  });
  $('#show-sign-up').click(function(e){
   e.preventDefault();
     if($('.sign-up-div').hasClass('hide')){
    $('.sign-up-div').removeClass('hide');
     }
     if(!$('.lg-in-for-company').hasClass('hide')){
    $('.lg-in-for-company').addClass('hide');
     }
     if(!$('.lg-in-for-firs-team').hasClass('hide')){
      $('.lg-in-for-firs-team').addClass('hide');
     }
  });
  $('#show-log-in-firs').click(function(e){
    e.preventDefault();
    if($('.lg-in-for-firs-team').hasClass('hide')){
      $('.lg-in-for-firs-team').removeClass('hide');
     }
     if(!$('.lg-in-for-company').hasClass('hide')){
       $('.lg-in-for-company').addClass('hide');
     }
     if(!$('.sign-up-div').hasClass('hide')){
       $('.sign-up-div').addClass('hide');
     }
  });
  var copy;
  var flag=0;
  $("#open_file_modal").click(function(){
    //e.preventDefault();
        alert('am');
var month = ['January','February','March','April','May','June','July','August','Septemer','October','November','December'];
    $('#Updfiles').html('');
    $('#Updfiles').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
    $.ajax({url:origin_url+"/index.php/dashboard/get_uploaded_file", type:"POST", data:null,
        success:function(response){
          var status = JSON.parse(response);
          if(status.length>0){
            $('#Updfiles').html('');
            for(var c=0;c<status.length;c++){
              var stage = status[c].status=='new'?'Not Yet':status[c].status;
              if(status[c].created.split('-')[0]=='1' || status[c].created.split('-')[0]=='11' || status[c].created.split('-')[0]=='21' || status[c].created.split(';')[0]=='31'){
                var sup = "1st";
              }
              else if(status[c].created.split('-')[0]=='2' || status[c].created.split('-')[0]=='12' || status[c].created.split('-')[0]=='22'){
                var sup = "2nd";
              }
              else if(status[c].created.split('-')[0]=='3' || status[c].created.split('-')[0]=='13' || status[c].created.split('-')[0]=='23'){
                var sup = "3rd";
              }
              else{
                var sup ="th";
              }

              var updfile = "<div class='row collapse' style='margin-top:10px;'>"
                           +"<div class='small-2 medium-2 large-2 columns' id='no_of_files' style='text-align:center'>"+status[c].files.split(';').length+""
                           +"</div><div class='small-10 medium-10 large-10 columns' style='margin-top:-5px;'>"
                           +"<ul>"
                           +"<li>"+status[c].files.split(';').length+" files were Uploaded</li>"
                           +"<li style='font-size:1.0em;margin-top:-5px;color:grey'>Date Uploaded: "+status[c].created.split('-')[0]+"<sup>"+sup+"</sup> "+month[Math.floor(status[c].created.split('-')[1])-1]+" "+status[c].created.split('-')[2]+"</li>"
                           +"<li style='font-size:1.0em;margin-top:-3px;color:grey'>Fin Statement Period: "+status[c].period+"</li>"
                           +"<li style='font-size:0.8em;margin-top:-3px;color:grey;'>Assessment Status: "+stage+"</li>"
                           +"</ul>"
                           +"</div>"
                           +"</div>"
                           +"<div class='divide'></div>";
              $('#Updfiles').append(updfile);

            }
            copy = $("#Updfiles").clone();
            flag=1;
          }
          else{
            $('#Updfiles').html('');
            $('#Updfiles').append("<p style='margin-top:20px;margin-left:20px;font-size:20px;'>No File Uploaded Yet</p>");
          }

        }
        });
  });

  $('#uploadedfile').click(function(e){
   e.preventDefault();
   if(!$('#Newfiles').hasClass('hide')){
       $('#Newfiles').addClass('hide');
       $('#Updfiles').removeClass('hide');
       $('#uploadedF').addClass('uploaded_file');
       $('#uploadedF').removeClass('upload_new');
       $('#uploadN').removeClass('uploaded_file');
       $('#uploadN').addClass('upload_new');
   }
   $("#Updfiles").html('');
   $("#Updfiles").append(copy);
    console.log(copy);
   if(fin_begin.length ==0 &&fin_end.length ==0 && fin_year.length ==0 && no_of_files.length ==0)
    { if(flag==0){$('#Updfiles').append("<p style='margin-top:20px;margin-left:20px;font-size:20px;'>No File Uploaded Yet</p>");}
      return false;}
      var updfile;
      for(var x =0;x<fin_begin.length;x++){
          updfile = "<div class='row collapse' style='margin-top:10px;'>"
                           +"<div class='small-2 medium-2 large-2 columns' id='no_of_files' style='text-align:center'>"+no_of_files[x]+""
                           +"</div><div class='small-10 medium-10 large-10 columns' style='margin-top:-5px;'>"
                           +"<ul>"
                           +"<li>"+no_of_files[x]+" files were Uploaded</li>"
                           +"<li style='font-size:0.8em;margin-top:-5px;'>Date Uploaded: 15<sup>th</sup> January 2016</li>"
                           +"<li style='font-size:0.8em;margin-top:-3px;'>Financial Statement Duration: "+fin_begin[x]+" to "+fin_end[x]+" "+fin_year[x]+"</li>"
                           +"<li style='font-size:0.8em;margin-top:-3px;'>Assessment Status: Not Yet</li>"
                           +"</ul>"
                           +"</div>"
                           +"</div>"
                           +"<div class='divide'></div>";
              $('#Updfiles').prepend(updfile);  
       }
      
  });
    $('#uploadfile').click(function(e){
   e.preventDefault();
   if(!$('#Updfiles').hasClass('hide')){
       $('#Updfiles').addClass('hide');
       $('#Newfiles').removeClass('hide');
       $('#uploadN').removeClass('upload_new');
       $('#uploadN').addClass('uploaded_file');
       $('#uploadedF').removeClass('uploaded_file');
       $('#uploadedF').addClass('upload_new');
   }

  });
    $('.add_firs_team').click(function(e){
      e.preventDefault();
      //alert("am");
      var firs_team_name = $('#firs_team_name').val();
      var firs_team_phone = $('#firs_team_phone').val();
      var firs_team_email = $('#firs_team_email').val();
      var category = $('#category').val();
      var region = $('#region').val();
      var msg='';
      if(firs_team_name ==""){
        msg ='Name';
      }
       if(firs_team_phone ==""){
        msg +=' Phone';
      }
       if(firs_team_email ==""){
        msg +=' email';
      }
      if(category ==""){
        msg +=' Category';
      }
      if(region ==""){
        msg +=' Region';
      }
      if(msg !=""){
        swal("Oops!", msg+ ' Field Empty', "error");
        return false;
      }
      
      if(isEmail(firs_team_email) ==false){
        swal("Oops!", 'Invalid Email', "error");
        return false;
      }

      if(!($.isNumeric(firs_team_phone) && Math.floor(firs_team_phone) == firs_team_phone)){
        swal("Oops!!", 'Please Input Digit Only in Phone Number field', "error");
        return false;
      }
      swal({title:"", text:"Creating Account.....", imageUrl: origin_url+"/assets/imgs/loading01.gif",showConfirmButton:false,allowOutsideClick:false});
      data = {firs_team_name:firs_team_name,firs_team_phone:firs_team_phone,firs_team_email:firs_team_email,category:category,region:region}
      console.log(data);
      //return false;
      $.ajax({url:origin_url+"/index.php/welcome/add_firs_team", type:"POST", data:data,
         success:function(response){
          var status = JSON.parse(response);
          //alert(status);
          if(status ==true){
            sweetAlert({
             title: 'Confirm',
             text: 'Firs member added',
             type: 'success'
           });
            $('#firs_team_name').val('');
            $('#firs_team_phone').val('');
            $('#firs_team_email').val('');
            $('#category').val('');
            $('#region').val('');

          }
          else if(status=="exist"){
            swal("Oops!!", 'User Already Exist', "error");
          }
          else if(status=="error"){
            swal("Oops!!", 'Unknown Error,try again later', "error");
          }
         }
      })
    });
 $('#firs_team_phone').focusout(function(){
    var phone =  $('#firs_team_phone').val();
     if($.isNumeric(phone) && Math.floor(phone) == phone){}
      else{
        swal("Oops!!", 'Please Input Digit Only in Phone Number field', "error");
      }
 });
    $("#add_company").click(function(e){
     e.preventDefault();
     var msg ='';
     if($('#company_name').val()==""){
       msg += " Company Name";
     }
     if($('#company_add').val()==""){
      msg += " Company Address";
     }
     if($("#company_tin").val()==""){
      msg += " Company tin_id";
     }
     if($("#company_rc").val()==""){
      msg += " Company RC Number";
     }
     if(($("#company_tax_zone").val()=="")){
      msg +=" Company tax Zone";
     }
     if($("#company_admin").val()==""){
      msg +=" Company Administrator Name";
     }
     if($("#company_phone").val()==""){
      msg +=" Company Phone";
     }
     if($('#company_email').val()==""){
      msg +=" Company email";
     }
     if(msg !=""){
      swal("Oops!!", msg+ 'Field Empty', "error");
      return false;
     }
     if(isEmail($('#company_email').val())!=true){
       swal("Oops!!", 'Invalid Email', "error");
       return false;
     }
     swal({title:"", text:"Adding Company....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
     data={company_name:$('#company_name').val(),company_add:$("#company_add").val(),company_tin:$("#company_tin").val(),company_rc:$("#company_rc").val(),company_bvn:$("#company_bvn").val(),company_tax_zone:$("#company_tax_zone").val(),company_admin:$("#company_admin").val(),company_phone:$("#company_phone").val(),company_email:$("#company_email").val()}
     console.log(data);
     $.ajax({url:origin_url+"/index.php/welcome/add_company", type:"POST", data:data,
         success:function(response) {
         var status = JSON.parse(response);
         if(status =='sent'){
            sweetAlert({
             title: 'Confirm',
             text: 'Company added',
             type: 'success'
           }); 
          }
          else if(status == 'exist'){
           swal("Oops!!", 'Company Already Exist', "error");
          }
         console.log(status);
           $('#managing_companies').text(parseInt($('#managing_companies').text())+1);
           $('#company_name').val('');
           $("#company_add").val('');
           $("#company_tin").val('');
           $("#company_rc").val('');
           $("#company_tax_zone").val('');
          $("#company_bvn").val('');
          $("#company_admin").val('');
          $("#company_phone").val('');
          $("#company_email").val('');
         
       }
      });
    });
    
    $("#open_file").click(function(e){
      e.preventDefault();
      $('#collect_files').trigger('click');
    });
      function readURL3(input) {
        var ext = $('#collect_files').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['pdf','doc','docx','txt']) == -1)
        { sweetAlert('Please Select an Document'); return false;}
        if (input.files && input.files[0]) {
          $('.align_img').append('<li style="">'+$('#collect_files')[0].files[0].name+'</li>');
          //$('input[type=file]')[0].files[0].name
    }
   }
         var no_of_file=0;
         var numFiles;
         var file_data = [];
        $('#collect_files').change(function(){
         readURL3(this);
         numFiles =  this.files.length;
         file_data.push($('#collect_files')[0].files[0]);
        });
        var fin_begin=[];
        var fin_end=[];
        var fin_year=[];
        var no_of_files=[];
       $('#sendfile').click(function(e){
         e.preventDefault();
           if($('#begin').val()=="" || $('#end').val()=="" || $('#year').val()==""){
          swal("Oops!!", 'Please Input Financial Statement Period', "error");
          return false;
         }
         fin_begin.push($('#begin').val());
         fin_end.push($('#end').val());
         fin_year.push($('#year').val());
         no_of_files.push(file_data.length);
         if(file_data.length==0){
          swal("Oops!!", 'Please Upload File', "error");
          return false;
         }
         var form_data = new FormData();
         for(var t=0;t<file_data.length;t++){
         form_data.append('file[]',file_data[t]); 
         form_data.append('financial_statement',$('#begin').val()+';'+$('#end').val()+';'+$('#year').val());
         }
         swal({title:"", text:"Uploading Document....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
          $.ajax({
                url: ''+origin_url+'/index.php/dashboard/uploadfile', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(response){
                  var res = JSON.parse(response);
                  alert(res);
                    if(res==true){
                      sweetAlert({
                       title: 'Confirm',
                       text: 'File Uploaded',
                       type: 'success'
                     }); 
                    file_data = [];
                    $('.align_img').html('');
                    $('#begin').val('');
                    $('#end').val('');
                    $('#year').val('');
                  }
                }
                     
                    //$('.responsefromserver').html('<div style="text-align:center; background-color:purple;"><p>Report Sent</p></div>');
            });  
       });
       $(".open_list_campany").click(function(e){
        $('#render_company').html('');
        $('#render_company').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
        //return false;
        e.preventDefault();
           $.ajax({url:origin_url+"/index.php/dashboard/all_companies", type:"POST", data:null,
                success: function(response){
                  var res = JSON.parse(response);
                    if(res.length>0){
                      $('#render_company').html('');
                      for(var s=0;s<res.length;s++){
                      var company = "<div style='width:98%;margin-left:auto;margin-right:auto;color:grey'>"
                                  +"<div class='row collapse' style='margin-top:20px;'>"
                      +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;background-color:#6A5ACD' id='company_icon'>"+res[s]['Name'].charAt(0).toUpperCase()+"</div>"
                      +"<div class='small-10 medium-10 large-10 columns'>"
                      +"<ul>"
                      +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[s]['Name']+"</li>"
                      +"<li style='font-size:1.0em;margin-top:-9px;'>"+res[s]['Address']+"</li>"
                      +"<li style='font-size:1.0em;margin-top:-4px;'>"+res[s]['tax_zone']+"</li>"
                      +"</ul>"
                      +"</div></div>"
                      +"</div>" 
                      +"<div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div>";
                       $('#render_company').append(company);
                    }
                  }
                  else{
                    $('#render_company').html('');
                     $('#render_company').append('<p style="margin-top:12px;text-align:center">No Company added yet</p>')
                  }  
                      }
                     
                    //$('.responsefromserver').html('<div style="text-align:center; background-color:purple;"><p>Report Sent</p></div>');
            });  
       });
    $(".notifications").click(function(e){
       e.preventDefault();
        $('#render_notif').html('');
        $('#render_notif').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
        $.ajax({url:origin_url+"/index.php/dashboard/notifications", type:"POST", data:null,
          success:function(response){
            var res = JSON.parse(response);
            if(res.length>0){
              $('#render_notif').html('');
              var seen='';
              var not_seen="";
                for(var s=0;s<res.length;s++){
                  if(res[s]['category'] =='company'){       
                  var notif = "<div style='width:98%;margin-left:auto;margin-right:auto;'>"
                                  +"<div class='row collapse' style='margin-top:20px;color:grey'>"
                      +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;background-color:#6A5ACD' id='company_icon'>"+res[s]['from'].charAt(0).toUpperCase()+"</div>"
                      +"<div class='small-10 medium-10 large-10 columns'>"
                      +"<ul>"
                      +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[s]['from']+"</li>"
                      +"<li style='font-size:1.0em;margin-top:-9px;'>"+res[s]['type']+"</li>"
                      +"<li style='font-size:1.0em;margin-top:-3px;'>Financial Statement: " +res[s]['period']+"</li>"
                      +"</ul>"
                      +"</div></div>"
                      +"</div>" 
                      +"<div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div>";
                       if(res[s]['is_seen']=='0'){
                        not_seen += notif;
                       }
                       else if(res[s]['is_seen']=='1'){
                        seen += notif;
                       }
                       //$('.scroll-y').append(notif);

                }
                
                else{
                  var notif = "<div style='width:98%;margin-left:auto;margin-right:auto;'>"
                                  +"<div class='row collapse' style='margin-top:20px;color:grey'>"
                      +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;background-color:#6A5ACD' id='company_icon'>"+res[s]['category'].charAt(0).toUpperCase()+"</div>"
                      +"<div class='small-10 medium-10 large-10 columns'>"
                      +"<ul>"
                      +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[s]['from']+"</li>"
                      +"<li style='font-size:0.8em;margin-top:-9px;'>"+res[s]['type']+"</li>"
                      +"<li style='font-size:0.8em;margin-top:-3px;'>Role: "+res[s]['category']+"</li>"
                      +"</ul>"
                      +"</div></div>"
                      +"</div>" 
                      +"<div class='row'><div class='small-12 medium-12 large-12 columns divide'></div></div>";
                       if(res[s]['is_seen']=='0'){
                        not_seen += notif;
                       }
                       else if(res[s]['is_seen']=='1'){
                        seen += notif;
                       }
                }
                
              }
                  $('#render_notif').append(not_seen);
                  $('#render_notif').append(seen);
            }
             else{
                    $('#render_notif').html('');
                     $('#render_notif').append('<p style="margin-top:12px;text-align:center">No Notification yet</p>')
                  } 
          }
        });
    });
   var clone; 
   $("#Assess").click(function(e){
        e.preventDefault();
        $('.ongoingAssessment').html('');
        $('.ongoingAssessment').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
        $.ajax({url:origin_url+"/index.php/dashboard/all_companies", type:"POST", data:null,
          success:function(response){
            var res = JSON.parse(response);
            if(res.length>0){
              console.log(res);
              $('.ongoingAssessment').html('');
                for(var s=0;s<res.length;s++){
                  var company ="<a href='#' class='see_ongoing_assessment' id='ongoing"+s+"' company_id='"+res[s]['id']+"'><div class='row collapse' style='margin-top:10px;'>"
                              +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[s]['Name'].charAt(0)+"</div>"
                              +"<div class='small-10 medium-10 large-10 columns'>"
                              +"<ul>"
                              +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[s]['Name']+"</li>"
                              +"<li style='font-size:0.8em;margin-top:-9px;'>"+res[s]['Address']+"</li>"
                              +"<li style='font-size:0.8em;margin-top:-4px;'><div class='row collapse'><div class='small-6 medium-6 large-6 columns'>"
                              +"Tax Zone</div><div class='small-6 medium-6 large-6 columns text_on_ass' style='font-size:0.8em;'><strong>Stage: Analyzing</strong></div></div></li>"
                              +"</ul>"
                              +"</div></div></a><div class='divide'></div>";
                 $('.ongoingAssessment').append(company);             
                }
                clone = $('.ongoingAssessment').clone();
              }
            }
          });
   });
   var ongoingass_page=[];
   var finishedass_page = [];
   $(".company_assess").click(function(e){
        e.preventDefault();
        $("#msg_container").addClass('hide');
        $("#headerforchat").addClass('hide');
        $("#header_chat").addClass('hide');
        if($('.ass_tobbar').hasClass('hide')){$('.ass_tobbar').removeClass('hide');}
        $(".firongoingAssessment").html('');
        $('.firongoingAssessment').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
        $.ajax({url:origin_url+"/index.php/dashboard/fir_ongoingassessment", type:"POST", data:null,
            success:function(response){
             var res = JSON.parse(response);
             $(".firongoingAssessment").html('');
               if(res.length >0){
                 ongoingass_page = res.slice();
                 for(var r=0;r<res.length;r++){
                   var ongoing = "<a href='#' class='eachassessment' stage='"+res[r].stage+"' assess_id='"+res[r].assessment_id+"' id='eachassessment"+r+"' company_id='"+res[r]['id']+"'><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[r].Name.charAt(0)+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[r].Name+"</li>"
                                +"<li style='font-size:1.0em;margin-top:-9px;'>Period: "+res[r]['period'].split(';')[0]+" to "+res[r]['period'].split(';')[1]+" "+res[r]['period'].split(';')[2]+"</li>"
                +"<li style='font-size:1.0em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>"
                +"Tax Zone</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'><strong>Stage: "+res[r].stage+"</strong></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $(".firongoingAssessment").append(ongoing);

                 }
                 
               }
               else{
                  $(".firongoingAssessment").append("<p style='font-size:1.0em;margin-top:20px;text-align:center;'>No Ongoing Assessment</p>");
               }
            }
          });
    });
  $(document).on('click','#Ogoing-page',function(){
    $(".firongoingAssessment").html('');
    var res = [];
    res = ongoingass_page.slice();
    if(res.length>0){
    for(var r=0;r<res.length;r++){
     var ongoing = "<a href='#' class='eachassessment' stage='"+res[r].stage+"' assess_id='"+res[r].assessment_id+"' id='eachassessment"+r+"' company_id='"+res[r]['id']+"'><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[r].Name.charAt(0)+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[r].Name+"</li>"
                                +"<li style='font-size:0.8em;margin-top:-9px;'>Period: "+res[r]['period'].split(';')[0]+" to "+res[r]['period'].split(';')[1]+" "+res[r]['period'].split(';')[2]+"</li>"
                +"<li style='font-size:0.8em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>"
                +"Tax Zone</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'><strong>Stage: "+res[r].stage+"</strong></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $(".firongoingAssessment").append(ongoing);
                }
              }
         else{
           $(".firongoingAssessment").append("<p style='font-size:1.0em;margin-top:20px;text-align:center;'>No Ongoing Assessment</p>");
         }     
  });
  $(document).on('click','#finished-page',function(e){
        e.preventDefault();;
        if($('.ass_tobbar').hasClass('hide')){$('.ass_tobbar').removeClass('hide');}
        $(".firongoingAssessment").html('');
        $('.firongoingAssessment').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
        $.ajax({url:origin_url+"/index.php/dashboard/fir_finishedassessment", type:"POST", data:null,
            success:function(response){
             var res = JSON.parse(response);
             $(".firongoingAssessment").html('');
               if(res.length >0){
                 finishedass_page = res.slice();
                 for(var r=0;r<res.length;r++){
                   var finished = "<a href='#' class='eachassessment' stage='"+res[r].stage+"' assess_id='"+res[r].assessment_id+"' id='eachassessment"+r+"' company_id='"+res[r]['id']+"'><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[r].Name.charAt(0)+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[r].Name+"</li>"
                                +"<li style='font-size:0.8em;margin-top:-9px;'>Period: "+res[r]['period'].split(';')[0]+" to "+res[r]['period'].split(';')[1]+" "+res[r]['period'].split(';')[2]+"</li>"
                +"<li style='font-size:0.8em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>"
                +"Tax Zone</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'><strong>Stage: Finished</strong></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $(".firongoingAssessment").append(finished);

                 }
                 
               }
               else{
                  $(".firongoingAssessment").append("<p style='font-size:1.0em;margin-top:20px;text-align:center;'>No Ongoing Assessment</p>");
               }
            }
          });
  });
  var copyongoingassessment,copyflow,stage,fin_sta_asssessment_id;
  var see_only = '0';
  $(document).on('click','.eachassessment',function(){
   var id = $("#"+this.id+"").attr('company_id');
   var assess_id = $("#"+this.id+"").attr('assess_id');
   stage = $("#"+this.id+"").attr('stage');
   //alert(stage);
        $('.firongoingAssessment').css('height','450px');
     $('.firongoingAssessment').css('overflow-y', 'hidden');
   fin_sta_asssessment_id = assess_id;
   copyongoingassessment= $(".firongoingAssessment").clone();
   $(".firongoingAssessment").html('');
    //$(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtolist left'><i class='fa fa-chevron-left'> Back</i></a></div>");
   var flow = "<div id='assess_bar' style=''><p style='margin-top:5px;margin-left:20px;font-size:1.8em;color:grey;text-align:center;'>Assessment ID: "+assess_id+"<p></div>"
              +"<div class='flow_div' style=''><div class='assessment_flow file_uploaded' style=''>File Uploaded</div>"
              +"<div class='vertical-line file_uploaded-line' style='height:50px;'></div>"
              +"<a href='#' id='analyzing-page' class='assessmenthover' assessment_id='"+assess_id+"' company_id='"+id+"'><div class='assessment_flow analyzing' style=''>Analyzing</div></a>"
              +"<div class='vertical-line analyzing-line' style='height: 50px;' />"
              +"<a href='#' id='estimated_tax' class='assessmenthover get_company_estimate_tax'><div class='assessment_flow estimated_tax' style=''>Estimated Tax</div></a>"
              +"<div class='vertical-line estimated_tax-line' style='height: 50px;' />"
              +"<a href='#' id='approval' class='assessmenthover'><div class='assessment_flow approval' style=''>Approval</div></a>"
              +"<div class='vertical-line approval-line' style='height: 50px;' />"
              +"<a href='#' class='assessmenthover'><div class='assessment_flow invoice' style=''>Invoice</div></a></div>"
   $(".firongoingAssessment").append(flow);  
   if(stage=='file uploaded'){
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('border','2px solid red');
    $('.file_uploaded-line').css('background-color','#228B22');
   }  
   else if(stage =='analyzing'){ 
   $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('border','2px solid red');
   //$('.analyzing').css('border','2px solid red');
   }
   else if(stage =='estimated tax'){    
   $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('background-color','#228B22');   
   $('.analyzing').css('background-color','#228B22');
   $('.analyzing').css('color','white');
   $('.analyzing').css('border','2px solid #228B22');
   $('.estimated_tax').css('border','2px solid red');
   $('.analyzing-line').css('background-color','#228B22');
   }
   else if(stage =='approval'){
      $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('background-color','#228B22');   
   $('.analyzing').css('color','white');
   $('.analyzing').css('border','2px solid #228B22');
   $('.analyzing-line').css('background-color','#228B22');
   $('.estimated_tax').css('background-color','#228B22');   
   $('.estimated_tax').css('color','white');
   $('.estimated_tax').css('border','2px solid #228B22');
   $('.estimated_tax-line').css('background-color','#228B22');
   $('.approval').css('border','2px solid red');
   //$('.invoice').css('border','2px solid red');
   }
      else if(stage =='invoice'){
   $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('background-color','#228B22');   
   $('.analyzing').css('color','white');
   $('.analyzing').css('border','2px solid #228B22');
   $('.analyzing-line').css('background-color','#228B22');
   $('.estimated_tax').css('background-color','#228B22');   
   $('.estimated_tax').css('color','white');
   $('.estimated_tax').css('border','2px solid #228B22');
   $('.estimated_tax-line').css('background-color','#228B22');
   $('.approval').css('background-color','#228B22');   
   $('.approval').css('color','white');
   $('.approval').css('border','2px solid #228B22');
   $('.approval-line').css('border','2px solid #228B22');
   $('.invoice').css('border','2px solid red');
   //$('.invoice').css('border','2px solid red');
   }
   else{
    $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('background-color','#228B22');   
   $('.analyzing').css('color','white');
   $('.analyzing').css('border','2px solid #228B22');
   $('.analyzing-line').css('background-color','#228B22');
   $('.estimated_tax').css('background-color','#228B22');   
   $('.estimated_tax').css('color','white');
   $('.estimated_tax').css('border','2px solid #228B22');
   $('.estimated_tax-line').css('background-color','#228B22');
   $('.approval').css('background-color','#228B22');   
   $('.approval').css('color','white');
   $('.approval').css('border','2px solid #228B22');
   $('.approval-line').css('border','2px solid #228B22');
   $('.invoice').css('background-color','#228B22');   
   $('.invoice').css('color','white');
   $('.invoice').css('border','2px solid #228B22');
   }
   copyflow = flow;
  });
  $(document).on('click','#get_company_estimate_tax',function(){
   
  });
  $(document).on('click','#estimated_tax',function(){
    $(".flow_div").slideUp();
    alert(fin_sta_asssessment_id);
    //$('.ass_container').css('height','460px');
    $('.firongoingAssessment').css('height', '450px');
    $('.firongoingAssessment').css('overflow-y', 'hidden');
    //$('.firongoingAssessment').css('background-color','red');
    //$('.firongoingAssessment').css('overflow-y', 'scroll');
      $("#headerforchat").removeClass('hide');
    $("#header_chat").removeClass('hide');
    $('.ass_tobbar').addClass('hide');
    $('#assess_bar').addClass('hide');
    $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
    $('.firongoingAssessment').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
     $.ajax({url:origin_url+"/index.php/dashboard/get_extimated_tax_plus_msg", type:"POST",data:{assess_id:fin_sta_asssessment_id},
       success:function(response){
         var res = JSON.parse(response);
         alert(res);
         if(see_only =='0'){
         if(res.length>0){
          
             $(".firongoingAssessment").html('');
             $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
             $(".firongoingAssessment").append("<div class='row headerforchat' style='margin-top:5px;font-size:1.8em;color:grey;text-align:center;'><div class='small-12 medium-12 large-12 columns'>Estimated Tax</div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><input type='text' id='to_pay' value='"+res[0].amount+"' placeholder='input Estmated tax'/></div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><textarea placeholder='message' id='addmessage' rows='15'>"+res[0].msg+"</textarea></div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><a href='#' class='button success expand' id='tax_to_pay'>Save</a></div></div>");
         
         }
         else{
             $(".firongoingAssessment").html('');
             $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
             $("#header_chat").append("<div class='row headerforchat' style='margin-top:5px;font-size:1.8em;color:grey;text-align:center;'><div class='small-12 medium-12 large-12 columns'>Estimated Tax</div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><input type='text' id='to_pay'  placeholder='input Estmated tax'/></div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><textarea placeholder='message'  id='addmessage' rows='15'></textarea></div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><a href='#' class='button success expand' id='tax_to_pay'>Save</a></div></div>");
         }
       }
       else{
            if(res.length>0){
             $(".firongoingAssessment").html('');
             $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
             $("#header_chat").append("<div class='row headerforchat' style='margin-top:5px;font-size:1.8em;color:grey;text-align:center;'><div class='small-12 medium-12 large-12 columns'>Estimated Tax</div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><p>Estimated Tax: "+res[0].amount+"</p></div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><p style='font-size:1.0em'>NOTE:</p></div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'>"+res[0].msg+"</div></div>");
             }
             else{
              $(".firongoingAssessment").html('');
             $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
             $("#header_chat").append("<div class='row headerforchat' style='margin-top:5px;font-size:1.8em;color:grey;text-align:center;'><div class='small-12 medium-12 large-12 columns'>Estimated Tax</div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><p>Not in This Stage Yet</p></div></div>");
             }
       }
     }
      }); 
     
  }); 
  $(document).on('click','.approval',function(){
     $(".flow_div").slideUp();
    $('.ass_tobbar').addClass('hide');
    $('#assess_bar').addClass('hide');
    //alert(fin_sta_asssessment_id);
    $('.firongoingAssessment').css('overflow-y', 'hidden');
      $("#headerforchat").removeClass('hide');
    $("#header_chat").removeClass('hide');
    //$('.firongoingAssessment').css('height','300px;');
    $('.firongoingAssessment').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
     $.ajax({url:origin_url+"/index.php/dashboard/get_approval_msg", type:"POST",data:{assess_id:fin_sta_asssessment_id},
       success:function(response){
         var res = JSON.parse(response);
         if(see_only =='0'){
         if(res.length>0){

             $(".firongoingAssessment").html('');
             $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
             $('.firongoingAssessment').append("<div class='row headerforchat' style='margin-top:5px;font-size:1.8em;color:grey;text-align:center;'><div class='small-12 medium-12 large-12 columns'>Stage Approval</div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><textarea placeholder='Message to company' id='addmessage' rows='18'>"+res[0].msg+"</textarea></div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><a href='#' class='button success expand' id='tax_to_pay'>Save</a></div></div>");
         }
         else{
             $(".firongoingAssessment").html('');
             $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
             $("#header_chat").append("<div class='row headerforchat' style='margin-top:5px;font-size:1.8em;color:grey;text-align:center;'><div class='small-12 medium-12 large-12 columns'>Stage Approval</div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><textarea placeholder='Message to company'  id='addmessage' rows='18'></textarea></div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><a href='#' class='button success expand' id='tax_to_pay'>Save</a></div></div>");
         }
        }
        else{
            if(res.length>0){
             $(".firongoingAssessment").html('');
             $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
             $("#header_chat").append("<div class='row headerforchat' style='margin-top:5px;font-size:1.8em;color:grey;text-align:center;'><div class='small-12 medium-12 large-12 columns'>Approved</div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'>"+res[0].msg+"</div></div>");
             }
             else{
              $(".firongoingAssessment").html('');
             $(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtoflow left'><i class='fa fa-chevron-left'> Back</i></a></div>");
             $("#header_chat").append("<div class='row headerforchat' style='margin-top:5px;font-size:1.8em;color:grey;text-align:center;'><div class='small-12 medium-12 large-12 columns'>Approved</div></div>");
             $(".firongoingAssessment").append("<div class='row'><div class='small-12 medium-12 large-12 columns'><p>Not in This Stage Yet</p></div></div>");
             }
       }
       }
      }); 
  });
  $(document).on('focusout',"#to_pay",function(){
    var pay = $('#to_pay').val();
     if(!$.isNumeric(pay)){
      sweetAlert('Estimated Tax must be a number');
     }
  });
  var newamount;
  $(document).on('click','#tax_to_pay',function(){
     var amount = $('#to_pay').val();
     newamount = amount;
     var msg = $('#addmessage').val();
      if(!$.isNumeric(amount)){
      sweetAlert('Estimated Tax must be a number');
      return false;
     } 
      $.ajax({url:origin_url+"/index.php/dashboard/save_extimated_tax_plus_msg", type:"POST",data:{amount:amount,msg:msg,assess_id:fin_sta_asssessment_id},
       success:function(response){
         var res = JSON.parse(response);
         if(res==true){
           sweetAlert('Estimated Tax saved');
         }
         else{}
       }
      }); 

  });
  var rand;
  $(document).on('click','#analyzing-page',function(){

     $('.firongoingAssessment').css('height','360px');
     $('.firongoingAssessment').css('overflow-y', 'scroll');  
     $(".firongoingAssessment").animate({ scrollTop: $(".firongoingAssessment")[0].scrollHeight}, 1000); 
    $(".flow_div").slideUp();
    $("#header_chat").html('');
    $('.ass_tobbar').addClass('hide');
    $('#assess_bar').addClass('hide');
    $("#msg_container").removeClass('hide');
    var company_id = $("#analyzing-page").attr('company_id');
    var assess_id = $("#analyzing-page").attr('assessment_id');
    rand = randomNumber();
    $("#header_chat").append("<div class='row headerforchat' style='margin-top:5px;'><div class='small-4 medium-4 large-4 columns'><a href='#' class='backtoflow'><i class='fa fa-chevron-left'> Back</i></a></div><div class='small-4 medium-4 large-4 columns' style='color:grey;'>PROCESSING</div><div class='small-4 medium-4 large-4 columns'><a href='#' id='Selectfile'  assessment_id='"+assess_id+"' company_id='"+company_id+"'>Share Files <i class='fa fa-upload'></i></div></div>");

    $(".firongoingAssessment").append("<div id='msg'></div>");
    $("#msg_container").append('<div class="row" style="position:absolute;top:435px;"><div class="large-12 columns"><div class="row collapse"><div class="small-10 columns"><input type="text" class="text_to" id="text_to'+rand+'" placeholder="Your Message"></div><div class="small-2 columns"><a href="#" id="'+rand+'" assessment_id="'+assess_id+'" company_id="'+company_id+'" class="button postfix send_msg">Send</a></div></div></div></div>');
    //$("msg_container").css('position');
    $('#msg').append('<div class="row hide" id="loading" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
    $("#loading").removeClass('hide');
    $("#headerforchat").removeClass('hide');
    $("#header_chat").removeClass('hide');
     $.ajax({url:origin_url+"/index.php/dashboard/get_chats", type:"POST",data:{assess_id:assess_id},
       success:function(response){
        var res = JSON.parse(response);
       $("#loading").addClass('hide');
       if(res.length>0){
        console.log(res);
        for(var t=0;t<res.length;t++){
          if(res[t].me==res[t].sender){
                  if(res[t].type=='')
                  $(".firongoingAssessment").append("<div class='clearfix'><p class='left' style='background-color:#DCDCDC;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'>"+res[t].message+"</p><p class='right'></p></div>");
                  else if(res[t].type=='file')
                   $(".firongoingAssessment").append("<div class='clearfix'><a href='"+origin_url+"/assets/upload/"+res[t].message+"' download class='left' style='background-color:#DCDCDC;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'>Download "+res[t].message+"</a><a class='right'></a></div>"); 
                  else if(res[t].type=='image')
                  $(".firongoingAssessment").append("<div class='clearfix'><a href='"+origin_url+"/assets/upload/"+res[t].message+"' download><img class'left src='"+origin_url+"/assets/upload/"+res[t].message+"' id='' style='width:200px;height:200px;'/></a><a class='right'></a></div>");   
          }
          else {
                 if(res[t].type==''){
                  $(".firongoingAssessment").append("<div class='clearfix'><p class='left'></p><p class='right' style='background-color:#DCDCDC;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'>"+res[t].message+"</p></div>");
                }
                else if(res[t].type=='file'){
                   $(".firongoingAssessment").append("<div class='clearfix'><a class='left'></a><a class='right' href='"+origin_url+"/assets/upload/"+res[t].message+"' download class='left' style='background-color:#DCDCDC;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'>Download "+res[t].message+"</a></div>");  
                }
                else if(res[t].type=='image'){
                  $(".firongoingAssessment").append("<div class='clearfix'><a class='left'></a><a class='right'> <img class'left src='"+origin_url+"/assets/upload/"+res[t].message+"' id='' style='width:200px;height:200px;'/></a></div>");   
                }
          }
        }
        $(".firongoingAssessment").animate({ scrollTop: $(".firongoingAssessment")[0].scrollHeight}, 1000);
       }
         
       }
     });
    
  });
  $(document).on('click','.backtolist',function(){
     $(".firongoingAssessment").html('');
     $('.ass_tobbar').removeClass('hide');
    var res = [];
    res = ongoingass_page.slice();
    if(res.length>0){
    for(var r=0;r<res.length;r++){
     var ongoing = "<a href='#' class='eachassessment' stage='"+res[r].stage+"' assess_id='"+res[r].assessment_id+"' id='eachassessment"+r+"' company_id='"+res[r]['id']+"'><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[r].Name.charAt(0)+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[r].Name+"</li>"
                                +"<li style='font-size:0.8em;margin-top:-9px;'>Period: "+res[r]['period'].split(';')[0]+" to "+res[r]['period'].split(';')[1]+" "+res[r]['period'].split(';')[2]+"</li>"
                +"<li style='font-size:0.8em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>"
                +"Tax Zone</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'><strong>Stage: "+res[r].stage+"</strong></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $(".firongoingAssessment").append(ongoing);
                }
              }
         else{
           $(".firongoingAssessment").append("<p style='font-size:1.0em;margin-top:20px;text-align:center;'>No Ongoing Assessment</p>");
         }     
  });
  $(document).on('click','.backtoflow',function(){
    $('.firongoingAssessment').css('height','450px');
    //$('.ass_container').css('height','470px');
    $('.firongoingAssessment').css('overflow-y', 'none');
    $(".firongoingAssessment").html('');
    $('.ass_tobbar').removeClass('hide');
    //$(".firongoingAssessment").append("<div class='clearfix'><a href='#' class='backtolist left'><i class='fa fa-chevron-left'> Back</i></a></div>");
    $('#header_chat').html('');
    $("#msg_container").addClass('hide');
    //$('.firongoingAssessment').css('height','450px');
     //$('.firongoingAssessment').css('overflow-y', 'scroll');
     //$(".firongoingAssessment").slideDown();
    $(".firongoingAssessment").html(''); 
    $(".firongoingAssessment").append(copyflow);
    $(".estimated_tax").text(newamount);
       if(stage=='file uploaded'){
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('border','2px solid red');
    $('.file_uploaded-line').css('background-color','#228B22');
   }  
   else if(stage =='analyzing'){ 
   $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('border','2px solid red');
   //$('.analyzing').css('border','2px solid red');
   }
   else if(stage =='estimated tax'){   
   $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('background-color','#228B22');   
   $('.analyzing').css('background-color','#228B22');
   $('.analyzing').css('color','white');
   $('.analyzing').css('border','2px solid #228B22');
   $('.estimated_tax').css('border','2px solid red');
   $('.analyzing-line').css('background-color','#228B22');
   }
   else if(stage =='approval'){
      $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('background-color','#228B22');   
   $('.analyzing').css('color','white');
   $('.analyzing').css('border','2px solid #228B22');
   $('.analyzing-line').css('background-color','#228B22');
   $('.estimated_tax').css('background-color','#228B22');   
   $('.estimated_tax').css('color','white');
   $('.estimated_tax').css('border','2px solid #228B22');
   $('.estimated_tax-line').css('background-color','#228B22');
   $('.approval').css('border','2px solid red');
   //$('.invoice').css('border','2px solid red');
   }
      else if(stage =='invoice'){
   $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('background-color','#228B22');   
   $('.analyzing').css('color','white');
   $('.analyzing').css('border','2px solid #228B22');
   $('.analyzing-line').css('background-color','#228B22');
   $('.estimated_tax').css('background-color','#228B22');   
   $('.estimated_tax').css('color','white');
   $('.estimated_tax').css('border','2px solid #228B22');
   $('.estimated_tax-line').css('background-color','#228B22');
   $('.approval').css('background-color','#228B22');   
   $('.approval').css('color','white');
   $('.approval').css('border','2px solid #228B22');
    $('.approval-line').css('background-color','#228B22');
   $('.invoice').css('border','2px solid red');
   //$('.invoice').css('border','2px solid red');
   }
    else{
    $('.file_uploaded-line').css('background-color','#228B22');      
   $('.file_uploaded').css('background-color','#228B22');
   $('.file_uploaded').css('color','white');
   $('.file_uploaded').css('border','2px solid #228B22');
   $('.analyzing').css('background-color','#228B22');   
   $('.analyzing').css('color','white');
   $('.analyzing').css('border','2px solid #228B22');
   $('.analyzing-line').css('background-color','#228B22');
   $('.estimated_tax').css('background-color','#228B22');   
   $('.estimated_tax').css('color','white');
   $('.estimated_tax').css('border','2px solid #228B22');
   $('.estimated_tax-line').css('background-color','#228B22');
   $('.approval').css('background-color','#228B22');   
   $('.approval').css('color','white');
   $('.approval').css('border','2px solid #228B22');
   $('.approval-line').css('border','2px solid #228B22');
   $('.invoice').css('background-color','#228B22');   
   $('.invoice').css('color','white');
   $('.invoice').css('border','2px solid #228B22');
   }
   //alert(stage);
  });
    var comp_id,ass_id; 
   $(document).on("click","#Selectfile",function(e){
      e.preventDefault();
      $('#sharefile').trigger('click');
      comp_id = $("#Selectfile").attr('company_id');
      ass_id = $("#Selectfile").attr('assessment_id');
    });
   $(document).on('change','#sharefile',function(){
     var sharefile=[];
     sharefile.push($('#sharefile')[0].files[0]);
     alert(sharefile);
     var ext = $('#sharefile').val().split('.').pop().toLowerCase();
     if($.inArray(ext, ['png','jpeg','jpg','gif'])!=-1){
      $(".firongoingAssessment").append("<div class='clearfix'><img src='#' id='blah' style='width:200px;height:200px;'/></div>");
       $(".firongoingAssessment").animate({ scrollTop: $(".firongoingAssessment")[0].scrollHeight}, 1000);
      if ($('#sharefile')[0].files && $('#sharefile')[0].files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL($('#sharefile')[0].files[0]);
    }
     }
     else{
      $(".firongoingAssessment").append("<div class='clearfix'><p class='left' style='background-color:#DCDCDC;padding:5px 5px 5px 5px;border-radius:5pxmargin-top:5px;'>"+$('#sharefile')[0].files[0].name+" is Upoaded</p></div>");
        $(".firongoingAssessment").animate({ scrollTop: $(".firongoingAssessment")[0].scrollHeight}, 1000);
     }
     form_data = new FormData();
     form_data.append('assessment_id',ass_id);
     form_data.append('company_id',comp_id);
     form_data.append('file[]',$('#sharefile')[0].files[0]);
      $.ajax({
                url: ''+origin_url+'/index.php/dashboard/uploadenquiryfile', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(response){
                  var res = JSON.parse(response);
                  alert(res);
                }
              });
        
   });
  $(document).on('click','.send_msg',function(){
    if($('#text_to').val()!=""){
      //$(".firongoingAssessment").append("<div class='row' style='margin-top:5px;'><div class='small-12 medium-12 large-12 columns'><p >"+$('#text_to').val()+"</p></div></div>");
      $(".firongoingAssessment").append("<div class='clearfix'><p class='left' style='background-color:#DCDCDC;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'>"+$('#text_to'+this.id+'').val()+"</p><p class='right'></p></div>");
      var text = $('#text_to'+this.id+'').val();
      }
      else {return false;}
       $(".firongoingAssessment").animate({ scrollTop: $(".firongoingAssessment")[0].scrollHeight}, 1000);
      var company_id = $('#'+this.id+'').attr('company_id');
      var assessment_id = $('#'+this.id+'').attr('assessment_id');
      //alert(assessment_id);
      //return false;
      $('#text_to'+this.id+'').val('');
      $.ajax({url:origin_url+"/index.php/dashboard/chats", type:"POST",data:{text:text,company_id:company_id,assessment_id:assessment_id},
       success:function(response){
        var res = JSON.parse(response);

         
       }
     });
  });
  $(document).on('click','.see_ongoing_assessment',function(e){
    e.preventDefault();
    $('.ongoingAssessment').html('');
    var company_id = $('#'+this.id+'').attr('company_id');
    $('.ongoingAssessment').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
    //$data = {'comp'}
     $.ajax({url:origin_url+"/index.php/dashboard/ongoing_assessment", type:"POST", data:data,
          success:function(response){
            var res = JSON.parse(response);
            if(res.length>0){
              $('.ongoingAssessment').html('');
              console.log(res);
              $('.ongoingAssessment').html('');
                for(var s=0;s<res.length;s++){
                }
              }
              else{

              }
            }
          });
  }); 
  $(document).on('click','#all_files',function(e){

              $.ajax({url:origin_url+"/index.php/dashboard/all_files", type:"POST", data:null,
        success:function(response){
            var res = JSON.parse(response);
              if(res.length > 0){ 
                $('#render_files').html('');
                for(s=0;s<res.length;s++){
                  
                  var files ="<a href='#' class='' id='' company_id=''><div class='row collapse' style='margin-top:10px;'>"
                              +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;background-color:#6A5ACD' id='company_icon'>"+res[s]['Name'].charAt(0)+"</div>"
                              +"<div class='small-10 medium-10 large-10 columns'>"
                              +"<ul>"
                              +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[s]['Name']+"</li>"
                              +"<li style='font-size:1.0em;margin-top:-9px;'>"+res[s]['Address']+"</li>"
                              +"<li style='font-size:1.0em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>"
                              +"Tax Zone</div><div class='small-4 medium-4 large-4 columns text_on_team show_file_to_download' num='"+s+"' style='font-size:0.8em;text-align:center;'><strong>Download</strong></div></div></li>"
                              +"</ul>"
                              +"</div></div></a><div id='eachfile"+s+"' class='hide'></div>"
                              +"<div class='' style='border:1px dashed black;'></div>";
                  $('#render_files').append(files); 
                    var nfile = res[s]['files'].split(';');
                  for(q=0;q<nfile.length;q++){
                    $("#eachfile"+s+"").append("<div class='row' style='font-size:1.0em;text-align:center;'><div class='small-6 medium-6 large-6 columns'><a class='downloadfile' assessment_id='"+res[s]['assessment_id']+"' id='downloadfile"+s+"' href='http://localhost/tax/assets/upload/"+nfile[q]+"' download>File "+(q+1)+"&nbsp;&nbsp;&nbsp;<i class='fa fa-download' style='color:#6495ED'></i></a></div><div class='small-6 medium-6 large-6 columns'></div></div>");
                  }        
                }
              }
              else{

              }
      }
      }); 
  });
  $(document).on('click','.show_file_to_download',function(){
    var value = $(this).attr('num');
    $('#eachfile'+value+'').removeClass('hide');
  }); 
  $(document).on('click','.downloadfile',function(){
    var assess_id = $('#'+this.id+'').attr('assessment_id');
    $.ajax({url:origin_url+"/index.php/dashboard/start_assessment", type:"POST", data:{assess_id:assess_id},
        success:function(response){
          var res = JSON.parse(response);
        }
      });
  })
  $(document).on('click','#company_ongoing_ass,#assessment_for_company',function(){
        //e.preventDefault();
        see_only = '1';
        $("#msg_container").addClass('hide');
        $("#headerforchat").addClass('hide');
        $("#header_chat").addClass('hide');
        if($('.ass_tobbar').hasClass('hide')){$('.ass_tobbar').removeClass('hide');}
        $(".firongoingAssessment").html('');
        $('.firongoingAssessment').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
        $.ajax({url:origin_url+"/index.php/dashboard/company_ongoingassessment", type:"POST", data:null,
            success:function(response){
             var res = JSON.parse(response);
             $(".firongoingAssessment").html('');
               if(res.length >0){
                 company_ongoingass_page = res.slice();
                 for(var r=0;r<res.length;r++){
                   var ongoing = "<a href='#' class='eachassessment' stage='"+res[r].stage+"' assess_id='"+res[r].assessment_id+"' id='eachassessment"+r+"' company_id='"+res[r]['id']+"'><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[r].Name.charAt(0)+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[r].Name+"</li>"
                                +"<li style='font-size:0.8em;margin-top:-9px;'>Period: "+res[r]['period'].split(';')[0]+" to "+res[r]['period'].split(';')[1]+" "+res[r]['period'].split(';')[2]+"</li>"
                +"<li style='font-size:0.8em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>"
                +"Tax Zone</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'><strong>Stage: "+res[r].stage+"</strong></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $(".firongoingAssessment").append(ongoing);

                 }
                 
               }
               else{
                  $(".firongoingAssessment").append("<p style='font-size:1.0em;margin-top:20px;text-align:center;'>No Ongoing Assessment</p>");
               }
            }
          });
  });
  $(document).on('click','#company_finished_ass',function(){
     //e.preventDefault();
        if($('.ass_tobbar').hasClass('hide')){$('.ass_tobbar').removeClass('hide');}
        $(".firongoingAssessment").html('');
        $('.firongoingAssessment').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
        $.ajax({url:origin_url+"/index.php/dashboard/company_finishedassessment", type:"POST", data:null,
            success:function(response){
             var res = JSON.parse(response);
             $(".firongoingAssessment").html('');
               if(res.length >0){
                 finishedass_page = res.slice();
                 for(var r=0;r<res.length;r++){
                   var finished = "<a href='#' class='eachassessment' stage='"+res[r].stage+"' assess_id='"+res[r].assessment_id+"' id='eachassessment"+r+"' company_id='"+res[r]['id']+"'><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[r].Name.charAt(0)+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[r].Name+"</li>"
                                +"<li style='font-size:0.8em;margin-top:-9px;'>Period: "+res[r]['period'].split(';')[0]+" to "+res[r]['period'].split(';')[1]+" "+res[r]['period'].split(';')[2]+"</li>"
                +"<li style='font-size:0.8em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>"
                +"Tax Zone</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'><strong>Stage: Finished</strong></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $(".firongoingAssessment").append(finished);

                 }
                 
               }
               else{
                  $(".firongoingAssessment").append("<p style='font-size:1.0em;margin-top:20px;text-align:center;'>No Ongoing Assessment</p>");
               }
            }
          });

  });
  var copyteam;
  $(document).on('click','#fir_team_member',function(e){
   //e.preventDefault();
   $("#render_team").html('');
   $('#render_team').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
    $.ajax({url:origin_url+"/index.php/dashboard/get_firs_team", type:"POST", data:null,
            success:function(response){
            var res = JSON.parse(response);
            $("#render_team").html('');
            if(res.length >0){
               copyteam = res.slice();
               for(var r=0;r<res.length;r++){
                   var team = "<a href='#' class='assign_task' id='"+randomNumber()+"' region='"+res[r].region+"' auditor_id ='"+res[r].fir_member_id+"'><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[r].category.charAt(0).toUpperCase()+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[r].firstname+" "+res[r].lastname+"</li>"
                                +"<li style='font-size:1.0em;margin-top:-9px;'>Email: "+res[r].email+"</li>"
                +"<li style='font-size:1.0em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>Tax Zone: " +res[r].region+""
                +"</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $("#render_team").append(team);

                 }
            }
            else{
               $("#render_team").append("<p style='font-size:1.0em;margin-top:20px;text-align:center;'>No team Member Added yet</p>");
            }
       }  
       });     
  });
  var auditor_id;
  var fir_team_member_region ;
  $(document).on('click','.assign_task, #assigntask',function(){
    if(this.id=='assigntask'){
      auditor_id = auditor_id
      fir_team_member_region= fir_team_member_region;
      //alert(fir_team_member_region);
    }
    else{
     fir_team_member_region = $('#'+this.id+'').attr('region');
     auditor_id = $('#'+this.id+'').attr('auditor_id');
     }
    $("#render_team").html('');
    $("#render_team").append("<div class='clearfix' style='margin-top:0px;'><a href='#' class='backtofir_team left'><i class='fa fa-chevron-left'> Back</i></a></div>");
     $("#render_team").append('<div class="row"><div class="small-6 medium-6 large-6 columns assign_task_page"><a href="#" id="assigntask" style="color:#FFFFFF;">Assign Task</a></div><div class="small-6 medium-6 large-6 columns assigned_task_page"><a href="#" style="color:#FFFFFF;" id="assignedtask">Assigned Task</a></div></div>');
     var clone = $('#taskpage').clone();
     clone.removeClass('hide');
     $("#render_team").append(clone);//.find('toremove').removeClass('hide');                                       
      $.ajax({url:origin_url+"/index.php/dashboard/all_companies", type:"POST", data:null,
            success:function(response){              
              var res = JSON.parse(response);
              console.log(res);
              if(res.length >0){
               $("company_to_audit").html('');
              var company = document.getElementById("company_to_audit"); 
              var createOption = document.createElement("option");
              createOption.textContent = "Select Company";
              createOption.value = ""; 
              company.appendChild(createOption);    
              for(var t=0;t<res.length;t++)
              {
                  
                  if(res[t].tax_zone == fir_team_member_region){
                    createOption = document.createElement("option");
                    createOption.textContent = res[t].Name;
                    createOption.value = res[t].id; 
                    company.appendChild(createOption);
                  }
              }
            } 
            else{
              $("#render_team").html('');
                $("#render_team").append("<div class='clearfix' style='margin-top:0px;'><a href='#' class='backtofir_team left'><i class='fa fa-chevron-left'> Back</i></a></div>");
                $("#render_team").append('<p>No Assigned Task yet<p>');
            }
          } 
        });  
         select_day();
         select_month();
  });
    $(document).on('click','#assignedtask',function(){
       $("#render_team").html('');
        $('#render_team').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
        $("#render_team").append("<div class='clearfix' style='margin-top:0px;'><a href='#' class='backtofir_team left'><i class='fa fa-chevron-left'> Back</i></a></div>");
        $("#render_team").append('<div class="row"><div class="small-6 medium-6 large-6 columns assign_task_page"><a href="#" id="assigntask" style="color:#FFFFFF;">Assign Task</a></div><div class="small-6 medium-6 large-6 columns assigned_task_page"><a href="#" style="color:#FFFFFF;" id="assignedtask">Assigned Task</a></div></div>');
        //swal({title:"", text:"Loading Assigned Task....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
          $('#taskpage').addClass('hide');
          $.ajax({url:origin_url+"/index.php/dashboard/get_assigned_task", type:"POST", data:{auditor_id:auditor_id},
            success:function(response){              
              var status = JSON.parse(response);
              if(status.length >0){
                $("#render_team").html('');
                $("#render_team").append('<div class="row"><div class="small-6 medium-6 large-6 columns assign_task_page"><a href="#" id="assigntask" style="color:#FFFFFF;">Assign Task</a></div><div class="small-6 medium-6 large-6 columns assigned_task_page"><a href="#" style="color:#FFFFFF;" id="assignedtask">Assigned Task</a></div></div>');
                $('.assign_task_page').css('background-color','#D2B48C');
                $('.assigned_task_page').css('background-color','#d36f2e');
                for(var r=0;r<status.length;r++){
                  if(status[r].created.split('-')[0]=='1' || status[r].created.split('-')[0]=='11' || status[r].created.split('-')[0]=='21' || status[r].created.split(';')[0]=='31'){
                var sup = "1st";
              }
              else if(status[r].created.split('-')[0]=='2' || status[r].created.split('-')[0]=='12' || status[r].created.split('-')[0]=='22'){
                var sup = "nd";
              }
              else if(status[r].created.split('-')[0]=='3' || status[r].created.split('-')[0]=='13' || status[r].created.split('-')[0]=='23'){
                var sup = "rd";
              }
              else{
                var sup ="th";
              }
                var assigned = "<a href='#' class='' region='' auditor_id =''><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+status[r].task.charAt(0).toUpperCase()+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+status[r].task+" "+status[r].task_on_id+"</li>"
                                +"<li style='font-size:1.0em;margin-top:-9px;'>Financial Statement Period: "+status[r].financial_statement_period+"</li>"
                +"<li style='font-size:0.8em;margin-top:-4px;'><div class='row collapse'><div class='small-12 medium-12 large-12 columns'>Audit Time: " +status[r].audit_time.split(';')[0]+"<sup>"+sup+"</sup> " +status[r].audit_time.split(';')[1]+" to " +status[r].audit_time.split(';')[2]+"<sup>"+sup+"</sup> " +status[r].audit_time.split(';')[3]+" "+status[r].audit_time.split(';')[4]+""
                +"</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $("#render_team").append(assigned);
                }
              }
              else{
                $("#render_team").html('');
                $("#render_team").append("<div class='clearfix' style='margin-top:0px;'><a href='#' class='backtofir_team left'><i class='fa fa-chevron-left'> Back</i></a></div>");
                $("#render_team").append('<p>No Assigned Task yet<p>');
              }
            }
          });

    })
    $(document).on('click','.backtofir_team',function(){
      $('#taskpage').addClass('hide');
        res = copyteam.slice();
               $("#render_team").html('');
               for(var r=0;r<res.length;r++){
                   var team = "<a href='#' class='assign_task' id='"+randomNumber()+"' region='"+res[r].region+"' auditor_id ='"+res[r].fir_member_id+"'><div class='row collapse' style='margin-top:10px;'>"
                                +"<div class='small-2 medium-2 large-2 columns' style='text-align:center;' id='company_icon'>"+res[r].category.charAt(0).toUpperCase()+"</div>"
                                +"<div class='small-10 medium-10 large-10 columns'>"
                                +"<ul>"
                                +"<li style='font-size:1.5em;margin-top:-9px;'>"+res[r].firstname+" "+res[r].lastname+"</li>"
                                +"<li style='font-size:1.0em;margin-top:-9px;'>Email: "+res[r].email+"</li>"
                +"<li style='font-size:1.0em;margin-top:-4px;'><div class='row collapse'><div class='small-8 medium-8 large-8 columns'>Tax Zone: " +res[r].region+""
                +"</div><div class='small-4 medium-4 large-4 columns text_on_team' style='font-size:0.8em;text-align:center;'></div></div></li>"
                              +"</ul>"
                              +"</div></div></a>"
                              +"<div class='divide'></div>";
                  $("#render_team").append(team);

                 }
    });
    function select_day(){
            $('#day_from').html('');
            $('#day_to').html('');
            var from = document.getElementById("day_from"); 
            var to = document.getElementById("day_to");
                var createOption = document.createElement("option");
                var createOption2 = document.createElement("option");
                createOption.textContent = "Day";
                createOption2.textContent = "Day";
                createOption.value = ""; 
                createOption2.value = "";
                from.appendChild(createOption);
                to.appendChild(createOption2);
           for(var t=1;t<=31;t++)
              {
                    createOption = document.createElement("option");
                    createOption2 = document.createElement("option");
                    createOption.textContent = t;
                    createOption2.textContent = t;
                    createOption.value = t;
                    createOption2.value = t;
                    from.appendChild(createOption);
                    to.appendChild(createOption2);
              }       
    } 
      function select_month(){
            $('#from_month').html('');
            $('#month_to').html('');
            var month = ['January','February','March','April','May','June','July','August','Septemer','October','November','December'];
            var from = document.getElementById("from_month"); 
            var to = document.getElementById("month_to");
                var createOption = document.createElement("option");
                var createOption2 = document.createElement("option");
                createOption.textContent = "Month";
                createOption2.textContent = "Month";
                createOption.value = ""; 
                createOption2.value = "";
                from.appendChild(createOption);
                to.appendChild(createOption2);
           for(var t=0;t<12;t++)
              {
                    createOption = document.createElement("option");
                    createOption2 = document.createElement("option");
                    createOption.textContent = month[t];
                    createOption2.textContent = month[t];
                    createOption.value = month[t];
                    createOption2.value = month[t];
                    from.appendChild(createOption);
                    to.appendChild(createOption2);
              }       
    }
  $(document).on('click','#task_to_do',function(){

    var auditor_key = auditor_id;
    var company_id = $('#company_to_audit').val();
    var task_type = $('#task_type').val();
    var begin = $('#begin').val();
    var end = $('#end').val();
    var year = $('#year').val();
    var from_day = $("#day_from").val();
    var from_month =$('#from_month').val();
    var day_to = $('#day_to').val();
    var month_to = $('#month_to').val();
    var desc = 'financial statement';
    var audit_yr = $('#audit_yr').val();
    var audit_time = from_day+';'+from_month+";"+day_to+";"+month_to+";"+audit_yr;  
    if(company_id =="" || task_type=="" || begin=="" || end=="" || year =="" || from_day=="" || from_month=="" || day_to=="" || month_to =="" || desc==""){
      swal("Oops!!", 'All input fields are compulsory', "error");
      return false;
    }
    swal({title:"", text:"Assigning Task....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
    $.ajax({url:origin_url+"/index.php/dashboard/assign_task", type:"POST", data:{auditor_id:auditor_id,task:task_type,company_id:company_id,begin:begin,end:end,year:year,desc:desc,audit_time:audit_time},
            success:function(response){              
              var res = JSON.parse(response);
                sweetAlert({
                       title: 'Confirm',
                       text: 'Task Assigned',
                       type: 'success'
                     });
            }
           }); 
  });
});