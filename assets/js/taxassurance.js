$(document).ready(function(){
  /*CKEDITOR.replace('editor1');
  CKEDITOR.config.height='400px';*/
  //origin_url = "http://localhost/tax";
  origin_url = "http://taxassurance23.epizy.com";
  alert($(window).width());
  function randomNumber(){
    var num = Math.floor(Math.random() * 90000) + 10000;
    //alert(num);
    return false;
  }
  function select_day(){
            $('#day_from').html('');
            $('#day_to').html('');
            var from = document.getElementById("day_from"); 
            var to = document.getElementById("day_to");
                var createOption = document.createElement("option");
                var createOption2 = document.createElement("option");
                createOption.textContent = "Day";
                createOption2.textContent = "Day";
                createOption.value = ""; 
                createOption2.value = "";
                from.appendChild(createOption);
                to.appendChild(createOption2);
           for(var t=1;t<=31;t++)
              {
                    createOption = document.createElement("option");
                    createOption2 = document.createElement("option");
                    createOption.textContent = t;
                    createOption2.textContent = t;
                    createOption.value = t;
                    createOption2.value = t;
                    from.appendChild(createOption);
                    to.appendChild(createOption2);
              }       
    } 
      function select_month(){
            $('#from_month').html('');
            $('#month_to').html('');
            var month = ['January','February','March','April','May','June','July','August','Septemer','October','November','December'];
            var from = document.getElementById("from_month"); 
            var to = document.getElementById("month_to");
                var createOption = document.createElement("option");
                var createOption2 = document.createElement("option");
                createOption.textContent = "Month";
                createOption2.textContent = "Month";
                createOption.value = ""; 
                createOption2.value = "";
                from.appendChild(createOption);
                to.appendChild(createOption2);
           for(var t=0;t<12;t++)
              {
                    createOption = document.createElement("option");
                    createOption2 = document.createElement("option");
                    createOption.textContent = month[t];
                    createOption2.textContent = month[t];
                    createOption.value = month[t];
                    createOption2.value = month[t];
                    from.appendChild(createOption);
                    to.appendChild(createOption2);
              }       
    }

  function isEmail(email) {
     var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  $("#add_company").click(function(e){
     e.preventDefault();
     var msg ='';
     if($('#company_name').val()==""){
       msg += " Company Name";
     }
     if($('#company_add').val()==""){
      msg += " Company Address";
     }
     if($("#company_tin").val()==""){
      msg += " Company tin_id";
     }
     if($("#company_rc").val()==""){
      msg += " Company RC Number";
     }
     if(($("#company_tax_zone").val()=="")){
      msg +=" Company tax Zone";
     }
     if($("#company_admin").val()==""){
      msg +=" Company Administrator Name";
     }
     if($("#company_phone").val()==""){
      msg +=" Company Phone";
     }
     if($('#company_email').val()==""){
      msg +=" Company email";
     }
     if(msg !=""){
      swal("Oops!!", msg+ 'Field Empty', "error");
      return false;
     }
     if(isEmail($('#company_email').val())!=true){
       swal("Oops!!", 'Invalid Email', "error");
       return false;
     }
     swal({title:"", text:"Adding Company....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
     data={company_name:$('#company_name').val(),company_add:$("#company_add").val(),company_tin:$("#company_tin").val(),company_rc:$("#company_rc").val(),company_bvn:$("#company_bvn").val(),company_tax_zone:$("#company_tax_zone").val(),company_admin:$("#company_admin").val(),company_phone:$("#company_phone").val(),company_email:$("#company_email").val()}
     console.log(data);
     $.ajax({url:origin_url+"/index.php/welcome/add_company", type:"POST", data:data,
         success:function(response) {
         var status = JSON.parse(response);
         if(status =='sent'){
            sweetAlert({
             title: 'Confirm',
             text: 'Company added',
             type: 'success'
           }); 
          }
          else if(status == 'exist'){
           swal("Oops!!", 'Company Already Exist', "error");
          }
         console.log(status);
           $('#managing_companies').text(parseInt($('#managing_companies').text())+1);
           $('#company_name').val('');
           $("#company_add").val('');
           $("#company_tin").val('');
           $("#company_rc").val('');
           $("#company_tax_zone").val('');
          $("#company_bvn").val('');
          $("#company_admin").val('');
          $("#company_phone").val('');
          $("#company_email").val('');
         
       }
      });
    });

$('.add_coord').click(function(e){
      e.preventDefault();
      var name = $('#coord_name').val();
      var phone = $('#coord_phone').val();
      var email = $('#coord_email').val();
      var region = $('#coord_region').val();
      var msg='';
      if(name ==""){
        msg ='Name';
      }
       if(phone ==""){
        msg +=' Phone';
      }
       if(email ==""){
        msg +=' email';
      }
      if(region ==""){
        msg +=' Region';
      }
      if(msg !=""){
        swal("Oops!", msg+ ' Field Empty', "error");
        return false;
      }
      
      if(isEmail(email) ==false){
        swal("Oops!", 'Invalid Email', "error");
        return false;
      }

      if(!($.isNumeric(phone) && Math.floor(phone) == phone)){
        swal("Oops!!", 'Please Input Digit Only in Phone Number field', "error");
        return false;
      }
      swal({title:"", text:"Creating Account.....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
      data = {name:name,phone:phone,email:email,category:'coordinator',region:region}
      console.log(data);
      //return false;
      $.ajax({url:origin_url+"/index.php/welcome/add_firs_team", type:"POST", data:data,
         success:function(response){
          var status = JSON.parse(response);
          //alert(status);
          if(status ==true){
            sweetAlert({
             title: 'Confirm',
             text: 'Coordinator added',
             type: 'success'
           });
            $('#coord_name').val('');
            $('#coord_phone').val('');
            $('#coord_email').val('');
            //$('#category').val('');
            $('#coord_region').val('');

          }
          else if(status=="exist"){
            swal("Oops!!", 'User Already Exist', "error");
          }
          else if(status=="error"){
            swal("Oops!!", 'Unknown Error,try again later', "error");
          }
         }
      })
    });

$('.add_auditor').click(function(e){
      e.preventDefault();
      var name = $('#auditor_name').val();
      var phone = $('#auditor_phone').val();
      var email = $('#auditor_email').val();
      var region = $('#auditor_region').val();
      var msg='';
      if(name ==""){
        msg ='Name';
      }
       if(phone ==""){
        msg +=' Phone';
      }
       if(email ==""){
        msg +=' email';
      }
      if(region ==""){
        msg +=' Region';
      }
      if(msg !=""){
        swal("Oops!", msg+ ' Field Empty', "error");
        return false;
      }
      
      if(isEmail(email) ==false){
        swal("Oops!", 'Invalid Email', "error");
        return false;
      }

      if(!($.isNumeric(phone) && Math.floor(phone) == phone)){
        swal("Oops!!", 'Please Input Digit Only in Phone Number field', "error");
        return false;
      }
      swal({title:"", text:"Creating Account.....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
      data = {name:name,phone:phone,email:email,category:'auditor',region:region}
      console.log(data);
      //return false;
      $.ajax({url:origin_url+"/index.php/welcome/add_firs_team", type:"POST", data:data,
         success:function(response){
          var status = JSON.parse(response);
          //alert(status);
          if(status ==true){
            sweetAlert({
             title: 'Confirm',
             text: 'Auditor added',
             type: 'success'
           });
         $('#auditor_name').val();
         $('#auditor_phone').val();
         $('#auditor_email').val();
         $('#auditor_region').val();

          }
          else if(status=="exist"){
            swal("Oops!!", 'User Already Exist', "error");
          }
          else if(status=="error"){
            swal("Oops!!", 'Unknown Error,try again later', "error");
          }
         }
      })
    });

 $('#coord_phone').focusout(function(){
    var phone =  $('#firs_team_phone').val();
     if($.isNumeric(phone) && Math.floor(phone) == phone){}
      else{
        swal("Oops!!", 'Please Input Digit Only in Phone Number field', "error");
      }
 });
 var copyauditors=[];
 $('#assign_task').click(function(){
  //alert("am");
  select_day();
  $('.coord_report_ass').addClass('hide');
  $('.coord_ongoing_ass').addClass('hide');
  $('.coord_orders').addClass('hide');
  $('.coord_finished_ass').addClass('hide');
  $('.coord_orders').removeClass('show-for-large-up');
    $('.coord_orders').addClass('hide');
    $('.assess_for_small_dev').addClass('hide');
  //$('.all_gen_reports').addClass('hide');
  $('#taskpage').removeClass('hide');
  select_month();
  if(copyauditors.length != 0){
    var res = copyauditors.slice();
    $('#auditor-in-charge').html('');
                var company = document.getElementById("auditor-in-charge"); 
                var createOption = document.createElement("option");
                createOption.textContent = "Select Auditor";
                createOption.value = ""; 
                company.appendChild(createOption); 
                for(var r=0;r<res.length;r++){
                   createOption = document.createElement("option");
                    createOption.textContent = res[r].firstname;
                    createOption.value = res[r].id; 
                    company.appendChild(createOption);
                }
                return false;
  }
   $('.msg').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
 	//swal({title:"", text:"Creating Account.....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
   //$("#taskpage").removeClass('hide');
   $('.ongoing_ass').addClass('hide');
    $.ajax({url:origin_url+"/index.php/dashboard/all_auditors", type:"POST", data:null,
         success:function(response) {
              var res = JSON.parse(response);
              if(res.length >0){
                 copyauditors =  res.slice();
                $('#auditor-in-charge').html('');
                var company = document.getElementById("auditor-in-charge"); 
                var createOption = document.createElement("option");
                createOption.textContent = "Select Auditor";
                createOption.value = ""; 
                company.appendChild(createOption); 
                for(var r=0;r<res.length;r++){
                   createOption = document.createElement("option");
                    createOption.textContent = res[r].firstname;
                    createOption.value = res[r].id; 
                    company.appendChild(createOption);
                }
                $("#taskpage").removeClass('hide');
                $('.msg').html('');
              }
              else{
                $('.msg').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns">No COmpany Added</div></div>');
              }
         }

       });
 });
 $(document).on('change','#auditor-in-charge',function(){
   //$('.msg').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
     //$('.ongoing_ass').addClass('hide');
    $.ajax({url:origin_url+"/index.php/dashboard/all_companies", type:"POST", data:null,
         success:function(response) {
              var res = JSON.parse(response);

              if(res.length >0){
                    console.log(res);
                $('#company_to_audit').html('');
                var company = document.getElementById("company_to_audit"); 
                var createOption = document.createElement("option");
                createOption.textContent = "Select Company";
                createOption.value = ""; 
                company.appendChild(createOption); 
                for(var r=0;r<res.length;r++){
                   createOption = document.createElement("option");
                    createOption.textContent = res[r].Name;
                    createOption.value = res[r].id; 
                    company.appendChild(createOption);
                }
                //$("#taskpage").removeClass('hide').hide().fadeIn('100000');
                //$('.msg').html('');
              }
              else{
                $('.msg').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns">No COmpany Added</div></div>');
              }
         }

       });
 });

  var wait=0;
  $(document).on('click','#task_to_do',function(){

    var d = new Date();
    var t = d.toString().split(' ');
    var company_id = $('#company_to_audit').val();
    var auditor_id = $("#auditor-in-charge").val();
    var begin = $('#begin').val();
    var end = $('#end').val();
    var year = $('#year').val();
    var from_day = $("#day_from").val();
    var from_month =$('#from_month').val();
    var day_to = $('#day_to').val();
    var month_to = $('#month_to').val();
    var audit_yr = $('#audit_yr').val();
    var audit_time = from_day+';'+from_month+";"+day_to+";"+month_to+";"+audit_yr; 
    if(company_id =="" || auditor_id =="" || begin=="" || end=="" || year =="" || from_day=="" || from_month=="" || day_to=="" || month_to ==""){
      swal("Oops!!", 'All input fields are compulsory', "error");
      return false;
    }
    data={auditor_id:auditor_id,company_id:company_id,begin:begin,end:end,year:year,audit_time:audit_time};
    console.log(data);
    swal({title:"", text:"Assigning Task....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
    $.ajax({url:origin_url+"/index.php/dashboard/assign_task", type:"POST", data:data,
            success:function(response){              
              var res = JSON.parse(response);
              if(res.success =='true'){
                sweetAlert({
                       title: 'Confirm',
                       text: 'Task Assigned',
                       type: 'success'
                     });
               //$("#taskpage").addClass('hide');
                //$('#assessment_order').removeClass('hide');
                $('#status').text(res.status);
                $('#order_date').text(t[0] +" "+t[1] +" "+t[2] +" "+t[3]);
                $('#company_name').text(res.company);
                $('#fin_period').text(begin +' to '+ end +' '+ year);
                $("#assess_id").text(res.assessment_id);
                $('#auditors_name').text(res.auditor);
                $("#audit_start_date").text(from_day+" of "+from_month +" "+audit_yr);
                $("#audit_end_date").text(day_to+" of "+month_to +" "+audit_yr);

               }
            }

           }); 
  });
$(document).on('click','#assigned_task',function(){
  $('#assessment_orders').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
    //swal({title:"", text:"Assigned Task....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
    $.ajax({url:origin_url+"/index.php/dashboard/assigned_task", type:"POST", data:null,
      success:function(response){
         var res = JSON.parse(response);
         if(res.length > 0){
          $('#assessment_orders').html('');
          for(var x=0;x<res.length;x++){
              var clone = $("#assessment_order").clone().removeClass('hide');
              $('#assessment_orders').append("<div style='margin-top:10px;'></div>");
              clone.find('.status').text(res[x].status.toUpperCase());
              clone.find('.order_date').text(res[x].created);
              clone.find('.company_name').text(res[x].company_name);
              clone.find('.fin_period').text(res[x].period);
              clone.find('.assess_id').text(res[x].assessment_id);
              clone.find('.auditors_name').text(res[x].auditor_name);
              clone.find('.audit_start_date').text(res[x].start_audit_time);
              clone.find('.audit_end_date').text(res[x].end_audit_time);
              //clone.find('.audit_start_date').text(res[x].company_name);
              $('#assessment_orders').append(clone);

          }

         }
         else{
             $('#assessment_orders').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns">No Assessment Order</div></div>');
         }
      }
    });
})
$(document).on('click',"#show_bar",function(){
   $(".divflow").mCustomScrollbar({
    axis:"x", // horizontal scrollbar
    theme:"dark-3",
  });
});
var animate;
var myVar;
function notify() {
   var color = $("#"+animate+"").css('background-color');
   if(color=='rgb(76, 175, 80)'){
        $("#"+animate+"").css('background-color','#e7505a');
      $("#"+animate+"").css('color','#e7505a');
      $("#"+animate+"").css('margin-top','14px');

   }
   else{
      $("#"+animate+"").css('background-color','#4CAF50');
      $("#"+animate+"").css('color','#4CAF50');
      $("#"+animate+"").css('margin-top','16px');

   }
}
$(document).on('click','#ongoing_ass',function(){
                $('.assessment_order').removeClass('show-for-large-up');
                $('.assessment_order').addClass('hide');
                $('.assess_for_small_dev').addClass('hide');
                $('.finished_ass').addClass('hide');
                $('.ongoing_ass').removeClass('hide');
                $('.gen_report').addClass('hide');
                $('.all_gen_reports').addClass('hide');
                $('#dash_title').text('Ongoing Assessment');
                //alert(justone_ass);
                var all_accepted = ['Statement_of_account:Accepted','Customer_invoices:Accepted','payment_customer_invoices:Accepted','supplier_invoices:Accepted','payment_supplier_invoices:Accepted','','',''];
                var all_uploaded = ['Statement_of_account_uploaded','Customer_invoices_uploaded','payment_customer_invoices_uploaded','supplier_invoices_uploaded','payment_supplier_invoices_uploaded','external_file_uploaded','',''];
                $('.div_for_table').addClass('hide');
                $('.ongoing_ass').removeClass('add_to_no_ongoing');
                $('#assessment_orders').append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
                 $.ajax({url:origin_url+"/index.php/dashboard/get_one_ongoing_for_auditor", type:"POST", data:{assess:justone_ass},
                 success:function(response){
                  var res = JSON.parse(response);
                  $(".ongoing_ass").html('');
                  if(res.length > 0){
                    //alert(res.length);
                    var x=0;
                    for(var m=0;m<res.length;m++){
                       if(res[m].status.toLowerCase() =='open' || res[m].status.toLowerCase() =='work in progress' || res[m].status.toLowerCase() =='closed'){
                        x=1;
                        var stage = res[m].status.toLowerCase()=='work in progress'?"WIP":res[m].status.toUpperCase();
                        //alert(res[m].stage);
               var ongoing = "<div class='eachass' id='eachass"+m+"' data-ongoing='"+res[m].assessment_id+"'>"
                                +"<div class='row' style='background-color:#364150;margin-top:20px;color:white;padding:5px 5px 5px 5px;'><div class='small-4 medium-4 large-4 columns left'>"
                                +res[m].Name.toUpperCase()+"</div><div class='small-4 medium-4 large-4 columns'>ID: "+res[m].assessment_id+"</div>"
                                +"<div class='small-4 medium-4 large-4 columns'>STAGE: "+stage+"</div></div>"
                                +"<div class='row auditor_flow show-for-medium-up' style='margin-top:20px;'><div class='small-4 medium-4 large-4 columns'>Auditor Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><div class='small-4 medium-4 large-4 columns'>Auditor Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><div class='small-4 medium-4 large-4 columns'>Auditor Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><hr class='show-for-medium-up' style='margin-top:-3px;'></div>"
                                +"<div class='divflow' style=''>"

                                +"<div class='div1 only' style=''><ul class='all_flows'>"
                                +"<li><span class='eachfile'  id='Statement_of_account"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='Statement_of_account' company_id = '"+res[m].company_id+"' id='companyfile"+m+"' data-reveal-id='companyuploadedfile'>Statement of Account</a></span><span class='subcon' id='subcon"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='sub' id='sub0"+m+"' filename='Statement_of_account'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='Customer_invoices"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='Customer_invoices' company_id = '"+res[m].company_id+"' id='companyfile2"+m+"' data-reveal-id='companyuploadedfile'>Customer Invoices</a></span><span class='subcon' id='subcon1"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='sub' id='sub1"+m+"' filename='Customer_invoices'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='payment_customer_invoices"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='payment_customer_invoices' company_id = '"+res[m].company_id+"' id='companyfile3"+m+"' data-reveal-id='companyuploadedfile'>Payment On Customer Invoices</a></span><span class='subcon' id='subcon2"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='sub' id='sub2"+m+"' filename='payment_customer_invoices'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='supplier_invoices"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='supplier_invoices' company_id = '"+res[m].company_id+"' id='companyfile4"+m+"' data-reveal-id='companyuploadedfile'>Supplier Invoices</a></span><span class='subcon' id='subcon3"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='sub' id='sub3"+m+"' filename='supplier_invoices'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='payment_supplier_invoices"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='payment_supplier_invoices' company_id = '"+res[m].company_id+"' id='companyfile5"+m+"' data-reveal-id='companyuploadedfile'>Payment On Supplier Invoices</a></span><span class='subcon' id='subcon4"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='sub' id='sub4"+m+"' filename='payment_supplier_invoices'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='external_file"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='otherfile' num='"+m+"' filename='external_file' company_id = '"+res[m].company_id+"' id='otherfile"+m+"' data-reveal-id='otherfile'>External Document</a></span><span class='subcon' id='subcon5"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='initial_assessment"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='generate_vat' num='"+m+"' filename='initial_assessment' company_id = '"+res[m].company_id+"' id='vat"+m+"' data-reveal-id='generate_vat'>Generate Initial Assessment</a></span><span class='subcon' id='subcon6"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='sub' id='sub5"+m+"' filename='initial_assessment'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='final_assessment"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='reportfile' num='"+m+"' filename='final_assessment' company_id = '"+res[m].company_id+"' id='reportfile8"+m+"' data-reveal-id='Report'>Generate Final Assessment</a></span><span class='subcon' id='subcon7"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='sub' id='sub6"+m+"' filename='final_assessment'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"</ul></div>"

                                +"<div class='show-for-medium-up progress [small-# large-#] [secondary alert success] [radius round]'>"
                                +"<span class='meter' id='meter"+m+"' style=''></span></div>"
                                +"<hr class='show-for-small-only' style='margin-bottom:40px;margin-top:-40px;'/>"
                                +"<div  style='margin-top:20px;margin-bottom:20px;'></div>"
                                 +"<div class='row companyflowtext show-for-medium-up' style='margin-left:-50px;'><div class='small-6 medium-4 large-4 large-offset-1 columns'>Company Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><div class='medium-4 large-3 columns'>Company Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><div class='medium-4 large-3 columns'>Company Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><hr class='show-for-medium-up' style='margin-top:-3px;width:1300px;margin-left:80px;'></div>"
                                +"<div class='div1'><ul class='all_flows'>"
                                +"<li><span class='eachfile' id='Statement_of_account2"+m+"'>Statement of Account</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='Customer_invoices2"+m+"'>Customer Invoices</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='payment_customer_invoices2"+m+"'>Payment On Customer Invoices</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='supplier_invoices2"+m+"'>Supplier Invoices</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='payment_supplier_invoices2"+m+"'>Payment On Supplier Invoices</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='external_file2"+m+"'>External Document</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='initial_assessment2"+m+"'>Generate Initial Assessment</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='final_assessment2"+m+"'>Generate Final Assessment</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"</ul></div>"
                                +"</div>"
                              $(".ongoing_ass").append(ongoing);  
                              //$('#show_bar').trigger("click");
                               scroll_x();
                               var vat = $.inArray('vat_generated',res[m].all_flow);
                               if(vat > 0){
                                 $("#initial_assessment"+m+"").css('border','2px solid #4CAF50');
                                 $("#initial_assessment2"+m+"").css('border','2px solid #4CAF50');
                                 $("#meter"+m+"").css('width','90%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('90%');
                               }
                               for(s=0;s<all_accepted.length;s++){
                                var acc = $.inArray(all_accepted[s],res[m].all_flow);
                                var upload = $.inArray(all_uploaded[s],res[m].all_flow);
                                if(acc>0){
                                  var filename = all_accepted[s].split(':')[0];
                                  $("#"+filename+""+m+"").css('border','2px solid #4CAF50');
                                  $("#"+filename+"2"+m+"").css('border','2px solid #4CAF50');
                                }
                                else if(acc==-1 && upload>0){
                                   var filename = all_uploaded[s].split('_uploaded')[0];
                                  $("#"+filename+""+m+"").css('border','2px solid yellow');
                                  $("#"+filename+"2"+m+"").css('border','2px solid yellow');
                                  if(filename ='external_file'){
                                     $("#"+filename+""+m+"").css('border','2px solid yellow');
                                     $("#"+filename+"2"+m+"").css('border','2px solid yellow');
                                  }
                                }
                              }
                              var final_assessment = $.inArray('final_assessment_uploaded',res[m].all_flow);
                              if(final_assessment > 0){
                                 $("#final_assessment"+m+"").css('border','2px solid #4CAF50');
                                 $("#final_assessment2"+m+"").css('border','2px solid #4CAF50');
                                 $("#meter"+m+"").css('width','100%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('100%');

                               }
                              if(res[m].stage=='Statement_of_account_uploaded'){
                                //alert()
                                $("#Statement_of_account"+m+"").css('border','2px solid yellow');
                                $("#Statement_of_account2"+m+"").css('border','2px solid yellow');
                                $("#Statement_of_account"+m+"").addClass('animated wobble');
                                $("#meter"+m+"").css('width','5%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('5%');
                                 $("#Statement_of_account"+m+"").addClass('animated wobble');
                                  $("#Statement_of_account"+m+"").css('-webkit-animation-duration','5s');
                                 $("#Statement_of_account"+m+"").css('-webkit-animation-delay','2s');
                              } 
                              else if(res[m].stage=='Statement_of_account:message_from_company'){
                                 $("#subcon"+m+"").addClass('animated infinite bounce');
                                 $("#sub0"+m+"").removeClass('msg_icon sub');
                                 $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                 $("#sub0"+m+"").addClass('animated infinite pulse');
                                 animate = "sub0"+m;
                                 myVar = setInterval(notify, 300);
                              } 
                              else if(res[m].stage=='Statement_of_account:message_from_auditor'){
                                 $("#sub0"+m+"").css('background-color','skyblue');
                                 $("#Statement_of_account"+m+"").css('border','2px solid skyblue');
                              }
                              else if(res[m].stage=='Statement_of_account:Rejected'){
                                $("#Statement_of_account"+m+"").css('border','2px solid red');
                                $("#Statement_of_account2"+m+"").css('border','2px solid red');
                                $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                //$("#sub0"+m+"").addClass('animated infinite pulse');
                              }  
                              else if(res[m].stage=='Statement_of_account:Accepted'){
                                $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','10%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('10%');

                              }  
                              else if(res[m].stage=='Customer_invoices_uploaded'){
                                //alert()
                                $("#Customer_invoices"+m+"").css('border','2px solid yellow');
                                $("#Customer_invoices2"+m+"").css('border','2px solid yellow');
                                $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','15%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('15%');
                                 $("#Customer_invoices"+m+"").addClass('animated wobble');
                                  $("#Customer_invoices"+m+"").css('-webkit-animation-duration','5s');
                                 $("#Customer_invoices"+m+"").css('-webkit-animation-delay','2s');
                              }  
                              else if(res[m].stage=='Customer_invoices:message_from_company'){
                                 $("#subcon1"+m+"").addClass('animated infinite bounce');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#sub1"+m+"").removeClass('msg_icon sub');
                                 $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                 animate = "sub1"+m;
                                 myVar = setInterval(notify, 300);
                              }
                              else if(res[m].stage=='Customer_invoices:message_from_auditor'){
                                 $("#sub1"+m+"").css('background-color','skyblue');
                                 $("#Customer_invoices"+m+"").css('border','2px solid skyblue');
                                  $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                              }
                              else if(res[m].stage=='Customer_invoices:Rejected'){
                                $("#Customer_invoices"+m+"").css('border','2px solid red');
                                $("#Customer_invoices2"+m+"").css('border','2px solid red');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                $("#sub1"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','15%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('10%');
                              }  
                              else if(res[m].stage=='Customer_invoices:Accepted'){
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#sub1"+m+"").removeClass('msg_icon sub');
                                $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                 $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','15%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('15%');
                              }  
                              else if(res[m].stage=='payment_customer_invoices_uploaded'){
                                //alert()
                                $("#payment_customer_invoices"+m+"").css('border','2px solid yellow');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid yellow');
                                $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','20%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('20%');
                                  $("#payment_customer_invoices"+m+"").addClass('animated wobble');
                                  $("#payment_customer_invoices"+m+"").css('-webkit-animation-duration','5s');
                                 $("#payment_customer_invoices"+m+"").css('-webkit-animation-delay','2s');
                              }  
                              else if(res[m].stage=='payment_customer_invoices:message_from_company'){
                                 $("#subcon2"+m+"").addClass('animated infinite bounce');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#sub2"+m+"").removeClass('msg_icon sub');
                                 $("#sub2"+m+"").addClass('msg_icon2 sub2');
                                animate = "sub2"+m;
                                 myVar = setInterval(notify, 300);
                              }
                              else if(res[m].stage=='payment_customer_invoices:message_from_auditor'){
                                 //$("#subcon2"+m+"").addClass('animated infinite bounce');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#payment_customer_invoices"+m+"").css('border','2px solid skyblue');
                                 $("#sub2"+m+"").removeClass('msg_icon sub');
                                 $("#sub2"+m+"").addClass('msg_icon2 sub2');
                              }
                              else if(res[m].stage=='payment_customer_invoices:Rejected'){
                                $("#payment_customer_invoices"+m+"").css('border','2px solid red');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid red');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 $("#sub2"+m+"").removeClass('msg_icon sub');
                                $("#sub2"+m+"").addClass('msg_icon2 sub2');
                                $("#sub1"+m+"").removeClass('msg_icon sub');
                                $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','20%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('20%');
                              }  
                              else if(res[m].stage=='payment_customer_invoices:Accepted'){
                                $("#payment_customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#sub2"+m+"").removeClass('msg_icon sub');
                                $("#sub2"+m+"").addClass('msg_icon2 sub2');
                                 $("#sub1"+m+"").removeClass('msg_icon sub');
                                $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                 $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','25%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('25%');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#sub012"+m+"").removeClass('msg_icon sub');
                                $("#sub012"+m+"").addClass('msg_icon2 sub2');
                              }
                             
                              else if(res[m].stage=='supplier_invoices_uploaded'){
                                //alert()
                                $("#payment_customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#supplier_invoices"+m+"").css('border','2px solid yellow');
                                $("#supplier_invoices"+m+"").addClass('animated wobble');
                                $("#supplier_invoices2"+m+"").css('border','2px solid yellow');
                                $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','30%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('30%');
                                 $("#supplier_invoices"+m+"").css('-webkit-animation-duration','5s');
                                 $("#supplier_invoices"+m+"").css('-webkit-animation-delay','2s');
                              }  
                              else if(res[m].stage=='supplier_invoices:message_from_company'){
                                 $("#subcon3"+m+"").addClass('animated infinite pulse');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                 $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 //$("#supplier_invoices"+m+"").css('border','2px solid skyblue');
                                 $("#sub3"+m+"").removeClass('msg_icon sub');
                                 $("#sub3"+m+"").addClass('msg_icon2 sub2');
                                  animate = "sub3"+m;
                                 myVar = setInterval(notify, 300);

                              }
                              else if(res[m].stage=='supplier_invoices:message_from_auditor'){
                                 //$("#subcon3"+m+"").addClass('animated infinite bounce');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                 $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                 $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 //$("#supplier_invoices"+m+"").css('border','2px solid #3598dc');
                                 $("#payment_customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                 $("#payment_customer_invoices2 "+m+"").css('border','2px solid #4CAF50');

                              }
                              else if(res[m].stage=='supplier_invoices:Rejected'){
                                $("#supplier_invoices"+m+"").css('border','2px solid red');
                                $("#supplier_invoices2"+m+"").css('border','2px solid red');
                                $("#payment_customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#sub2"+m+"").removeClass('msg_icon sub');
                                $("#sub2"+m+"").addClass('msg_icon2 sub2');
                                $("#sub1"+m+"").removeClass('msg_icon sub');
                                $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#sub012"+m+"").removeClass('msg_icon sub');
                                $("#sub012"+m+"").addClass('msg_icon2 sub2');
                                $("#sub013"+m+"").removeClass('msg_icon sub');
                                $("#sub013"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','30%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('30%');
                              }  
                              else if(res[m].stage=='supplier_invoices:Accepted'){
                                $("#supplier_invoices"+m+"").css('border','2px solid #4CAF50');
                                 $("#supplier_invoices"+m+"").addClass('animated bounce');
                                $("#supplier_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#sub2"+m+"").removeClass('msg_icon sub');
                                $("#sub2"+m+"").addClass('msg_icon2 sub2 animated bounce');
                                 $("#sub1"+m+"").removeClass('msg_icon sub');
                                $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                 $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','35%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('35%');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#sub012"+m+"").removeClass('msg_icon sub');
                                $("#sub012"+m+"").addClass('msg_icon2 sub2');
                                $("#sub013"+m+"").removeClass('msg_icon sub');
                                $("#sub013"+m+"").addClass('msg_icon2 sub2');

                              }
                              else if(res[m].stage=='payment_supplier_invoices_uploaded'){
                                //alert()
                                $("#payment_supplier_invoices"+m+"").css('border','2px solid yellow');
                                $("#payment_supplier_invoices2"+m+"").css('border','2px solid yellow');
                                $("#payment_customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#supplier_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#supplier_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','40%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('40%');
                                 $("#payment_supplier_invoices"+m+"").addClass('animated wobble');
                                  $("#payment_supplier_invoices"+m+"").css('-webkit-animation-duration','5s');
                                 $("#payment_supplier_invoices"+m+"").css('-webkit-animation-delay','2s');

                              }  
                              else if(res[m].stage=='payment_supplier_invoices:message_from_company'){
                                 $("#subcon4"+m+"").addClass('animated infinite bounce');
                                  animate = "sub4"+m;
                                 myVar = setInterval(notify, 300);
                              }
                              else if(res[m].stage=='payment_supplier_invoices:Rejected'){
                                $("#payment_supplier_invoices"+m+"").css('border','2px solid red');
                                $("#payment_supplier_invoices2"+m+"").css('border','2px solid red');
                                $("#supplier_invoices"+m+"").css('border','2px solid red');
                                $("#supplier_invoices2"+m+"").css('border','2px solid red');
                                $("#payment_customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 $("#sub2"+m+"").removeClass('msg_icon sub');
                                $("#sub2"+m+"").addClass('msg_icon2 sub2');
                                $("#sub1"+m+"").removeClass('msg_icon sub');
                                $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#sub012"+m+"").removeClass('msg_icon sub');
                                $("#sub012"+m+"").addClass('msg_icon2 sub2');
                                $("#sub013"+m+"").removeClass('msg_icon sub');
                                $("#sub013"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','40%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('40%');
                              }  
                              else if(res[m].stage=='payment_supplier_invoices:Accepted'){
                                $("#payment_supplier_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_supplier_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#supplier_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#supplier_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#payment_customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices"+m+"").css('border','2px solid #4CAF50');
                                $("#Customer_invoices2"+m+"").css('border','2px solid #4CAF50');
                                 $("#Statement_of_account"+m+"").css('border','2px solid #4CAF50');
                                $("#Statement_of_account2"+m+"").css('border','2px solid #4CAF50');
                                $("#sub2"+m+"").removeClass('msg_icon sub');
                                $("#sub2"+m+"").addClass('msg_icon2 sub2');
                                 $("#sub1"+m+"").removeClass('msg_icon sub');
                                $("#sub1"+m+"").addClass('msg_icon2 sub2');
                                 $("#sub0"+m+"").removeClass('msg_icon sub');
                                $("#sub0"+m+"").addClass('msg_icon2 sub2');
                                $("#sub010"+m+"").removeClass('msg_icon sub');
                                $("#sub010"+m+"").addClass('msg_icon2 sub2');
                                $("#meter"+m+"").css('width','45%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('45%');
                                $("#sub011"+m+"").removeClass('msg_icon sub');
                                $("#sub011"+m+"").addClass('msg_icon2 sub2');
                                $("#sub012"+m+"").removeClass('msg_icon sub');
                                $("#sub012"+m+"").addClass('msg_icon2 sub2');
                                $("#sub013"+m+"").removeClass('msg_icon sub');
                                $("#sub013"+m+"").addClass('msg_icon2 sub2');
                              }
                               else if(res[m].stage=='initial_assessment:message_from_company'){
                                 $("#subcon3"+m+"").addClass('animated infinite pulse');
                                 $("#sub5"+m+"").removeClass('msg_icon sub');
                                $("#sub5"+m+"").addClass('msg_icon2 sub2');
                                animate = "sub5"+m;
                                myVar = setInterval(notify, 300);
                              }


                      
                 }
               }
               if(x==0){
                $('.ongoing_ass').addClass('add_to_no_ongoing');
                $('.ongoing_ass').append('<div>Yet to Start</div>');
               }
               
          }
          else{
                   $('.ongoing_ass').addClass('add_to_no_ongoing');
                   $('.ongoing_ass').append('<div>Yet to Start</div>');
               }
              
        }
      });
  
});
$(document).on("click","#share_with_company",function(e){
 e.preventDefault();
  var text = $("#g_vat").text();
   $.ajax({url:origin_url+"/index.php/dashboard/msg", type:"POST", data:{assessment_id:gen_ass_id,whichfile:'initial_assessment',company_id:share_gen_vat_with_company_id,text:text,request:'share generated vat with company'},
    success:function(response){
      var res  =JSON.parse(response);
        
     }
     }); 
});
var uploadfile_ass_id,share_gen_vat_with_company_id,gen_ass_id,toupload_filename;
$(document).on("click",'.otherfile',function(){
  uploadfile_ass_id = $("#"+this.id+"").attr('assess_id');
  toupload_filename =  $("#"+this.id+"").attr('filename');
});
$(document).on("click",'.reportfile',function(){
  uploadfile_ass_id = $("#"+this.id+"").attr('assess_id');
  toupload_filename =  $("#"+this.id+"").attr('filename');
});

$(document).on("click",'.generate_vat',function(){
     gen_ass_id = $("#"+this.id+"").attr('assess_id');
     share_gen_vat_with_company_id = $("#"+this.id+"").attr('company_id');
  $(".the_gen_vat").addClass('hide');
  $(".loading").removeClass("hide");
   $.ajax({url:origin_url+"/index.php/dashboard/gen_vat", type:"POST", data:{assessment_id:gen_ass_id},
    success:function(response){
      var res  =JSON.parse(response);
      $(".loading").addClass("hide");
      $(".the_gen_vat").removeClass('hide');
      $(".the_gen_vat").html("<div class='small-12 medium-12 large-12 columns'>Generated VAT is #<span id='g_vat'>"+res+"</span></div>"); 
     }
     }); 
});
$(document).on("click",'.subcon',function(){
  $("#"+this.id+"").removeClass('animated infinite bounce');
});
var companyassess_id,whichfile,company_under_ass_id,id_of_selected;
$(document).on("click",".companyfile",function(e){
  e.preventDefault();
  $('.sta_of_doc').attr('checked', false);
  $(".comment").addClass('hide');
         $(".doc_stuff").slideDown();
         $('.change-title').text('UPLOADED FILE');
  companyassess_id = $("#"+this.id+"").attr('assess_id');
  whichfile = $("#"+this.id+"").attr('filename');
  company_under_ass_id =  $("#"+this.id+"").attr("company_id");
  id_of_selected = $("#"+this.id+"").attr('num');
   //swal({title:"", text:"Sending Comments.....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
    $.ajax({url:origin_url+"/index.php/dashboard/getuploadedfile", type:"POST", data:{assessment_id:companyassess_id,whichfile:whichfile},
    success:function(response){
      var res  =JSON.parse(response);
      $(".uploadedfile").html('');
      if(res.length > 0){
        var files = res[0].files.split(';');
        for(var b=0;b<files.length;b++){
          $(".status_of_doc").removeClass('hide');
          $(".uploadedfile").append('<div class="row" style="text-align:center;"><div class="small-12 medium-12 large-12 columns"><a class="" assess_id ="'+companyassess_id+'" p="'+id_of_selected+'" id="clicked_to_download'+id_of_selected+'" href="http://localhost/tax/assets/upload/'+files[b]+'" download>Download and Review File</a></div></div>');
        }
      }
      else{
        $(".status_of_doc").addClass('hide');
        $(".uploadedfile").append('<div class="row" style="text-align:center;"><div class="small-12 medium-12 large-12 columns"><a>File Yet to be Uploaded</a></div></div>');
      }

    }
   });     


})
$(document).on("click","#backtomainpage",function(e){
 e.preventDefault();
         $(".comment").addClass('hide');
         $(".doc_stuff").slideDown();
         $('.change-title').text('UPLOADED FILE');
});
 var status_of_file;
$(document).on('click','.sta_of_doc',function(e){
     //var num = status_of_doc;
       var border = $("#"+whichfile+""+id_of_selected+"").css('border');
        if(border=='2px solid rgb(76, 175, 80)'){
          sweetAlert('Oops!!!','You have already accepted this file, you can not re-accept or reject','warning');
          return false;
        }
         status_of_file = $("input[type=radio]:checked").val();
      if(status_of_file =='Rejected'){
        $(".doc_stuff").slideUp();
         $(".comment").removeClass('hide');
         $('.change-title').text('Comments');
         $("#reject_with_comment").val('');
      }
   })
$(document).on('close.fndtn.reveal', '#companyuploadedfile', function () {
    var border = $("#"+whichfile+""+id_of_selected+"").css('border');
        if(border=='2px solid rgb(76, 175, 80)'){
          //sweetAlert('Oops!!!','You have already accepted this file, you can not re-accept or reject','warning');
          return false;
        }
   var to_save = whichfile+":"+status_of_file;
   if(status_of_file==undefined || status_of_file=='Rejected'){
    return false;
   }
   var msg = 'File Accepted';
   company_id = company_under_ass_id;
   swal({title:"", text:"Sending Comments.....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
    $.ajax({url:origin_url+"/index.php/dashboard/request", type:"POST", data:{request:to_save,text:msg,assessment_id:companyassess_id,company_id:company_under_ass_id,whichfile:whichfile},
             success:function(response){
              var res = JSON.parse(response);
              //alert(res);
              if(res==true){
                sweetAlert({
                 title: 'Confirm',
                 text: 'Comment Sent',
                 type: 'success',
                 showCancelButton: false,
                 confirmButtonText: 'Yes',
                 cancelButtonText: 'No'
               });
              }
              //sub_id = '1';
                //$('.sub').trigger("click");
              $("#"+whichfile+""+id_of_selected+"").css('border','2px solid #4CAF50');
              $("#"+whichfile+"2"+id_of_selected+"").css('border','2px solid #4CAF50');
            }
              });
});
var sub_id='0';
$(document).on("click","#sendcomment",function(e){
   var msg = $("#reject_with_comment").val();
   if(msg ==""){
    sweetAlert("Oops!!!","Please Input message",'error');
    return false;
   }
   var to_save = whichfile+":"+status_of_file;
   company_id = company_under_ass_id;
   swal({title:"", text:"Sending Comments.....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
    $.ajax({url:origin_url+"/index.php/dashboard/request", type:"POST", data:{request:to_save,text:msg,assessment_id:companyassess_id,company_id:company_under_ass_id,whichfile:whichfile},
             success:function(response){
              var res = JSON.parse(response);
              //alert(res);
              if(res==true){
                sweetAlert({
                 title: 'Confirm',
                 text: 'Rejection Comment Sent',
                 type: 'success',
                 showCancelButton: false,
                 confirmButtonText: 'Yes',
                 cancelButtonText: 'No'
               });
              }
              $("#reject_with_comment").val('');
              //sub_id = '1';
                //$('.sub').trigger("click");
              $("#"+whichfile+""+id_of_selected+"").css('border','2px solid red');
              $("#"+whichfile+"2"+id_of_selected+"").css('border','2px solid red');
            }
              });
});
var assessment_id_for_chats,company_id_for_chat,msg_id,msg_file_name;
$(document).on("click",".sub,.sub2", function(e){
   e.preventDefault();
   clearInterval(myVar);
   $("#allchats").html('');
   var filename =$("#"+this.id+"").attr('filename');
   msg_file_name = filename;
   assessment_id_for_chats = $("#"+this.id+"").attr('assess_id');
   company_id_for_chat = $("#"+this.id+"").attr('company_id');
   //msg_id = $("#"+this.id+"").attr('num');
    $.ajax({url:origin_url+"/index.php/dashboard/get_chats", type:"POST",data:{assess_id:assessment_id_for_chats,filename:filename},
       success:function(response){
        var res = JSON.parse(response);
       //$("#loading").addClass('hide');
       if(res.length>0){
        for(var t=0;t<res.length;t++){
          if(res[t].me==res[t].sender){
                  $("#allchats").append("<div class='clearfix'><p class='left' style='background-color:#E6E6FA;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'>"+res[t].message+"</p><p class='right'></p></div>");
                 
          }
          else {

                  $("#allchats").append("<div class='clearfix'><p class='left'></p><p class='right' style='background-color:#E6E6FA;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'>"+res[t].message+"</p></div>");
                }
               
        }
        $("#allchats").animate({ scrollTop: $("#allchats")[0].scrollHeight}, 1000);
       }
         
       }
     });

});
var status_of_doc,assess_id_to_update_status;
$(document).on('click','.status',function(e){
  e.preventDefault();
   assess_id_to_update_status = $('#'+this.id+'').attr('assess_id');
   status_of_doc = $('#'+this.id+'').attr('num');

});
$(document).on('click','#request_other_file',function(e){
  e.preventDefault();
  var request="";
  if ($('#checkbox1').is(":checked"))
  {
    request = $("#checkbox1").val();
  }
  if ($('#checkbox2').is(":checked"))
  {  
    if(request != ""){
    request += "/"+$("#checkbox2").val();
     }
     else{
     request = $("#checkbox2").val();
     }
  }
  swal({title:"", text:"Request to Upload file.....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
  $.ajax({url:origin_url+"/index.php/dashboard/request", type:"POST", data:{request:request,assessment_id:assessment_id_for_more_request},
      success:function(response){
            var res = JSON.parse(response);
            if(res==true){
              sweetAlert('Request sent');
            }
        }
    });
});
$(document).on('click','.request',function(){
  var num = $("#"+this.id+"").attr('num');
  var assess_id = $("#"+this.id+"").attr('assess_id');
  $("#is_to_upload"+num+"").removeClass('hide');
  $("#to_upload_file"+num+"").removeClass('hide');
  swal({title:"", text:"Request to Upload file.....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
   $.ajax({url:origin_url+"/index.php/dashboard/request", type:"POST", data:{request:'Request file',assessment_id:assess_id},
      success:function(response){
            var res = JSON.parse(response);
            if(res==true){
              sweetAlert('Request sent');
            }
        }
    });
});
$(document).on('click','#finished_ass',function(e){


})
$(document).on('click','#orders',function(e){
 e.preventDefault();
  $('.finished_ass').addClass('hide');
  $('.div_for_table').removeClass('hide');
  $('.ongoing_ass').addClass('hide');
  $('.gen_report').addClass('hide');
  $('.all_gen_reports').addClass('hide');
   $('.assessment_order').addClass('show-for-large-up');
  $('.assessment_order').removeClass('hide');
  $('.assess_for_small_dev').removeClass('hide');
});
 $(document).on("click","#addfile",function(e){
   e.preventDefault();
   $('#collectfile').trigger('click');
   
 });
  file_data = [];
 $('#collectfile').change(function(){
         readURL3(this);
         file_data.push($('#collectfile')[0].files[0]);
         console.log(file_data);
         //alert(assess_id_to_stage);
          $("#AcceptedF"+assess_id_to_stage+"").css('border','none');
         $("#Others"+assess_id_to_stage+"").css('border','2px solid red');
        });
  function readURL3(input) {
        var ext = $('#collectfile').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['pdf','doc','docx','txt']) == -1)
        { sweetAlert('Please Select an Document'); return false;}
        if (input.files && input.files[0]) {
          $('.listfile').append('<li style="background-color:green;color:white;margin-bottom:5px;text-align:center;">'+$('#collectfile')[0].files[0].name+'</li>');
          //$('input[type=file]')[0].files[0].name
    }
   }
       $(document).on("click","#sendfile",function(e){
      e.preventDefault();
        if(file_data.length ==0){
        sweetAlert("Please Select File");
        return false;
       }
         var form_data = new FormData();
         for(var t=0;t<file_data.length;t++){
         form_data.append('file[]',file_data[t]); 
         }
         if(filename=='final_assessment'){
           $("#final_assessment"+m+"").css('border','2px solid #4CAF50');
          $("#final_assessment2"+m+"").css('border','2px solid #4CAF50');
         }
         if(filename =='external_file'){
           $("#external_file"+m+"").css('border','2px solid #4CAF50');
          $("#external_file2"+m+"").css('border','2px solid #4CAF50');
         }
         var filename =  toupload_filename;
         form_data.append('assessment_id',uploadfile_ass_id);
         form_data.append('fin_statement_period',"same year");
         form_data.append('request',filename+'_uploaded');
         form_data.append('filename',filename);
         //alert(uploadfile_external);
         //alert(filename+":"+uploadfile_ass_id);
          swal({title:"", text:"Uploading Document....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
          $.ajax({
                url: ''+origin_url+'/index.php/dashboard/uploadfile', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(response){
                  var res = JSON.parse(response);
                    if(res==true){
                      sweetAlert({
                       title: 'Confirm',
                       text: 'File Uploaded',
                       type: 'success'
                     }); 
                    file_data = [];
                    //$("#meter"+assess_id_to_stage+"").css('width','80%');
                    $('#listfile').html('');
                  }
                }
                     
                    //$('.responsefromserver').html('<div style="text-align:center; background-color:purple;"><p>Report Sent</p></div>');
            });  

    });
  $(document).on('click','#sendreport', function(e){
    e.preventDefault();
     if ($('#copy_company').is(":checked")){
      var request ='show_report';
     }
     else if(!$('#copy_company').is(":checked")){
       var request ='hide_for_company';
     }
     var content = CKEDITOR.instances.editor1.getData();
       form_data = new FormData();
       form_data.append('request',request);
       form_data.append('content',content);
       form_data.append('assessment_id',$('#report_id').val());
       //form_data.append('company_id',report_on_company_id);
       swal({title:"", text:"Saving Document....", imageUrl: origin_url+"/assets/imgs/ajax-loader.gif",showConfirmButton:false,allowOutsideClick:false});
          $.ajax({
                url: ''+origin_url+'/index.php/dashboard/savereport', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(response){
                  var res = JSON.parse(response);
                  //alert(res);
                  console.log(res);
                    if(res==true){
                      sweetAlert({
                       title: 'Confirm',
                       text: 'Document Saved',
                       type: 'success'
                     }); 
                  }
                }
                     
                    //$('.responsefromserver').html('<div style="text-align:center; background-color:purple;"><p>Report Sent</p></div>');
            });
  });
  $(document).on('click','#send_msg',function(e){
    e.preventDefault();
    //alert(assessment_id_for_chats+';'+company_id_for_chat);
    //$('#convers'+msg_id+'').removeClass('hide');
     //$('#convers'+msg_id+'').css('border','2px solid red');
     //$("#is_reviewing"+msg_id+"").css('border','none');
     assessment_id = assessment_id_for_chats;
     company_id = company_id_for_chat;
     //alert(assessment_id_for_chats+";"+company_id);
    if($('#text_to').val()!=""){
      $("#allchats").append("<div class='clearfix'><p class='left' style='background-color:#E6E6FA;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'>"+$('#text_to').val()+"</p><p class='right'></p></div>");
      var text = $('#text_to').val();
      }
      else {return false;}
       $("#allchats").animate({ scrollTop: $("#allchats")[0].scrollHeight}, 1000);
      $('#text_to').val('');
      //alert(company_id);
      var request = msg_file_name+":message_from_auditor";
      data={text:text,company_id:company_id,assessment_id:assessment_id,request:request,whichfile:msg_file_name}
      $.ajax({url:origin_url+"/index.php/dashboard/msg", type:"POST",data:data,
       success:function(response){
        var res = JSON.parse(response);

         
       }
     });
  });
function scroll_x(){
   $(".divflow").mCustomScrollbar({
    axis:"x", // horizontal scrollbar
    theme:"dark-3",
    
    callbacks:{
        onScroll:function(){
          //
            var topos1 = (-1 *this.mcs.left)-50;
            var topos2 = (-1 *this.mcs.left)+50;
            //alert(topos);
            $(".companyflowtext").animate({ "margin-left":""+topos1+"px"}, 1000);
            $(".progress").animate({ "margin-left":""+topos2+"px"}, 1000);
        }
    }
  });
}
  $(document).on('click','#coord_ongoing_ass',function(e){
    e.preventDefault();
    //$(".coord_orders").addClass('hide');
    $("#taskpage").addClass('hide');
    $('.coord_finished_ass').addClass('hide');
     $('.coord_ongoing_ass').removeClass('hide');
     $('.coord_report_ass').addClass('hide');
      $('.coord_orders').removeClass('show-for-large-up');
    $('.coord_orders').addClass('hide');
    $('.assess_for_small_dev').addClass('hide');
    $('.coord_ongoing_ass').append("<div class='row' id='loading_coord_ongoing_ass'><div class='small-12 medium-12 large-12 columns'><i class='fa fa-spinner fa-spin fa-3x'></i></div></div>");
    var all_accepted = ['Statement_of_account:Accepted','Customer_invoices:Accepted','payment_customer_invoices:Accepted','supplier_invoices:Accepted','payment_supplier_invoices:Accepted','','',''];
    var all_uploaded = ['Statement_of_account_uploaded','Customer_invoices_uploaded','payment_customer_invoices_uploaded','supplier_invoices_uploaded','payment_supplier_invoices_uploaded','external_file_uploaded','',''];
    $.ajax({url:origin_url+"/index.php/dashboard/get_one_ongoing_for_auditor", type:"POST",data:{assess:justone_ass_for_coord},
       success:function(response){
         var res = JSON.parse(response);
         $('.coord_ongoing_ass').html('');
         var x =0;
         if(res.length>0){
         
          for(var m=0;m<res.length;m++){
              if(res[m].status.toLowerCase() =='open' || res[m].status.toLowerCase() =='work in progress' || res[m].status.toLowerCase() =='closed'){
                        x=1;
                        var stage = res[m].status.toLowerCase()=='work in progress'?"WIP":res[m].status.toUpperCase();
                        var ongoing = "<div class='eachass' id='eachass"+m+"' data-ongoing='"+res[m].assessment_id+"'>"
                                +"<div class='row' style='background-color:#364150;margin-top:20px;color:white;padding:5px 5px 5px 5px;'><div class='small-4 medium-4 large-4 columns left'>"
                                +res[m].Name.toUpperCase()+"</div><div class='small-4 medium-4 large-4 columns'>ID: "+res[m].assessment_id+"</div>"
                                +"<div class='small-4 medium-4 large-4 columns'>STAGE: "+stage+"</div></div>"
                                +"<div class='row auditor_flow show-for-medium-up' style='margin-top:20px;'><div class='small-4 medium-4 large-4 columns'>Auditor Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><div class='small-4 medium-4 large-4 columns'>Auditor Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><div class='small-4 medium-4 large-4 columns'>Auditor Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><hr class='show-for-medium-up' style='margin-top:-3px;'></div>"
                                +"<div class='divflow'>"

                                +"<div class='div1'><ul class='all_flows'>"
                                +"<li><span class='eachfile'  id='Statement_of_account"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='Statement_of_account' company_id = '"+res[m].company_id+"' id='companyfile"+m+"' data-reveal-id='companyuploadedfile'>Statement of Account</a></span><span class='subcon' id='subcon"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='chats_for_coord sub_for_coord' id='sub0"+m+"' filename='Statement_of_account'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='Customer_invoices"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='Customer_invoices' company_id = '"+res[m].company_id+"' id='companyfile2"+m+"' data-reveal-id='companyuploadedfile'>Customer Invoices</a></span><span class='subcon' id='subcon1"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='chats_for_coord sub_for_coord' id='sub1"+m+"' filename='Customer_invoices'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='payment_customer_invoices"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='payment_customer_invoices' company_id = '"+res[m].company_id+"' id='companyfile3"+m+"' data-reveal-id='companyuploadedfile'>Payment On Customer Invoices</a></span><span class='subcon' id='subcon2"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='chats_for_coord sub_for_coord' id='sub2"+m+"' filename='payment_customer_invoices'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='supplier_invoices"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='supplier_invoices' company_id = '"+res[m].company_id+"' id='companyfile4"+m+"' data-reveal-id='companyuploadedfile'>Supplier Invoices</a></span><span class='subcon' id='subcon3"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='chats_for_coord sub_for_coord' id='sub3"+m+"' filename='supplier_invoices'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='payment_supplier_invoices"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='payment_supplier_invoices' company_id = '"+res[m].company_id+"' id='companyfile5"+m+"' data-reveal-id='companyuploadedfile'>Payment On Supplier Invoices</a></span><span class='subcon' id='subcon4"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='sub' id='sub4"+m+"' filename='payment_supplier_invoices'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='external_file"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='external_file' company_id = '"+res[m].company_id+"' id='otherfile"+m+"' data-reveal-id='externalfile'>External Document</a></span><span class='subcon' id='subcon5"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></span></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='initial_assessment"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='generate_vat' num='"+m+"' filename='initial_assessment' company_id = '"+res[m].company_id+"' id='vat"+m+"' data-reveal-id='generate_vat'>Generate Initial Assessment</a></span><span class='subcon' id='subcon6"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='chats_for_coord sub_for_coord' id='sub5"+m+"' filename='initial_assessment'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='final_assessment"+m+"'><a href='#' assess_id='"+res[m].assessment_id+"' class='companyfile' num='"+m+"' filename='final_assessment' company_id = '"+res[m].company_id+"' id='reportfile8"+m+"' data-reveal-id='companyuploadedfile'>Generate Final Assessment</a></span><span class='subcon' id='subcon7"+m+"'><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'><a href='#' data-reveal-id='allconversations' assess_id='"+res[m].assessment_id+"' num='"+m+"' company_id = '"+res[m].company_id+"' class='chats_for_coord sub_for_coord' id='sub6"+m+"' filename='final_assessment'>SS</a></sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"</ul></div>"

                                +"<div class='show-for-medium-up progress [small-# large-#] [secondary alert success] [radius round]' style='border:1px solid skyblue;height:30px;margin-left:50px;width:900px;'>"
                                +"<span class='meter' id='meter"+m+"' style=''></span></div>"
                                +"<div style='margin-top:20px;margin-bottom:20px;'></div>"
                                 +"<div class='row companyflowtext'><div class='small-6 medium-4 large-4 large-offset-1 columns'>Company Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><div class='small-6 medium-4 large-3 columns'>Company Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div><div class='small-6 medium-4 large-3 columns'>Company Flow &nbsp;&nbsp;<i class='fa fa-long-arrow-right'></i></div></div><hr class='show-for-medium-up' style='margin-top:-3px;width:1300px;margin-left:80px;'>"
                                +"<div class='div1'><ul class='all_flows'>"
                                +"<li><span class='eachfile' id='Statement_of_account2"+m+"'>Statement of Account</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='Customer_invoices2"+m+"'>Customer Invoices</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"<li style='margin-left:20px;'><span class='eachfile' id='payment_customer_invoices2"+m+"'>Payment On Customer Invoices</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='supplier_invoices2"+m+"'>Supplier Invoices</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='payment_supplier_invoices2"+m+"'>Payment On Supplier Invoices</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='external_file2"+m+"'>External Document</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='initial_assessment2"+m+"'>Generate Initial Assessment</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                 +"<li style='margin-left:20px;'><span class='eachfile' id='final_assessment2"+m+"'>Generate Final Assessment</span><sub><sub><sub><sub><sub><sub><sub><sub><sub class='msg_icon' style='font-size:3.8em'>pp</sub></sub></sub></sub></sub></sub></sub></sub></sub></li>"
                                +"</ul></div>"
                                +"</div>"
                              $(".coord_ongoing_ass").append(ongoing);  
                              scroll_x();

                              //alert("am here");
                               var vat = $.inArray('vat_generated',res[m].all_flow);
                               if(vat > 0){
                                 $("#initial_assessment"+m+"").css('border','2px solid #4CAF50');
                                 $("#initial_assessment2"+m+"").css('border','2px solid #4CAF50');
                                 $("#meter"+m+"").css('width','90%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('90%');
                               }
                               for(s=0;s<all_accepted.length;s++){
                                var acc = $.inArray(all_accepted[s],res[m].all_flow);
                                var upload = $.inArray(all_uploaded[s],res[m].all_flow);
                                if(acc>0){
                                  var filename = all_accepted[s].split(':')[0];
                                  $("#"+filename+""+m+"").css('border','2px solid #4CAF50');
                                  $("#"+filename+"2"+m+"").css('border','2px solid #4CAF50');
                                }
                                else if(acc==-1 && upload>0){
                                   var filename = all_uploaded[s].split('_uploaded')[0];
                                  $("#"+filename+""+m+"").css('border','2px solid yellow');
                                  $("#"+filename+"2"+m+"").css('border','2px solid yellow');
                                  if(filename ='external_file'){
                                     $("#"+filename+""+m+"").css('border','2px solid yellow');
                                     $("#"+filename+"2"+m+"").css('border','2px solid yellow');
                                  }
                                }
                              }
                              var final_assessment = $.inArray('final_assessment_uploaded',res[m].all_flow);
                              if(final_assessment > 0){
                                 $("#final_assessment"+m+"").css('border','2px solid #4CAF50');
                                 $("#final_assessment2"+m+"").css('border','2px solid #4CAF50');
                                 $("#meter"+m+"").css('width','100%');
                                 $('#meter'+m+'').css('text-align','center');
                                 $('#meter'+m+'').css('color','white');
                                 $('#meter'+m+'').text('100%');

                               }



                }   
              }      
              if(x==0 && m==(res.length-1)){
               $(".coord_ongoing_ass").append("No Ongoing Assessment");
                $(".coord_ongoing_ass").addClass('add_to_no_ongoing');
           }           
          }
          else{
            $(".coord_ongoing_ass").append("No Ongoing Assessment");
             $(".coord_ongoing_ass").addClass('add_to_no_ongoing');
          }
       }
    });
  });
  $(document).on("click",".coord_otherfile",function(e){
   e.preventDefault();
  // alert("am");
   var assess_id = $("#"+this.id+"").attr('assess_id');
   $.ajax({url:origin_url+"/index.php/dashboard/getuploadedfile", type:"POST", data:{assessment_id:assess_id},
    success:function(response){
        var res  =JSON.parse(response);
        $(".listfile").html('');
      if(res.length > 0){
         $(".listfile").html('');
         var files = res[0].files.split(';');
        for(var b=0;b<files.length;b++){
          if(files[b].length>0)
          $(".listfile").append('<li><a href="http://localhost/tax/assets/upload/'+files[b]+'" download>'+files[b]+'</a></li>');
        }
       }
       else{
         $(".listfile").append('<li>No External File</li>');
       }
     }
    });
  });
  $(document).on('click','.status_of_acceptance',function(e){
    //alert(sta);
  var status = $("#"+this.id+"").attr('status');
   e.preventDefault();
   if(status =='Accepted'){
      sweetAlert({
              title: 'Confirm',
              text: 'The Auditor has Accredited all document and attested it to be okay.',
              type: 'success'
              });
   }
   else if(status=='Rejected'){
    swal("Oops!!", 'The document was rejected', "error");
   }
   else{
    sweetAlert("Auditing still in Progress");
   }
  });
  $(document).on('click','.coord_request', function(e){
    e.preventDefault();
    $('#show_coord_uploaded_file').html('');
    var assess_id = $("#"+this.id+"").attr('assess_id');
       $("#show_coord_uploaded_file").append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
   $.ajax({url:origin_url+"/index.php/dashboard/getuploadedfile", type:"POST", data:{assessment_id:assess_id},
    success:function(response){
      var res  =JSON.parse(response);
      if(res.length > 0){
        $("#show_coord_uploaded_file").html('');
        //$("#show_coord_uploaded_file").append('<div class="row" style="margin-top:30px;color:grey"><div class="small-12 medium-12 large-12 columns"><span style="font-size:1.2em">Financial Statement Period</span>: '+res[0].period.split(';')[0]+'  to '+res[0].period.split(';')[1]+'   '+res[0].period.split(';')[2]+'</div></div>');
        //$("#show_coord_uploaded_file").append('<div class="row" style="margin-top:20px;color:grey"><div class="small-12 medium-12 large-12 columns">Uploaded Files</div></div>');
        var files = res[0].files.split(';');
        for(var b=0;b<files.length;b++){
          $("#show_coord_uploaded_file").append('<div class="row" style="margin-left:17px;"><div class="small-12 medium-12 large-12 columns"><a href="http://localhost/tax/assets/upload/'+files[b]+'" download>'+files[b]+'</a></div></div>');
        }
      }

    }
   });
  })
  $(document).on('click','#coord_assigned_task', function(e){
    e.preventDefault();
    $('#taskpage').addClass('hide');
    $('.coord_ongoing_ass').addClass('hide');
    $('.coord_finished_ass').addClass('hide');
    $('.coord_orders').removeClass('hide');
    $('.coord_report_ass').addClass('hide');
    $('.coord_orders').addClass('show-for-large-up');
    $('.coord_orders').removeClass('hide');
    $('.assess_for_small_dev').removeClass('hide');

  })
  $(document).on("click",".chats_for_coord", function(e){
   e.preventDefault();
   var filename  = $("#"+this.id+"").attr('filename');;
   assessment_id_for_chats = $("#"+this.id+"").attr('assess_id');
   company_id_for_chat = $("#"+this.id+"").attr('company_id');
   //msg_id = $("#"+this.id+"").attr('num');
    $.ajax({url:origin_url+"/index.php/dashboard/get_chats", type:"POST",data:{assess_id:assessment_id_for_chats,filename:filename},
       success:function(response){
        var res = JSON.parse(response);
       if(res.length>0){
        $("#allchats").html('');
        for(var m=0;m<res.length;m++){
          if(res[m].task_to_id == res[m].sender){
            $("#allchats").append("<div class='clearfix'><p class='left' style='background-color:#F8F8FF;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'><span style='font-size:0.8em;color:red'>Auditor</span><br> "+res[m].message+"</p><a class='right'></a></div>");
          }
          if(res[m].task_to_id != res[m].sender){
            $("#allchats").append("<div class='clearfix'><p class='left' style='background-color:#F8F8FF;padding:5px 5px 5px 5px;border-radius:5px;margin-top:5px;'><span style='font-size:0.8em;color:red'>Company</span><br> "+res[m].message+"</p><a class='right'></a></div>");
          }
        }
        $("#allchats").animate({ scrollTop: $("#allchats")[0].scrollHeight}, 1000);
       }
       else{
        $("#allchats").append("<p>No COnversations<p>");
       }  
       }
     });
   //alert(assessment_id_for_more_request);
    /*$("#ReviewF"+num+"").html('<a style="font-size:0.8em;">Review File</a>');
    $("#requestMF"+num+"").css('border','1px solid red');
    $("#ReviewF"+num+"").css('border','none');
    $("#meter"+num+"").css('width','50%');
   $.ajax({url:origin_url+"/index.php/dashboard/getallchats", type:"POST", data:{assessment_id:assessment_id_for_more_request},
    success:function(response){
      var res = JSON.parse(response);
      $('.company_add_file').append('<div row style="margin-top:20px;color:grey"> <div class="small-12 medium-12 large-12 columns">ADDITIONAL FILE</div></div><hr style="margin-top:-3px;">');
      var file = res[0].files.split(';');
      for(var t=0;t<file.length;t++){
        $('.company_add_file').append('<div row style="margin-top:20px;"> <div class="small-8 medium-8 large-8 medium-offset-2 large-offset-2 columns"><a href="localhost/tax/assets/upload/'+file[t]+'" download>'+file[t]+'</a></div></div>');
      }
    }
  });*/
});
var sta;

 $(document).on('click','#gen_report',function(e){
  e.preventDefault();
  $('.assessment_order').removeClass('show-for-large-up');
  $('.assessment_order').addClass('hide');
  $('.finished_ass').addClass('hide');
  $('.ongoing_ass').addClass('hide');
  $('.gen_report').removeClass('hide');
  $(".all_gen_reports").addClass('hide');
  $("#dash_title").text('Write Report');

 });
  $(document).on('click','#all_reports',function(e){
   e.preventDefault();
   $('.assessment_order').removeClass('show-for-large-up');
   $('.assessment_order').addClass('hide');
  $('.finished_ass').addClass('hide');
  $('.ongoing_ass').addClass('hide');
  $('.gen_report').addClass('hide');
  $(".all_gen_reports").removeClass('hide');
  $("#dash_title").text('Reports');
  $(".all_gen_reports").append('<div class="row" style="text-align:center;margin-top:40px;"><div class="small-12 medium-12 large-12 columns"><i class="fa fa-spinner fa-spin fa-3x"></i></div></div>');
   $.ajax({url:origin_url+"/index.php/dashboard/all_auditors_report", type:"POST", data:null,
             success:function(response){
              var res = JSON.parse(response);
               $(".all_gen_reports").html('');
               if(res.length>0){
             for(var m=0;m<res.length;m++){

           if(res[m].report.length>300){
                var shortText = res[m].report.substring(0, 300);
                var otherpart = res[m].report.substring(300,  res[m].length);
               }
               else if(res[m].report.length<300){
                var shortText = res[m].report;
               }
               var report = "<div class='row' style='background-color:#364150;font-weight:100px;margin-top:20px;color:white;padding-top:5px;padding-bottom:5px;'><div class='small-12 medium-6 large-6 columns'>ASSESSMENT ID: "+ res[m].assessment_id+""
                            +"</div><div class='small-12 medium-4 large-4 medium-offset-2 large-offset-2 columns'>REPORT DATE: " +res[m].created+"</div></div>"
                            +"<div class='row' style='margin-top:15px;'><div class='small-12 medium-12 large-12 columns' id='shortreport"+m+"'>"+shortText+"<a href='#' class='detreport' num='"+m+"'  id='detreport"+m+"'>...More</a></div></div>"
                            +"<div class='row hide style='margin-top:15px;' id='allreport"+m+"'><div class='small-12 medium-12 large-12 columns'>"+res[m].report+"<a href='#' class='redreport' num='"+m+"'  id='redreport"+m+"'>...Less</a></div></div>"


                $(".all_gen_reports").append(report); 
        }
      }
      else{
        $('.all_gen_reports').html('');
            $('.all_gen_reports').addClass('add_to_no_ongoing');
            $('.all_gen_reports').append('No Report');    
      }
              }
            });
  });
$(document).on('click','.detreport',function(e){
  e.preventDefault();
  var num = $("#"+this.id+"").attr('num');
  $("#shortreport"+num+"").addClass('hide');
  $("#allreport"+num+"").removeClass('hide');
})
$(document).on('click','.redreport',function(e){
  e.preventDefault();
   var num = $("#"+this.id+"").attr('num');
     $("#allreport"+num+"").addClass('hide');
  $("#shortreport"+num+"").removeClass('hide');
})
$(document).on('click','.tools',function(e){
  var pos= $(".sideall").position();
  if($(window).width() <= 767 && pos.top <='-2000'){
 e.preventDefault();
  $(".sideall").animate({top: '50px'});
 $('.sideall').css('z-index','100');
  }
  else{
    $(".sideall").animate({top: '-2000px'});
    //$('.sideall').css('z-index','100');
  }
});
$(document).on('click','.sidelist',function(e){
  if($(window).width() <= 767){
 e.preventDefault();
  $(".sideall").animate({top: '-2000px'});
 $('.sideall').css('z-index','100');
  }
});
$(document).on('click','.menubar',function(e){
   e.preventDefault();
  $(".sideall").animate({top: '-2000px'});
  if($(window).width() <= 767){
   var pos= $(".sideall").position();
   if(pos.top == 0){
    $(".sideall").animate({top: '-2000px'});
  }else{
     
  }
 } 
});
$(window).scroll(function(e){
  e.preventDefault();
  $('.sideall').css('margin-top',$(document).scrollTop()+'px');
})
$(document).on('click','#all_report',function(e){
 e.preventDefault();
 $('.coord_ongoing_ass').addClass('hide');
 $('.coord_finished_ass').addClass('hide');
 $('#taskpage').addClass('hide');
 $('.coord_orders').addClass('hide');
 $('.coord_report_ass').removeClass('hide');
  $('.coord_orders').removeClass('show-for-large-up');
    $('.coord_orders').addClass('hide');
    $('.assess_for_small_dev').addClass('hide');
   $.ajax({url:origin_url+"/index.php/dashboard/all_coord_report", type:"POST", data:null,
             success:function(response){
              var res = JSON.parse(response);
               $(".coord_report_ass").html('');
               if(res.length>0){
             for(var m=0;m<res.length;m++){

           if(res[m].report.length>300){
                var shortText = res[m].report.substring(0, 300)+"<a href='#' class='detreport' num='"+m+"'  id='detreport"+m+"'>...More</a>";
                var otherpart = res[m].report.substring(300,  res[m].length);
               }
               else if(res[m].report.length<300){
                var shortText = res[m].report;
               }
               var report = "<div class='clearfix' style='background-color:#364150;font-weight:100px;margin-top:20px;color:white;height:30px;padding-top:5px;padding-bottom:5px;'><p class='left' style='margin-left:10px;'>ASSESSMENT ID: "
                            + res[m].assessment_id+"</p>"
                            +"<p class='right' style='margin-right:10px;'>REPORT DATE: " +res[m].created+"</p></div>"
                            +"<div class='row'><div class='small-12 medium-12 large-12 columns' id='shortreport"+m+"'>"+shortText+"</div></div>"
                            +"<div class='row hide' id='allreport"+m+"'><div class='small-12 medium-12 large-12 columns'>"+res[m].report+"<a href='#' class='redreport' num='"+m+"'  id='redreport"+m+"'>...Less</a></div></div>"


                $(".coord_report_ass").append(report); 
        }
      }
      else{
        $('.coord_report_ass').html('');
               $(".coord_report_ass").append("No Report");
                $(".coord_report_ass").addClass('add_to_no_ongoing');
      }
              }
            });       
});
$(document).on("click","#to_change_pass", function(e){
  e.preventDefault();
  var pass = $("#ch_pass").val();
  var cpass = $("#ch_cpass").val();
  if(pass != cpass){
    swal("Oops!!!","Password Mismatched","error");
    return false;
  }
  $.ajax({url:origin_url+"/index.php/dashboard/change_pass", type:"POST", data:{pass:pass,cpass:cpass},
    success:function(response){
      var res = JSON.parse(response);
      if(res == true){
        $('#change_pass').foundation('reveal', 'close');
        sweetAlert({
              title: 'Confirm',
              text: 'Password successfully changed',
              type: 'success'
              });
      }
      else{
         swal("Oops!!!","Password Mismatched","error");
      }
    }
  });
});
  var justone_ass,justone_ass_for_coord; 
 $(document).on("click",".open_assess",function(e){
    justone_ass= $("#"+this.id+"").attr('data-id');
   $("#ongoing_ass").trigger("click");   
 });
 $(document).on("click",".open_assess_for_coord",function(e){
    justone_ass_for_coord= $("#"+this.id+"").attr('data-id');
    //alert(justone_ass_for_coord);
   $("#coord_ongoing_ass").trigger("click");   
 });

 $('#value_to_search').on('keyup',function(){
    var value = this.value.toLowerCase().trim();

    $("table tr").each(function (index) {
        if (!index) return;
        $(this).find("td").each(function () {
            var id = $(this).text().toLowerCase().trim();
            var not_found = (id.indexOf(value) == -1);
            $(this).closest('tr').toggle(!not_found);
            return not_found;
        });
    });
});
});